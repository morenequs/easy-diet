<?php

declare(strict_types=1);

namespace App\Util;

use Doctrine\DBAL\Driver\Connection;

class InsertBulk
{
    private Connection $connection;
    private string $tableName;
    private string $columns;
    /** @var string[] */
    private array $bulkStorage = [];

    /** @param string[] $columns */
    public function __construct(Connection $connection, string $tableName, array $columns)
    {
        $this->connection = $connection;
        $this->tableName = $tableName;
        $this->columns = implode(', ', $columns);
    }

    /** @param string[] $data */
    public function addRow(array $data): void
    {
        $this->bulkStorage[] = $this->createValuesStringFromData($data);
    }

    /** @param string[][] $rows */
    public function addRows(array $rows): void
    {
        $this->bulkStorage = array_merge(
            $this->bulkStorage,
            array_map(fn (array $row) => $this->createValuesStringFromData($row), $rows)
        );
    }


    public function executeInsert(): void
    {
        if (!count($this->bulkStorage)) {
            return;
        }

        $insert = 'INSERT INTO ' . $this->tableName . ' (' . $this->columns . ') VALUES ' . implode(',', $this->bulkStorage) . ';';
        $this->connection->exec($insert);
        $this->bulkStorage = [];
    }

    /** @param string[] $data */
    private function createValuesStringFromData(array $data): string
    {
        return '(' . implode(',', $data) . ')';
    }
}
