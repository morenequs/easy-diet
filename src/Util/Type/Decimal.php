<?php

declare(strict_types=1);

namespace App\Util\Type;

class Decimal implements \JsonSerializable
{
    const DEFAULT_SCALE = 3;

    private string $value;

    private int $scale;

    private function __construct(string $value, int $scale = self::DEFAULT_SCALE)
    {
        if (!self::isNumber($value)) {
            throw new \InvalidArgumentException('Value is not valid number');
        }

        $this->value = bcadd($value, '0', $scale);
        $this->scale = $scale;
    }

    public static function fromString(string $value, int $scale = self::DEFAULT_SCALE): Decimal
    {
        return new self($value, $scale);
    }

    public static function createFromInt(int $value, int $scale = self::DEFAULT_SCALE): Decimal
    {
        return new self((string) $value, $scale);
    }

    public static function add(Decimal $left, Decimal $right, int $scale = self::DEFAULT_SCALE): Decimal
    {
        return new self(bcadd($left->__toString(), $right->__toString(), $scale), $scale);
    }

    public static function sub(Decimal $left, Decimal $right, int $scale = self::DEFAULT_SCALE): Decimal
    {
        return new self(bcsub($left->__toString(), $right->__toString(), $scale), $scale);
    }

    public static function mul(Decimal $left, Decimal $right, int $scale = self::DEFAULT_SCALE): Decimal
    {
        return new self(bcmul($left->__toString(), $right->__toString(), $scale), $scale);
    }

    public static function div(Decimal $left, Decimal $right, int $scale = self::DEFAULT_SCALE): Decimal
    {
        $result = bcdiv($left->__toString(), $right->__toString(), $scale);
        if (is_null($result)) {
            throw new \DivisionByZeroError();
        }

        return new self($result, $scale);
    }

    public static function isNumber(string $value): bool
    {
        return is_numeric($value);
    }

    public function equal(Decimal $value): bool
    {
        $scaleToCompare = $this->scale > $value->getScale() ? $this->scale : $value->getScale();

        return 0 === bccomp($this->value, $value->__toString(), $scaleToCompare);
    }

    public function greaterThenOrEqual(Decimal $value): bool
    {
        $scaleToCompare = $this->scale > $value->getScale() ? $this->scale : $value->getScale();

        return 0 <= bccomp($this->value, $value->__toString(), $scaleToCompare);
    }

    public function lessThenOrEqual(Decimal $value): bool
    {
        $scaleToCompare = $this->scale > $value->getScale() ? $this->scale : $value->getScale();

        return 0 >= bccomp($this->value, $value->__toString(), $scaleToCompare);
    }

    public function mulByInt(int $right, int $scale = null): Decimal
    {
        $scale = $scale ?: $this->scale;

        return new self(bcmul($this->value, (string) $right, $scale), $scale);
    }

    public function getPercentage(Decimal $percentage, int $scale = self::DEFAULT_SCALE): Decimal
    {
        return self::div(self::mul($this, $percentage, $scale), Decimal::createFromInt(100), $scale);
    }

    public function getScale(): int
    {
        return $this->scale;
    }

    public function __toString(): string
    {
        return $this->value;
    }

    public function toInt(): int
    {
        return intval($this->value);
    }

    public function jsonSerialize()
    {
        return $this->__toString();
    }
}
