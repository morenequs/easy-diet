<?php

declare(strict_types=1);

namespace App\Util\Type;

use ArrayIterator;
use IteratorAggregate;

/**
 * @template T
 * @phpstan-implements IteratorAggregate<int, T>
 */
abstract class Collection implements IteratorAggregate
{
    /**
     * @var T[]
     */
    protected array $values;

    /**
     * @return T[]
     */
    public function toArray(): array
    {
        return $this->values;
    }

    public function getIterator()
    {
        return new ArrayIterator($this->values);
    }
}
