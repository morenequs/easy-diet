<?php

declare(strict_types=1);

namespace App\Util\Progress;

use Symfony\Component\Console\Style\SymfonyStyle;

class ProgressTerminal implements ProgressInterface
{
    private SymfonyStyle $symfonyStyle;

    public function __construct(SymfonyStyle $symfonyStyle)
    {
        $this->symfonyStyle = $symfonyStyle;
    }

    public function start(int $maxSteps): void
    {
        $this->symfonyStyle->progressStart($maxSteps);
    }

    public function advance(int $step): void
    {
        $this->symfonyStyle->progressAdvance($step);
    }

    public function finish(): void
    {
        $this->symfonyStyle->progressFinish();
    }
}
