<?php

namespace App\Util\Progress;

interface ProgressInterface
{
    public function start(int $maxSteps): void;

    public function advance(int $step): void;

    public function finish(): void;
}
