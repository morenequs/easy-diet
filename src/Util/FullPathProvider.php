<?php

declare(strict_types=1);

namespace App\Util;

use Symfony\Component\HttpFoundation\RequestStack;

class FullPathProvider
{
    private RequestStack $requestStack;

    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    public function createFullPath(string $path): string
    {
        if(!$request = $this->requestStack->getMasterRequest()) {
            throw new \RuntimeException('Master request is not available');
        }

        return $request->getSchemeAndHttpHost() . '/' . $path;
    }
}