<?php

declare(strict_types=1);

namespace App\Infrastructure\ViewModel\Product\Transformer;

use App\Entity\Product;
use App\Infrastructure\ViewModel\Product\ProductNameViewModel;

class ProductNameTransformer
{
    public function transform(Product $product): ProductNameViewModel
    {
        return new ProductNameViewModel(
            $product->getId(),
            $product->getName(),
            $product->getSlug()
        );
    }

    public function transformMultiple(array $products): array
    {
        return array_map(fn (Product $product) => $this->transform($product), $products);
    }
}