<?php

declare(strict_types=1);

namespace App\Infrastructure\ViewModel\Product;

class ProductNameViewModel
{
    private $id;

    private string $name;

    private string $slug;

    public function __construct($id, string $name, string $slug)
    {
        $this->id = $id;
        $this->name = $name;
        $this->slug = $slug;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }
}