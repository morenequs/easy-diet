<?php

declare(strict_types=1);

namespace App\Infrastructure\ViewModel\MealPlan;

use App\Infrastructure\ViewModel\Diet\DietViewModel;

class InitDataViewModel
{
    /** @var DietViewModel[] */
    private array $diets;

    public function __construct(array $diets)
    {
        $this->diets = $diets;
    }

    public function getDiets(): array
    {
        return $this->diets;
    }
}