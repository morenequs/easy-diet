<?php

declare(strict_types=1);

namespace App\Infrastructure\ViewModel\MealPlan;

class MealPlanResponseViewModel
{
    private string $type;

    private ?MealPlanViewModel $mealPlan;

    public function __construct(string $type, ?MealPlanViewModel $mealPlan)
    {
        $this->type = $type;
        $this->mealPlan = $mealPlan;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getMealPlan(): ?MealPlanViewModel
    {
        return $this->mealPlan;
    }
}