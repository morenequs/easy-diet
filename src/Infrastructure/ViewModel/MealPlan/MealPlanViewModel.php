<?php

declare(strict_types=1);

namespace App\Infrastructure\ViewModel\MealPlan;

use App\Infrastructure\ViewModel\Meal\MealViewModel;

class MealPlanViewModel
{
    /** @var MealViewModel[] */
    private array $meals;

    /** @var array */
    private array $totalNutrients;

    public function __construct(array $meals, array $totalNutrients)
    {
        $this->meals = $meals;
        $this->totalNutrients = $totalNutrients;
    }

    public function getMeals(): array
    {
        return $this->meals;
    }

    public function getTotalNutrients(): array
    {
        return $this->totalNutrients;
    }
}