<?php

declare(strict_types=1);

namespace App\Infrastructure\ViewModel\MealPlan\Transformer;

use App\Entity\MealPlan;
use App\Entity\MealPlanType;
use App\Infrastructure\ViewModel\MealPlan\MealPlanResponseViewModel;

class MealPlanResponseTransformer
{
    private MealPlanTransformer $mealPlanTransformer;

    public function __construct(MealPlanTransformer $mealPlanTransformer)
    {
        $this->mealPlanTransformer = $mealPlanTransformer;
    }

    public function transform(MealPlan $mealPlan): MealPlanResponseViewModel
    {
        return new MealPlanResponseViewModel(
            (string) $mealPlan->getType(),
            $mealPlan->isType(MealPlanType::SUCCESS) ? $this->mealPlanTransformer->transform($mealPlan) : null
        );
    }
}