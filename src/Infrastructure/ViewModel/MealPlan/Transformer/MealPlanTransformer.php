<?php

declare(strict_types=1);

namespace App\Infrastructure\ViewModel\MealPlan\Transformer;

use App\Entity\MealPlan;
use App\Infrastructure\ViewModel\Meal\Transformer\MealTransformer;
use App\Infrastructure\ViewModel\MealPlan\MealPlanViewModel;

class MealPlanTransformer
{
    private MealTransformer $mealTransformer;

    public function __construct(MealTransformer $mealTransformer)
    {
        $this->mealTransformer = $mealTransformer;
    }

    public function transform(MealPlan $mealPlan): MealPlanViewModel
    {
        return new MealPlanViewModel(
            $this->mealTransformer->transformMultiple($mealPlan->getMeals()->toArray()),
            $mealPlan->getTotalNutrientAmountData()
        );
    }
}