<?php

declare(strict_types=1);

namespace App\Infrastructure\ViewModel\MealPlan\Transformer;

use App\Infrastructure\ViewModel\Diet\Transformer\DietTransformer;
use App\Infrastructure\ViewModel\MealPlan\InitDataViewModel;

class InitDataTransformer
{
    private DietTransformer $dietTransformer;

    public function __construct(DietTransformer $dietTransformer)
    {
        $this->dietTransformer = $dietTransformer;
    }

    public function transform(array $diets): InitDataViewModel
    {
        return new InitDataViewModel($this->dietTransformer->transformMultiple($diets));
    }
}