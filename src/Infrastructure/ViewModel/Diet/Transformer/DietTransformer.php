<?php

declare(strict_types=1);

namespace App\Infrastructure\ViewModel\Diet\Transformer;

use App\Entity\Diet;
use App\Infrastructure\ViewModel\Diet\DietViewModel;

class DietTransformer
{
    public function transform(Diet $diet): DietViewModel
    {
        return new DietViewModel(
            $diet->getId(),
            $diet->getName(),
            $diet->getSlug()
        );
    }

    /**
     * @param Diet[] $diets
     * @return DietViewModel[]
     */
    public function transformMultiple(array $diets): array
    {
        return array_map(fn (Diet $diet) => $this->transform($diet), $diets);
    }
}