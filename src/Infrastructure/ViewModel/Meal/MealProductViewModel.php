<?php

declare(strict_types=1);

namespace App\Infrastructure\ViewModel\Meal;

class MealProductViewModel
{
    private int $id;
    private string $name;
    private string $gram;

    public function __construct(int $id, string $name, string $gram)
    {
        $this->id = $id;
        $this->name = $name;
        $this->gram = $gram;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getGram(): string
    {
        return $this->gram;
    }
}