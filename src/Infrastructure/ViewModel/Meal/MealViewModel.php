<?php

declare(strict_types=1);

namespace App\Infrastructure\ViewModel\Meal;

class MealViewModel
{
    private int $id;

    private string $name;

    private string $image;

    private int $prepTime;

    /** @var string[] */
    private array $steps;

    private array $totalNutrients;

    /** @var MealProductViewModel[] */
    private array $mealProducts;

    public function __construct(
        int $id,
        string $name,
        string $image,
        int $prepTime,
        array $steps,
        array $totalNutrients,
        array $mealProducts
    )
    {
        $this->id = $id;
        $this->name = $name;
        $this->image = $image;
        $this->prepTime = $prepTime;
        $this->steps = $steps;
        $this->totalNutrients = $totalNutrients;
        $this->mealProducts = $mealProducts;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getImage(): string
    {
        return $this->image;
    }

    public function getPrepTime(): int
    {
        return $this->prepTime;
    }

    public function getSteps(): array
    {
        return $this->steps;
    }

    public function getTotalNutrients(): array
    {
        return $this->totalNutrients;
    }

    public function getMealProducts(): array
    {
        return $this->mealProducts;
    }
}