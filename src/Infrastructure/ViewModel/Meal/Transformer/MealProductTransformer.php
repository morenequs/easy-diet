<?php

declare(strict_types=1);

namespace App\Infrastructure\ViewModel\Meal\Transformer;

use App\Entity\MealProduct;
use App\Infrastructure\ViewModel\Meal\MealProductViewModel;

class MealProductTransformer
{
    public function transform(MealProduct $mealProduct): MealProductViewModel
    {
        return new MealProductViewModel(
            $mealProduct->getProduct()->getId(),
            $mealProduct->getProduct()->getName(),
            $mealProduct->getGram()
        );
    }

    /**
     * @param MealProduct[] $mealProducts
     * @return MealProductViewModel[]
     */
    public function transformMultiple(array $mealProducts): array
    {
        return array_map(fn (MealProduct $mealProduct) => $this->transform($mealProduct), $mealProducts);
    }
}