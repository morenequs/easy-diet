<?php

declare(strict_types=1);

namespace App\Infrastructure\ViewModel\Meal\Transformer;

use App\Entity\Meal;
use App\Infrastructure\ViewModel\Meal\MealViewModel;
use App\Util\FullPathProvider;

class MealTransformer
{
    private MealProductTransformer $mealProductTransformer;
    private FullPathProvider $fullPathProvider;

    public function __construct(MealProductTransformer $mealProductTransformer, FullPathProvider $fullPathProvider)
    {
        $this->mealProductTransformer = $mealProductTransformer;
        $this->fullPathProvider = $fullPathProvider;
    }

    public function transform(Meal $meal): MealViewModel
    {
        return new MealViewModel(
            $meal->getId(),
            $meal->getName(),
            $this->fullPathProvider->createFullPath($meal->getImage()),
            $meal->getPrepTime(),
            $meal->getSteps(),
            $meal->getTotalNutrientAmountData(),
            $this->mealProductTransformer->transformMultiple($meal->getMealProducts()->toArray())
        );
    }

    public function transformMultiple(array $meals): array
    {
        return array_map(fn (Meal $meal) => $this->transform($meal), $meals);
    }
}