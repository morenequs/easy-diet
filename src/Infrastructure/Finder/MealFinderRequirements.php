<?php

declare(strict_types=1);

namespace App\Infrastructure\Finder;

class MealFinderRequirements
{
    private array $excludedIds = [];
    private ?string $dietType = null;

    public function __construct(array $excludedIds, ?string $dietType)
    {
        $this->excludedIds = $excludedIds;
        $this->dietType = $dietType;
    }

    public function getExcludedIds(): array
    {
        return $this->excludedIds;
    }

    public function setExcludedIds(array $excludedIds): void
    {
        $this->excludedIds = $excludedIds;
    }

    public function getDietType(): ?string
    {
        return $this->dietType;
    }

    public function setDietType(?string $dietType): void
    {
        $this->dietType = $dietType;
    }
}