<?php

declare(strict_types=1);

namespace App\Infrastructure\Finder;

use App\Entity\Meal;
use App\Entity\NutrientType;
use App\Infrastructure\Service\MealPlan\MealPlanRequirements\MealPlanRequirements;
use App\Infrastructure\Service\MealPlan\MealPlanRequirements\Range;

interface MealFinderInterface
{
    /**
     * @param NutrientType[] $returnedNutrientTypes
     * @param int[] $excludedIds
     *
     * @return array[]
     */
    public function findMealsByRangeOfEnergy(
        Range $range,
        array $returnedNutrientTypes,
        MealFinderRequirements $mealFinderRequirement
    ): array;

    public function findMealByRequirements(MealPlanRequirements $requirements): ?int;
}
