<?php

declare(strict_types=1);

namespace App\Infrastructure\Finder;

use App\Entity\NutrientType;
use App\Infrastructure\Projector\MealPlan\MealProjector;
use App\Infrastructure\Service\MealPlan\MealPlanRequirements\MealPlanRequirements;
use App\Infrastructure\Service\MealPlan\MealPlanRequirements\NutrientRequirement;
use App\Infrastructure\Service\MealPlan\MealPlanRequirements\Range;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Query\QueryBuilder;

class MealFinder implements MealFinderInterface
{
    private Connection $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function findMealsByRangeOfEnergy(
        Range $range,
        array $returnedNutrientTypes,
        MealFinderRequirements $mealFinderRequirements
    ): array
    {
        if (!in_array('energy', $returnedNutrientTypes)) {
            throw new \InvalidArgumentException('$returnedNutrientTypes must contain at least energy field');
        }

        if (array_diff($returnedNutrientTypes, NutrientType::AVAILABLE_NUTRIENT_TYPES)) {
            throw new \InvalidArgumentException('$returnedNutrientTypes has unsupported columns');
        }

        return $this->prepareQueryBuilder(
            $this->prepareColumnsToRetrieve($returnedNutrientTypes),
            $mealFinderRequirements
        )
            ->andWhere('mp.energy >= :minEnergy AND mp.energy <= :maxEnergy')
            ->setParameter('minEnergy', (string)$range->getMin())
            ->setParameter('maxEnergy', (string)$range->getMax())
            ->setMaxResults(100)
            ->execute()
            ->fetchAll();
    }

    public function findMealByRequirements(MealPlanRequirements $requirements): ?int
    {
        $nutrientTypes = $requirements->getNutrientTypes();
        if (!in_array('energy', $nutrientTypes)) {
            throw new \InvalidArgumentException('$returnedNutrientTypes must contain at least energy field');
        }

        if (array_diff($nutrientTypes, NutrientType::AVAILABLE_NUTRIENT_TYPES)) {
            throw new \InvalidArgumentException('$returnedNutrientTypes has unsupported columns');
        }

        $mealFinderRequirements = new MealFinderRequirements(
            $requirements->getExcludedMeals(),
            $requirements->getDietType()
        );

        $qb = $this->prepareQueryBuilder(
            'id',
            $mealFinderRequirements
        )
            ->orderBy('RAND()')
            ->setMaxResults(1);

        $this->applyNutrientRequirements($qb, $requirements->getNutrientRequirements());

        $mealId = $qb->execute()->fetchColumn();

        if (false === $mealId) {
            return null;
        }

        return intval($mealId);
    }

    private function prepareColumnsToRetrieve(array $returnedNutrientTypes): string
    {
        return 'mp.id, ' . implode(', ', $returnedNutrientTypes);
    }

    public function prepareQueryBuilder(
        string $columns,
        MealFinderRequirements $mealFinderRequirements
    ): QueryBuilder
    {
        $qb = $this->connection->createQueryBuilder();
        $qb
            ->select($columns)
            ->from(MealProjector::TABLE_NAME, 'mp');

        $excludedIds = $mealFinderRequirements->getExcludedIds();
        if (!empty($excludedIds)) {
            $qb->andWhere('mp.id NOT IN (:excludedIds)')
                ->setParameter('excludedIds', $excludedIds, Connection::PARAM_INT_ARRAY);
        }

        $dietType = $mealFinderRequirements->getDietType();
        if ($dietType) {
            $qb
                ->innerJoin('mp', 'meal_diet', 'meal_diet', 'mp.id = meal_diet.meal_id')
                ->innerJoin('meal_diet', 'diet', 'diet', 'diet.id = meal_diet.diet_id AND diet.slug = :diet')
                ->setParameter('diet', $dietType);
        }

        return $qb;
    }

    private function applyNutrientRequirements(QueryBuilder $qb, array $nutrientRequirements): void
    {
        /** @var NutrientRequirement $requirement */
        foreach ($nutrientRequirements as $requirement) {
            $type = (string)$requirement->getType();

            $qb->andWhere("mp.$type >= :min$type AND mp.$type <= :max$type")
                ->setParameter("min$type", (string)$requirement->getMin())
                ->setParameter("max$type", (string)$requirement->getMax());
        }
    }
}