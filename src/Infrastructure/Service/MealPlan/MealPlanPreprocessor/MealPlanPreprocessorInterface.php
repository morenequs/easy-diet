<?php

declare(strict_types=1);

namespace App\Infrastructure\Service\MealPlan\MealPlanPreprocessor;

use App\Infrastructure\Service\MealPlan\MealPlanRequirements\MealPlanRequirements;

interface MealPlanPreprocessorInterface
{
    /**
     * @return array[]
     */
    public function prepareSetsOfMealsByRequirements(MealPlanRequirements $mealPlanRequirements): array;
}
