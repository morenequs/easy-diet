<?php

declare(strict_types=1);

namespace App\Infrastructure\Service\MealPlan\MealPlanPreprocessor;

use App\Infrastructure\Finder\MealFinderInterface;
use App\Infrastructure\Finder\MealFinderRequirements;
use App\Infrastructure\Service\MealPlan\MealPlanRequirements\MealPlanRequirements;
use App\Infrastructure\Service\MealPlan\MealPlanRequirements\Range;
use App\Util\Type\Decimal;

class MealPlanPreprocessor implements MealPlanPreprocessorInterface
{
    const MIN_BIAS_MEAL_ENERGY_IN_PERCENTAGE = 10;

    private MealFinderInterface $mealFinder;

    public function __construct(MealFinderInterface $mealFinder)
    {
        $this->mealFinder = $mealFinder;
    }

    /**
     * @return array[]
     */
    public function prepareSetsOfMealsByRequirements(MealPlanRequirements $mealPlanRequirements): array
    {
        $energyRangesPerMeal = $this->createEnergyRangesPerMeal($mealPlanRequirements);
        $nutrientTypes = $mealPlanRequirements->getNutrientTypes();

        $excludedMeals = $mealPlanRequirements->getExcludedMeals();
        $dietType = $mealPlanRequirements->getDietType();
        $mealFinderRequirements = new MealFinderRequirements($excludedMeals, $dietType);

        return array_map(function (Range $range) use ($excludedMeals, $nutrientTypes, $mealFinderRequirements) {
            return $this->mealFinder->findMealsByRangeOfEnergy($range, $nutrientTypes, $mealFinderRequirements);
        }, $energyRangesPerMeal);
    }

    /**
     * @return Range[]
     */
    private function createEnergyRangesPerMeal(MealPlanRequirements $mealPlanRequirements): array
    {
        $totalEnergyDietRange = $mealPlanRequirements->getEnergyRequirement()->getRange();
        $energyMealSplit = $mealPlanRequirements->getEnergyMealPlanSplit();

        return array_map(function (int $mealEnergy) use ($totalEnergyDietRange) {
            return $this->createRange($totalEnergyDietRange, $mealEnergy);
        }, $energyMealSplit->getValues());
    }

    private function createRange(Range $totalEnergyRange, int $mealEnergyPercent): Range
    {
        $mealEnergyPercentDecimal = Decimal::createFromInt($mealEnergyPercent);
        $mealAverageEnergy = $totalEnergyRange->getAverage()->getPercentage($mealEnergyPercentDecimal);
        $bias = $mealAverageEnergy->getPercentage($this->getAcceptableEnergyBias($totalEnergyRange));

        return new Range(Decimal::sub($mealAverageEnergy, $bias), Decimal::add($mealAverageEnergy, $bias));
    }

    // If bias in total energy range is greater than 10% then return this one
    private function getAcceptableEnergyBias(Range $totalEnergyRange): Decimal
    {
        $energyBias = $totalEnergyRange->getEstimatedBiasForAverage()->mulByInt(100);
        $minEnergyBias = Decimal::createFromInt(self::MIN_BIAS_MEAL_ENERGY_IN_PERCENTAGE);

        return $energyBias->greaterThenOrEqual($minEnergyBias)
            ? $energyBias
            : Decimal::createFromInt(self::MIN_BIAS_MEAL_ENERGY_IN_PERCENTAGE);
    }
}
