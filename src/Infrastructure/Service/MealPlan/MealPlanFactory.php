<?php

declare(strict_types=1);

namespace App\Infrastructure\Service\MealPlan;

use App\Entity\MealPlan;
use App\Entity\MealPlanType;
use App\Infrastructure\Service\Solver\SolutionInterface;
use App\Repository\MealRepository;
use Ramsey\Uuid\UuidInterface;

class MealPlanFactory
{
    private MealRepository $mealRepository;

    public function __construct(MealRepository $mealRepository)
    {
        $this->mealRepository = $mealRepository;
    }

    public function create(UuidInterface $uuid, SolutionInterface $solution): MealPlan
    {
        $meals = $this->mealRepository->findMealsByIds($solution->getMealsIds());
        return new MealPlan($uuid, new MealPlanType(MealPlanType::SUCCESS) ,$meals);
    }
}