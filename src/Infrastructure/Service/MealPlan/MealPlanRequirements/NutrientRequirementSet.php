<?php

declare(strict_types=1);

namespace App\Infrastructure\Service\MealPlan\MealPlanRequirements;

use App\Util\Type\Collection;

/**
 * @extends Collection<NutrientRequirement>
 */
class NutrientRequirementSet extends Collection
{
    public function __construct(NutrientRequirement ...$values)
    {
        if (!$this->uniqueTypes(...$values)) {
            throw new \InvalidArgumentException('NutrientRequirementSet collection must has unique types');
        }

        $this->values = $values;
    }

    public function hasType(string $type): bool
    {
        return (bool) count(array_filter(
            $this->values,
            fn (NutrientRequirement $nutrientRequirement) => $nutrientRequirement->isType($type)
        ));
    }

    public function getByType(string $type): NutrientRequirement
    {
        $filteredItems = array_filter(
            $this->values,
            fn (NutrientRequirement $nutrientRequirement) => $nutrientRequirement->isType($type)
        );

        if (1 !== count($filteredItems)) {
            throw new \LogicException(sprintf('Type "%s" not exist in this set', $type));
        }

        return array_shift($filteredItems);
    }

    private function uniqueTypes(NutrientRequirement ...$nutrientRequirements): bool
    {
        $types = array_map(fn (NutrientRequirement $item) => (string) $item->getType(), $nutrientRequirements);

        return count($types) === count(array_flip($types));
    }
}
