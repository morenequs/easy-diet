<?php

declare(strict_types=1);

namespace App\Infrastructure\Service\MealPlan\MealPlanRequirements;

use App\Entity\NutrientType;
use App\Repository\DietRepository;
use App\Util\Type\Decimal;
use Webmozart\Assert\Assert;

class MealPlanRequirementsFactory
{
    private DietRepository $dietRepository;

    public function __construct(DietRepository $dietRepository)
    {
        $this->dietRepository = $dietRepository;
    }

    /**
     * @param array[] $data
     */
    public function create(array $data): MealPlanRequirements
    {
        $energyMealSplit = $this->createEnergyMealSplit($data);
        $nutrientRequirements = $this->createNutrientRequirements($data);
        $excludedMeals = $this->excludedMeals($data);
        $dietType = $this->getDietType($data);

        return new MealPlanRequirements([$energyMealSplit], $nutrientRequirements, $excludedMeals, $dietType);
    }

    /**
     * @param array[] $data
     */
    private function createEnergyMealSplit(array $data): NutrientMealPlanSplit
    {
        Assert::keyExists($data, 'energyMealSplit');
        Assert::isArray($data['energyMealSplit']);
        Assert::allInteger($data['energyMealSplit']);
        Assert::eq(array_sum($data['energyMealSplit']), 100);

        return new NutrientMealPlanSplit(new NutrientType(NutrientType::ENERGY), $data['energyMealSplit']);
    }

    /**
     * @param array[] $data
     *
     * @return NutrientRequirement[]
     */
    private function createNutrientRequirements(array $data): array
    {
        $this->assertNutrients($data);

        return array_map(function (array $nutrient) {
            return new NutrientRequirement(
                new NutrientType($nutrient['type']),
                Decimal::fromString((string) $nutrient['min']),
                Decimal::fromString((string) $nutrient['max'])
            );
        }, $data['nutrients']);
    }

    /**
     * @param array[] $data
     */
    private function assertNutrients(array $data): void
    {
        Assert::keyExists($data, 'nutrients');
        Assert::isArray($data['nutrients']);
        foreach ($data['nutrients'] as $nutrient) {
            Assert::keyExists($nutrient, 'type');
            Assert::true(NutrientType::isMealGeneratorSupportedNutrientType($nutrient['type']), 'Nutrient requirement type is not supported.');
            Assert::keyExists($nutrient, 'min');
            Assert::keyExists($nutrient, 'max');
            Assert::numeric($nutrient['min']);
            Assert::numeric($nutrient['max']);
            Assert::lessThan($nutrient['min'], $nutrient['max']);
        }

        Assert::true(
            (bool) count(
                array_filter($data['nutrients'], fn (array $nutrient) => NutrientType::ENERGY === $nutrient['type'])
            ),
            'Nutrients must have energy type defined'
        );

        Assert::uniqueValues(
            array_map(fn (array $nutrient) => $nutrient['type'], $data['nutrients'], ),
            'Defined nutrients types must be unique.'
        );
    }

    private function excludedMeals(array $data): array
    {
        if (!isset($data['excludedMeals'])) {
            return [];
        }

        Assert::allInteger($data['excludedMeals']);

        return $data['excludedMeals'];
    }

    private function getDietType(array $data): ?string
    {
        if (!isset($data['dietType'])) {
            return null;
        }

        if (!$dietType = $data['dietType']) {
            return null;
        }

        Assert::string($dietType);

        if(!$this->dietRepository->findOneBy(['slug' => $dietType])) {
            return null;
        }

        return $dietType;
    }
}
