<?php

declare(strict_types=1);

namespace App\Infrastructure\Service\MealPlan\MealPlanRequirements;

use App\Entity\NutrientType;
use App\Util\Type\Decimal;

class NutrientRequirement
{
    private NutrientType $type;

    private Range $range;

    public function __construct(NutrientType $type, Decimal $min, Decimal $max)
    {
        if (!$type->isMealGeneratorSupportedNutrient()) {
            throw new \InvalidArgumentException(sprintf('Unrecognized nutrient requirement %s', $type));
        }

        $this->type = $type;
        $this->range = new Range($min, $max);
    }

    public function isType(string $type): bool
    {
        return $this->type->isType($type);
    }

    public function getType(): NutrientType
    {
        return $this->type;
    }

    public function getMin(): Decimal
    {
        return $this->range->getMin();
    }

    public function getMax(): Decimal
    {
        return $this->range->getMax();
    }

    public function getRange(): Range
    {
        return $this->range;
    }

    public function getAverage(): Decimal
    {
        return $this->range->getAverage();
    }
}
