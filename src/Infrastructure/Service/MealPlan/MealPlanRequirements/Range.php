<?php

declare(strict_types=1);

namespace App\Infrastructure\Service\MealPlan\MealPlanRequirements;

use App\Util\Type\Decimal;

class Range
{
    private Decimal $min;
    private Decimal $max;
    private int $scale;

    public function __construct(Decimal $min, Decimal $max)
    {
        // TODO Assert min < max
        $this->min = $min;
        $this->max = $max;
        $this->scale = $min->getScale() > $max->getScale() ? $min->getScale() : $max->getScale();
    }

    public function getMin(): Decimal
    {
        return $this->min;
    }

    public function getMax(): Decimal
    {
        return $this->max;
    }

    public function getAverage(): Decimal
    {
        return Decimal::div(Decimal::add($this->min, $this->max), Decimal::createFromInt(2), $this->scale);
    }

    public function inRange(Decimal $value): bool
    {
        return $this->min->lessThenOrEqual($value) and $this->max->greaterThenOrEqual($value);
    }

    public function getEstimatedBiasForAverage(): Decimal
    {
        $diff = Decimal::sub($this->getAverage(), $this->min);

        return Decimal::div($diff, $this->getAverage());
    }
}
