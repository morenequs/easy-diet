<?php

declare(strict_types=1);

namespace App\Infrastructure\Service\MealPlan\MealPlanRequirements;

use App\Entity\NutrientType;
use App\Exception\MealPlanRequirements\MissEnergyMealPlanSplitException;
use App\Exception\MealPlanRequirements\MissingEnergyRequirementException;
use Webmozart\Assert\Assert;

class MealPlanRequirements
{
    /** @var NutrientMealPlanSplitSet<NutrientMealPlanSplit> */
    private NutrientMealPlanSplitSet $nutrientsMealPlanSplitSet;

    /** @var NutrientRequirementSet<NutrientRequirement> */
    private NutrientRequirementSet $nutrientRequirementSet;

    /** @var int[] */
    private array $excludedMeals;

    private ?string $dietType;

    /**
     * @param NutrientMealPlanSplit[] $nutrientsMealPlanSplit
     * @param NutrientRequirement[] $nutrientRequirements
     * @param int[]
     */
    public function __construct(
        array $nutrientsMealPlanSplit,
        array $nutrientRequirements,
        array $excludedMeals,
        ?string $dietType
    )
    {
        $nutrientsMealPlanSplitSet = new NutrientMealPlanSplitSet(...$nutrientsMealPlanSplit);
        if (!$nutrientsMealPlanSplitSet->hasType(NutrientType::ENERGY)) {
            throw new MissEnergyMealPlanSplitException();
        }

        $nutrientRequirementsSet = new NutrientRequirementSet(...$nutrientRequirements);
        if (!$nutrientRequirementsSet->hasType(NutrientType::ENERGY)) {
            throw new MissingEnergyRequirementException();
        }

        Assert::allInteger($excludedMeals);

        $this->nutrientsMealPlanSplitSet = $nutrientsMealPlanSplitSet;
        $this->nutrientRequirementSet = $nutrientRequirementsSet;
        $this->excludedMeals = $excludedMeals;
        $this->dietType = $dietType;
    }

    public function getEnergyMealPlanSplit(): NutrientMealPlanSplit
    {
        return $this->nutrientsMealPlanSplitSet->getByType(NutrientType::ENERGY);
    }

    public function getEnergyRequirement(): NutrientRequirement
    {
        return $this->nutrientRequirementSet->getByType(NutrientType::ENERGY);
    }

    /**
     * @return NutrientRequirement[]
     */
    public function getNutrientRequirements(): array
    {
        return $this->nutrientRequirementSet->toArray();
    }

    /**
     * @return int[]
     */
    public function getExcludedMeals(): array
    {
        return $this->excludedMeals;
    }

    /**
     * @return NutrientType[]
     */
    public function getNutrientTypes(): array
    {
        return array_map(
            fn(NutrientRequirement $nutrientRequirement) => $nutrientRequirement->getType(),
            $this->nutrientRequirementSet->toArray()
        );
    }

    public function getDietType(): ?string
    {
        return $this->dietType;
    }
}
