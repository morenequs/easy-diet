<?php

declare(strict_types=1);

namespace App\Infrastructure\Service\MealPlan\MealPlanRequirements;

use App\Util\Type\Collection;

/**
 * @extends Collection<NutrientMealPlanSplit>
 */
class NutrientMealPlanSplitSet extends Collection
{
    public function __construct(NutrientMealPlanSplit ...$values)
    {
        if (!$this->uniqueTypes(...$values)) {
            throw new \InvalidArgumentException('NutrientMealPlanSplit collection must has unique types');
        }

        $this->values = $values;
    }

    public function hasType(string $type): bool
    {
        return (bool) count(array_filter(
            $this->values,
            fn (NutrientMealPlanSplit $nutrientsMealPlanSplit) => $nutrientsMealPlanSplit->isType($type)
        ));
    }

    public function getByType(string $type): NutrientMealPlanSplit
    {
        $filteredItems = array_filter(
            $this->values,
            fn (NutrientMealPlanSplit $nutrientsMealPlanSplit) => $nutrientsMealPlanSplit->isType($type)
        );

        if (1 !== count($filteredItems)) {
            throw new \LogicException(sprintf('Type "%s" not exist in this set', $type));
        }

        return array_shift($filteredItems);
    }

    private function uniqueTypes(NutrientMealPlanSplit ...$nutrientMealPlanSplits): bool
    {
        $types = array_map(fn (NutrientMealPlanSplit $item) => (string) $item->getType(), $nutrientMealPlanSplits);

        return count($types) === count(array_flip($types));
    }
}
