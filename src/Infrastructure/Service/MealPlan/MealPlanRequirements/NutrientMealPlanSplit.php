<?php

declare(strict_types=1);

namespace App\Infrastructure\Service\MealPlan\MealPlanRequirements;

use App\Entity\NutrientType;
use Webmozart\Assert\Assert;

class NutrientMealPlanSplit
{
    private NutrientType $type;

    /** @var int[] */
    private array $values;

    /**
     * @param int[] $values
     */
    public function __construct(NutrientType $type, array $values)
    {
        Assert::allInteger($values);
        Assert::eq(100, array_sum($values));
        if (!$type->isMealGeneratorSupportedNutrient()) {
            throw new \InvalidArgumentException(sprintf('Unrecognized nutrient meal plan split type %s', $type));
        }

        $this->type = $type;
        $this->values = $values;
    }

    public function isType(string $type): bool
    {
        return $this->type->isType($type);
    }

    /**
     * @return int[]
     */
    public function getValues(): array
    {
        return $this->values;
    }

    public function getType(): NutrientType
    {
        return $this->type;
    }
}
