<?php

declare(strict_types=1);

namespace App\Infrastructure\Service\MealPlan\MealPlanResponse;

use App\Entity\Meal;
use App\Entity\MealPlan;
use App\Entity\MealProduct;
use App\Entity\NutrientType;

class MealPlanResponse implements \JsonSerializable
{
    private MealPlan $mealPlan;

    public function __construct(MealPlan $mealPlan)
    {
        $this->mealPlan = $mealPlan;
    }

    public function jsonSerialize(): array
    {
        return $this->toArray();
    }

    public function toArray(): array
    {
        $meals = array_map(function (Meal $meal) {
            return [
                'id' => $meal->getId(),
                'name' => $meal->getName(),
                'image' => $meal->getImage(),
                'steps' => $meal->getSteps(),
                'prepTime' => $meal->getPrepTime(),
                'products' => $this->getMealProducts($meal),
                'totalNutrients' => $meal->getTotalNutrientAmountData(),
            ];
        }, $this->mealPlan->getMeals()->toArray());

        return [
            'meals' => $meals,
            'totalNutrients' => $this->mealPlan->getTotalNutrientAmountData()
        ];
    }

    private function getMealProducts(Meal $meal): array
    {
        return array_map(fn (MealProduct $mealProduct) => [
            'gram' => $mealProduct->getGram(),
            'name' => $mealProduct->getProduct()->getName(),
            'id' => $mealProduct->getProduct()->getId(),
        ], $meal->getMealProducts()->toArray());
    }
}