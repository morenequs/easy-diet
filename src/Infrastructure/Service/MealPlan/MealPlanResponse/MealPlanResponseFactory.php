<?php

declare(strict_types=1);

namespace App\Infrastructure\Service\MealPlan\MealPlanResponse;

use App\Entity\MealPlan;

class MealPlanResponseFactory
{
    public function create(MealPlan $mealPlan): MealPlanResponse
    {
        return new MealPlanResponse($mealPlan);
    }
}