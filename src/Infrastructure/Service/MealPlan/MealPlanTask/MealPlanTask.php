<?php

declare(strict_types=1);

namespace App\Infrastructure\Service\MealPlan\MealPlanTask;

class MealPlanTask
{
    /** @var array[] */
    private array $requirements;

    /** @var array[] */
    private array $sets;

    /**
     * @param array[] $requirements
     * @param array[] $sets
     */
    public function __construct(array $requirements, array $sets)
    {
        $this->requirements = $requirements;
        $this->sets = $sets;
    }

    public function getArray(): array
    {
        return [
            'requirements' => $this->requirements,
            'sets' => $this->sets,
        ];
    }

    public function getJson(): string
    {
        $json = json_encode($this->getArray());

        if (false === $json) {
            throw new \RuntimeException('MealPlanTask json encode failed.');
        }

        return $json;
    }
}
