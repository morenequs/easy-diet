<?php

declare(strict_types=1);

namespace App\Infrastructure\Service\MealPlan\MealPlanTask;

use App\Infrastructure\Service\MealPlan\MealPlanRequirements\MealPlanRequirements;
use App\Infrastructure\Service\MealPlan\MealPlanRequirements\NutrientRequirement;

class MealPlanTaskFactory
{
    /**
     * @param array[] $setsOfMeal
     */
    public function create(MealPlanRequirements $mealPlanRequirements, array $setsOfMeal): MealPlanTask
    {
        $requirements = $this->processRequirements($mealPlanRequirements);
        $sets = $this->processSetsOfMeals($setsOfMeal);

        return new MealPlanTask($requirements, $sets);
    }

    /**
     * @return array[]
     */
    private function processRequirements(MealPlanRequirements $mealPlanRequirements): array
    {
        $requirements = [];

        /**
         * @var NutrientRequirement $nutrientRequirements
         */
        foreach ($mealPlanRequirements->getNutrientRequirements() as $nutrientRequirements) {
            $requirements[(string) $nutrientRequirements->getType()] = [
                'min' => (string) $nutrientRequirements->getMin(),
                'max' => (string) $nutrientRequirements->getMax(),
            ];
        }

        return $requirements;
    }

    /**
     * @param array[] $setsOfMeal
     *
     * @return array[]
     */
    private function processSetsOfMeals(array $setsOfMeal): array
    {
        return array_map(function (array $set) {
            return array_map(function (array $meal) {
                $columns = [];
                foreach ($meal as $columnName => $value) {
                    $columns[$columnName] = (string) $value;
                }

                return $columns;
            }, $set);
        }, $setsOfMeal);
    }
}
