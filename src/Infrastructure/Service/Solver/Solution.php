<?php

declare(strict_types=1);

namespace App\Infrastructure\Service\Solver;

use App\Entity\MealPlanType;
use Webmozart\Assert\Assert;

class Solution implements SolutionInterface
{
    private string $type;

    private array $mealsIds;

    public function __construct(string $type, array $mealsId)
    {
        if (!MealPlanType::isValidType($type)) {
            throw new \InvalidArgumentException(sprintf('Not recognized meal plan type "%s"', $type));
        }
        Assert::allInteger($mealsId);

        $this->mealsIds = $mealsId;
        $this->type = $type;
    }

    public function getMealsIds(): array
    {
        return $this->mealsIds;
    }

    public function getType(): string
    {
        return $this->type;
    }
}