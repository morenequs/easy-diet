<?php

declare(strict_types=1);

namespace App\Infrastructure\Service\Solver;

interface SolutionInterface
{
    public function getType(): string;

    public function getMealsIds(): array;
}