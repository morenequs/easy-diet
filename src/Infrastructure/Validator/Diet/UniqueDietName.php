<?php

declare(strict_types=1);

namespace App\Infrastructure\Validator\Diet;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @Target({"CLASS", "ANNOTATION"})
 */
class UniqueDietName extends Constraint
{
    public $message = 'unique';

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}