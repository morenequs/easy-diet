<?php

declare(strict_types=1);

namespace App\Infrastructure\Validator\Diet;

use App\Form\Model\DietModel;
use App\Repository\DietRepository;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class UniqueDietNameValidator extends ConstraintValidator
{
    private SluggerInterface $slugger;
    private DietRepository $dietRepository;

    public function __construct(SluggerInterface $slugger, DietRepository $dietRepository)
    {
        $this->slugger = $slugger;
        $this->dietRepository = $dietRepository;
    }

    public function validate($dietModel, Constraint $constraint)
    {
        if (null === $dietModel) {
            return;
        }

        if (!$dietModel instanceof DietModel) {
            return;
        }

        if (!$dietName = $dietModel->getName()) {
            return;
        }

        $slug = $this->slugger->slug($dietName)->lower()->toString();
        if(!$diet = $this->dietRepository->findOneBy(['slug' => $slug])) {
            return;
        }

        if ($dietModel->getId() === $diet->getId()) {
            return;
        }

        $this->context->buildViolation($constraint->message)
            ->atPath('name')
            ->addViolation();
    }
}