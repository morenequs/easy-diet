<?php

declare(strict_types=1);

namespace App\Infrastructure\Validator\Product;

use App\Form\Model\ProductModel;
use App\Repository\ProductRepository;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class UniqueProductNameValidator extends ConstraintValidator
{
    private SluggerInterface $slugger;
    private ProductRepository $productRepository;

    public function __construct(SluggerInterface $slugger, ProductRepository $productRepository)
    {
        $this->slugger = $slugger;
        $this->productRepository = $productRepository;
    }

    public function validate($productModel, Constraint $constraint)
    {
        if (null === $productModel) {
            return;
        }

        if (!$productModel instanceof ProductModel) {
            return;
        }

        if (!$productName = $productModel->getName()) {
            return;
        }

        $slug = $this->slugger->slug($productName)->lower()->toString();
        if(!$product = $this->productRepository->findOneBy(['slug' => $slug])) {
            return;
        }

        if ($productModel->getId() === $product->getId()) {
            return;
        }

        $this->context->buildViolation($constraint->message)
            ->atPath('name')
            ->addViolation();
    }
}