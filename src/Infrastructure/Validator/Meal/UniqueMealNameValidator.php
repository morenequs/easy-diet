<?php

declare(strict_types=1);

namespace App\Infrastructure\Validator\Meal;

use App\Form\Model\MealModel;
use App\Repository\MealRepository;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class UniqueMealNameValidator extends ConstraintValidator
{
    private SluggerInterface $slugger;
    private MealRepository $mealRepository;

    public function __construct(SluggerInterface $slugger, MealRepository $mealRepository)
    {
        $this->slugger = $slugger;
        $this->mealRepository = $mealRepository;
    }

    public function validate($mealModel, Constraint $constraint)
    {
        if (null === $mealModel) {
            return;
        }

        if (!$mealModel instanceof MealModel) {
            return;
        }

        if (!$mealName = $mealModel->getName()) {
            return;
        }

        $slug = $this->slugger->slug($mealName)->lower()->toString();
        if(!$diet = $this->mealRepository->findOneBy(['slug' => $slug])) {
            return;
        }

        if ($mealModel->getId() === $diet->getId()) {
            return;
        }

        $this->context->buildViolation($constraint->message)
            ->atPath('name')
            ->addViolation();
    }
}