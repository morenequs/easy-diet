<?php

declare(strict_types=1);

namespace App\Infrastructure\Serializer;

use App\Infrastructure\Service\MealPlan\MealPlanTask\MealPlanTask;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;

class MealPlanTaskNormalizer implements ContextAwareNormalizerInterface
{
    public function normalize($object, string $format = null, array $context = [])
    {
        return $object->getArray();
    }

    public function supportsNormalization($data, string $format = null, array $context = [])
    {
        return $data instanceof MealPlanTask;
    }
}