<?php

declare(strict_types=1);

namespace App\Infrastructure\Serializer;

use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;

class UuidNormalizer implements ContextAwareNormalizerInterface
{
    public function normalize($object, string $format = null, array $context = [])
    {
        return (string) $object;
    }

    public function supportsNormalization($data, string $format = null, array $context = [])
    {
        return $data instanceof UuidInterface;
    }
}