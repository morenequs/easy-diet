<?php

declare(strict_types=1);

namespace App\Infrastructure\Serializer;

use App\Infrastructure\Service\Solver\Solution;
use App\Infrastructure\Service\Solver\SolutionInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

class SolutionDenormalizer implements DenormalizerInterface
{
    public function denormalize($data, string $type, string $format = null, array $context = [])
    {
        return new Solution($data['mealsIds']);
    }

    public function supportsDenormalization($data, string $type, string $format = null)
    {
        return $data === SolutionInterface::class;
    }
}