<?php

declare(strict_types=1);

namespace App\Infrastructure\Serializer;

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

class UuidDenormalizer implements DenormalizerInterface
{
    public function denormalize($data, string $type, string $format = null, array $context = [])
    {
        if (!$this->isValid($data)) {
            throw new \UnexpectedValueException('Expected a valid Uuid.');
        }

        if (null === $data) {
            return null;
        }

        return Uuid::fromString($data);
    }

    public function supportsDenormalization($data, string $type, string $format = null)
    {
        return (Uuid::class === $type || UuidInterface::class === $type);
    }

    private function isValid($data): bool
    {
        return $data === null || (is_string($data) && Uuid::isValid($data));
    }
}