<?php

declare(strict_types=1);

namespace App\Infrastructure\Serializer;

use App\Infrastructure\Service\Solver\SolutionInterface;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;

class SolutionNormalizer implements ContextAwareNormalizerInterface
{
    public function normalize($object, string $format = null, array $context = [])
    {
        return ['mealsIds' => $object->getMealsIds()];
    }

    public function supportsNormalization($data, string $format = null, array $context = [])
    {
        return $data instanceof SolutionInterface;
    }
}