<?php

declare(strict_types=1);

namespace App\Infrastructure\Serializer\Messenger;

use App\Infrastructure\Command\MealPlan\CreateMealPlanFromSolutionCommand;
use App\Infrastructure\Service\Solver\Solution;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Exception\MessageDecodingFailedException;
use Symfony\Component\Messenger\Transport\Serialization\SerializerInterface;
use Symfony\Component\Messenger\Exception\RuntimeException;
use Webmozart\Assert\Assert;

class ExternalSystemMessageSerializer implements SerializerInterface
{
    const TYPE_MEAL_PLAN_SOLUTION = 'mealPlanSolution';

    const TYPE_TO_CLASS_MAP = [
        self::TYPE_MEAL_PLAN_SOLUTION => CreateMealPlanFromSolutionCommand::class
    ];

    public function decode(array $encodedEnvelope): Envelope
    {
        $body = $encodedEnvelope['body'];
        $headers = $encodedEnvelope['headers'];

        if (!isset($headers['type'])) {
            throw new MessageDecodingFailedException('Encoded message must has defined a type in headers.');
        }

        $type = $headers['type'];
        if (!in_array($type, array_keys(self::TYPE_TO_CLASS_MAP))) {
            throw new MessageDecodingFailedException(sprintf('Type "%s" is not supported for this transport.', $type));
        }

        $data = json_decode($body, true);
        if (null === $data) {
            throw new MessageDecodingFailedException('Not valid JSON message.');
        }

        $envelope = null;
        switch ($type) {
            case self::TYPE_MEAL_PLAN_SOLUTION:
                $envelope = $this->decodeMealPlanSolutionMessages($data);
        }

        if (is_null($envelope)) {
            throw new \LogicException('Message can not be null.');
        }

        $stamps = [];
        if (isset($headers['stamps'])) {
            $stamps = unserialize($headers['stamps']);
        }
        $envelope = $envelope->with(... $stamps);

        return $envelope;
    }

    public function encode(Envelope $envelope): array
    {
        $message = $envelope->getMessage();

        if (!in_array(get_class($message), self::TYPE_TO_CLASS_MAP)) {
            throw new RuntimeException('This message is not supported in this transport.');
        }

        $encodedEnvelope = [];
        switch (get_class($message)) {
            case CreateMealPlanFromSolutionCommand::class:
                $encodedEnvelope = $this->encodeMealPlanSolutionMessages($message);
        }

        $allStamps = [];
        foreach ($envelope->all() as $stamps) {
            $allStamps = array_merge($allStamps, $stamps);
        }

        $encodedEnvelope['headers']['stamps'] = serialize($allStamps);

        return $encodedEnvelope;
    }

    private function decodeMealPlanSolutionMessages(array $data): Envelope
    {
        if (!isset($data['uuid'])) {
            throw new \InvalidArgumentException(
                sprintf('Missing "uuid" body key for "%s" type', self::TYPE_MEAL_PLAN_SOLUTION)
            );
        }

        if (!isset($data['mealPlanIds'])) {
            throw new \InvalidArgumentException(
                sprintf('Missing "mealPlanIds" body key for "%s" type', self::TYPE_MEAL_PLAN_SOLUTION)
            );
        }

        if (!isset($data['type'])) {
            throw new \InvalidArgumentException(
                sprintf('Missing "type" body key for "%s" type', self::TYPE_MEAL_PLAN_SOLUTION)
            );
        }

        Assert::allInteger($data['mealPlanIds']);

        return new Envelope(
            new CreateMealPlanFromSolutionCommand(
                Uuid::fromString($data['uuid']),
                new Solution($data['type'], $data['mealPlanIds']))
        );
    }

    private function encodeMealPlanSolutionMessages(CreateMealPlanFromSolutionCommand $message): array
    {
        $data = [
            'uuid' => $message->getUuid(),
            'mealPlanIds' => $message->getSolution()->getMealsIds(),
            'type' => $message->getSolution()->getType(),
        ];

        $headers = ['type' => self::TYPE_MEAL_PLAN_SOLUTION];

        return [
            'body' => json_encode($data),
            'headers' => $headers
        ];
    }
}