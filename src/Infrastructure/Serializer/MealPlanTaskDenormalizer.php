<?php

declare(strict_types=1);

namespace App\Infrastructure\Serializer;

use App\Infrastructure\Service\MealPlan\MealPlanTask\MealPlanTask;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

class MealPlanTaskDenormalizer implements DenormalizerInterface
{
    public function denormalize($data, string $type, string $format = null, array $context = [])
    {
        $array = json_decode($data, true);
        if (null === $array) {
            throw new \UnexpectedValueException('Expected a valid MealPlanTask.');
        };

        return new MealPlanTask($array['requirements'], $array['sets']);
    }

    public function supportsDenormalization($data, string $type, string $format = null)
    {
        return $type === MealPlanTask::class;
    }
}