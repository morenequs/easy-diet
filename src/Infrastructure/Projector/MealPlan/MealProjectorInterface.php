<?php

declare(strict_types=1);

namespace App\Infrastructure\Projector\MealPlan;

interface MealProjectorInterface
{
    public function refreshAllFromDatabase(): void;
}