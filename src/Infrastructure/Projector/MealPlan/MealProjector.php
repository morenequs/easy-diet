<?php

declare(strict_types=1);

namespace App\Infrastructure\Projector\MealPlan;

use App\Util\InsertBulk;
use Doctrine\DBAL\Connection;

class MealProjector implements MealProjectorInterface
{
    const TABLE_NAME = 'projection_meal';

    private Connection $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function refreshAllFromDatabase(): void
    {
        $this->clear();
        $results = $this->getResults();
        $this->insert($results);
    }

    private function getResults(array $mealIds = null): array
    {
        $whereStatement = '';
        if ($mealIds) {
            $whereStatement = 'WHERE meal.id IN (' . implode(',', $mealIds) . ')' . PHP_EOL;
        }

        $sql = "
            SELECT 
                meal.id,
                SUM(CASE
                    WHEN nutrient.type = 'energy' THEN meal_product.gram * nutrient.amount / 100
                    ELSE 0
                END) AS energy,
                SUM(CASE
                    WHEN nutrient.type = 'proteins' THEN meal_product.gram * nutrient.amount / 100
                    ELSE 0
                END) AS proteins,
                SUM(CASE
                    WHEN nutrient.type = 'fats' THEN meal_product.gram * nutrient.amount / 100
                    ELSE 0
                END) AS fats,
                SUM(CASE
                    WHEN nutrient.type = 'carbs' THEN meal_product.gram * nutrient.amount / 100
                    ELSE 0
                END) AS carbs
            FROM
                meal
                    LEFT JOIN
                meal_product ON meal.id = meal_product.meal_id
                    LEFT JOIN
                product ON meal_product.product_id = product.id
                    LEFT JOIN
                nutrient ON nutrient.product_id = product.id
            $whereStatement
            GROUP BY meal.id
            ;
        ";

        return $this->connection->fetchAll($sql);
    }

    private function clear(): void
    {
        $this->connection->exec('TRUNCATE ' . self::TABLE_NAME);
    }

    private function insert(array $rows): void
    {
        $insertBulk = new InsertBulk($this->connection, self::TABLE_NAME, ['id', 'energy', 'proteins', 'fats', 'carbs']);
        $insertBulk->addRows($rows);
        $insertBulk->executeInsert();
    }
}