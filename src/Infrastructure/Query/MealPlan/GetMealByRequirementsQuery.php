<?php

declare(strict_types=1);

namespace App\Infrastructure\Query\MealPlan;

use App\Infrastructure\Service\MealPlan\MealPlanRequirements\MealPlanRequirements;
use App\Shared\System\Query\QueryInterface;

class GetMealByRequirementsQuery implements QueryInterface
{
    private MealPlanRequirements $requirements;

    public function __construct(MealPlanRequirements $requirements)
    {
        $this->requirements = $requirements;
    }

    public function getRequirements(): MealPlanRequirements
    {
        return $this->requirements;
    }
}