<?php

declare(strict_types=1);

namespace App\Infrastructure\Query\MealPlan;

use App\Shared\System\Query\QueryInterface;

class GetMealPlanByIdQuery implements QueryInterface
{
    private string $id;

    public function __construct(string $id)
    {
        $this->id = $id;
    }

    public function getId(): string
    {
        return $this->id;
    }
}