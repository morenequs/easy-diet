<?php

declare(strict_types=1);

namespace App\Infrastructure\Query\Meal;

use App\Shared\System\Query\QueryInterface;

class GetMealByIdQuery implements QueryInterface
{
    private $mealId;

    public function __construct($mealId)
    {
        $this->mealId = $mealId;
    }

    public function getMealId()
    {
        return $this->mealId;
    }
}