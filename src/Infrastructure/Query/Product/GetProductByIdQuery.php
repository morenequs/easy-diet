<?php

declare(strict_types=1);

namespace App\Infrastructure\Query\Product;

use App\Shared\System\Query\QueryInterface;

class GetProductByIdQuery implements QueryInterface
{
    private $productId;

    public function __construct($productId)
    {
        $this->productId = $productId;
    }

    public function getProductId()
    {
        return $this->productId;
    }
}