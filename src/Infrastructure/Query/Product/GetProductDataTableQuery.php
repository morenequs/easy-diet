<?php

declare(strict_types=1);

namespace App\Infrastructure\Query\Product;

use App\Shared\System\Query\QueryInterface;

class GetProductDataTableQuery implements QueryInterface
{
    private array $params;

    public function __construct(array $params)
    {
        $this->params = $params;
    }

    public function getParams(): array
    {
        return $this->params;
    }
}