<?php

declare(strict_types=1);

namespace App\Infrastructure\Query\Product;

use App\Shared\System\Query\QueryInterface;

class SearchProductsByNameQuery implements QueryInterface
{
    private string $query;
    private int $limit;

    public function __construct(string $query, int $limit)
    {
        $this->query = $query;
        $this->limit = $limit;
    }

    public function getQuery(): string
    {
        return $this->query;
    }

    public function getLimit(): int
    {
        return $this->limit;
    }
}