<?php

declare(strict_types=1);

namespace App\Infrastructure\Query\Diet;

use App\Shared\System\Query\QueryInterface;

class GetDietByIdQuery implements QueryInterface
{
    private $dietId;

    public function __construct($dietId)
    {
        $this->dietId = $dietId;
    }

    public function getDietId()
    {
        return $this->dietId;
    }
}