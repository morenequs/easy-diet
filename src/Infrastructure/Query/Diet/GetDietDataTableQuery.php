<?php

declare(strict_types=1);

namespace App\Infrastructure\Query\Diet;

use App\Shared\System\Query\QueryInterface;

class GetDietDataTableQuery implements QueryInterface
{
    private array $params;

    public function __construct(array $params)
    {
        $this->params = $params;
    }

    public function getParams(): array
    {
        return $this->params;
    }
}