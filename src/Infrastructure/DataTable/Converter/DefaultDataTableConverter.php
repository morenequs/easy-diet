<?php

declare(strict_types=1);

namespace App\Infrastructure\DataTable\Converter;

class DefaultDataTableConverter implements DataTableConverterInterface
{
    public function convert(array $data): array
    {
        return $data;
    }
}