<?php

namespace App\Infrastructure\DataTable\Converter;

interface DataTableConverterInterface
{
    public function convert(array $data): array;
}