<?php

namespace App\Infrastructure\DataTable;

interface DataSourceInterface
{
    public function getDataSource(array $params, string $entity): array;
}