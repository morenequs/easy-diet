<?php

declare(strict_types=1);

namespace App\Infrastructure\DataTable;

use App\Infrastructure\DataTable\Converter\DataTableConverterInterface;
use App\Repository\DataTableRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;

class DataSource implements DataSourceInterface
{
    private EntityManagerInterface $em;
    private DataTableConverterInterface $dataTableConverter;

    public function __construct(EntityManagerInterface $em, DataTableConverterInterface $dataTableConverter)
    {
        $this->em = $em;
        $this->dataTableConverter = $dataTableConverter;
    }

    public function getDataSource(array $params, string $entity): array
    {
        $filter = $params['search']['value'];

        $repository = $this->em->getRepository($entity);

        if (!$repository instanceof DataTableRepositoryInterface) {
            throw new \RuntimeException(
                sprintf('Repository of %s entity does not implements IDataTableRepository', $entity)
            );
        }

        $recordsTotal = $repository->count([]);
        $data = $repository->findByParams($filter, intval($params['start']), intval($params['length']));
        $recordsFiltered = strlen($filter) ? $repository->countFiltered($filter) : $recordsTotal;
        $data = $this->dataTableConverter->convert($data);

        return [
            'recordsFiltered' => $recordsFiltered,
            'recordsTotal' => $recordsTotal,
            'data' => $data
        ];
    }
}