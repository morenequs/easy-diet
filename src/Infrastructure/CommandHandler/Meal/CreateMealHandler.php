<?php

declare(strict_types=1);

namespace App\Infrastructure\CommandHandler\Meal;

use App\Entity\Meal;
use App\Form\Model\Transformer\MealModelTransformer;
use App\Infrastructure\Command\Meal\CreateMealCommand;
use App\Infrastructure\Projector\MealPlan\MealProjectorInterface;
use App\Shared\System\Command\CommandHandlerInterface;
use Doctrine\ORM\EntityManagerInterface;

class CreateMealHandler implements CommandHandlerInterface
{
    private EntityManagerInterface $em;
    private MealModelTransformer $mealModelTransformer;
    private MealProjectorInterface $mealProjector;

    public function __construct(
        EntityManagerInterface $em,
        MealModelTransformer $mealModelTransformer,
        MealProjectorInterface $mealProjector
    )
    {
        $this->em = $em;
        $this->mealModelTransformer = $mealModelTransformer;
        $this->mealProjector = $mealProjector;
    }

    public function __invoke(CreateMealCommand $createMealCommand): void
    {
        $meal = new Meal();
        $this->mealModelTransformer->transformModelToMeal($createMealCommand->getMealModel(), $meal);

        $this->em->persist($meal);
        $this->em->flush();

        $this->mealProjector->refreshAllFromDatabase();
    }
}