<?php

declare(strict_types=1);

namespace App\Infrastructure\CommandHandler\Meal;

use App\Form\Model\Transformer\MealModelTransformer;
use App\Infrastructure\Command\Meal\EditMealCommand;
use App\Infrastructure\Projector\MealPlan\MealProjectorInterface;
use App\Repository\MealRepository;
use App\Shared\System\Command\CommandHandlerInterface;
use Doctrine\ORM\EntityManagerInterface;

class EditMealHandler implements CommandHandlerInterface
{
    private MealRepository $mealRepository;
    private MealModelTransformer $mealModelTransformer;
    private EntityManagerInterface $em;
    private MealProjectorInterface $mealProjector;

    public function __construct(
        MealRepository $mealRepository,
        MealModelTransformer $mealModelTransformer,
        EntityManagerInterface $em,
        MealProjectorInterface $mealProjector
    )
    {
        $this->mealRepository = $mealRepository;
        $this->mealModelTransformer = $mealModelTransformer;
        $this->em = $em;
        $this->mealProjector = $mealProjector;
    }

    public function __invoke(EditMealCommand $editMealCommand): void
    {
        if(!$meal = $this->mealRepository->findOneBy(['id' => $editMealCommand->getMealId()])) {
            throw new \RuntimeException('Meal not found');
        }

        $this->mealModelTransformer->transformModelToMeal($editMealCommand->getMealModel(), $meal);

        $this->em->persist($meal);
        $this->em->flush();

        $this->mealProjector->refreshAllFromDatabase();
    }
}