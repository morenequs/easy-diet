<?php

declare(strict_types=1);

namespace App\Infrastructure\CommandHandler\Product;

use App\Entity\Product;
use App\Form\Model\Transformer\ProductModelTransformer;
use App\Infrastructure\Command\Product\CreateProductCommand;
use App\Shared\System\Command\CommandHandlerInterface;
use Doctrine\ORM\EntityManagerInterface;

class CreateProductHandler implements CommandHandlerInterface
{
    private ProductModelTransformer $productModelTransformer;
    private EntityManagerInterface $em;

    public function __construct(ProductModelTransformer $productModelTransformer, EntityManagerInterface $em)
    {
        $this->productModelTransformer = $productModelTransformer;
        $this->em = $em;
    }

    public function __invoke(CreateProductCommand $createProductCommand): void
    {
        $product = new Product();
        $this->productModelTransformer->transformModelToProduct($createProductCommand->getProductModel(), $product);
        $this->em->persist($product);
        $this->em->flush();
    }
}