<?php

declare(strict_types=1);

namespace App\Infrastructure\CommandHandler\Product;

use App\Form\Model\Transformer\ProductModelTransformer;
use App\Infrastructure\Command\Product\EditProductCommand;
use App\Infrastructure\Projector\MealPlan\MealProjectorInterface;
use App\Repository\ProductRepository;
use App\Shared\System\Command\CommandHandlerInterface;
use Doctrine\ORM\EntityManagerInterface;

class EditProductHandler implements CommandHandlerInterface
{
    private ProductModelTransformer $productModelTransformer;
    private EntityManagerInterface $em;
    private ProductRepository $productRepository;
    private MealProjectorInterface $mealProjector;

    public function __construct(
        ProductModelTransformer $productModelTransformer,
        EntityManagerInterface $em,
        ProductRepository $productRepository,
        MealProjectorInterface $mealProjector
    )
    {
        $this->productModelTransformer = $productModelTransformer;
        $this->em = $em;
        $this->productRepository = $productRepository;
        $this->mealProjector = $mealProjector;
    }

    public function __invoke(EditProductCommand $editProductCommand): void
    {
        if(!$product = $this->productRepository->findOneBy(['id' => $editProductCommand->getProductId()])) {
            throw new \RuntimeException('Product not found');
        }

        $this->productModelTransformer->transformModelToProduct($editProductCommand->getProductModel(), $product);
        $this->em->persist($product);
        $this->em->flush();

        $this->mealProjector->refreshAllFromDatabase();
    }
}