<?php

declare(strict_types=1);

namespace App\Infrastructure\CommandHandler\Diet;

use App\Entity\Diet;
use App\Form\Model\Transformer\DietModelTransformer;
use App\Infrastructure\Command\Diet\CreateDietCommand;
use App\Shared\System\Command\CommandHandlerInterface;
use Doctrine\ORM\EntityManagerInterface;

class CreateDietHandler implements CommandHandlerInterface
{
    private EntityManagerInterface $em;
    private DietModelTransformer $dietModelTransformer;

    public function __construct(EntityManagerInterface $em, DietModelTransformer $dietModelTransformer)
    {
        $this->em = $em;
        $this->dietModelTransformer = $dietModelTransformer;
    }

    public function __invoke(CreateDietCommand $createDietCommand): void
    {
        $diet = new Diet();

        $this->dietModelTransformer->transformModelToDiet($createDietCommand->getDietModel(), $diet);
        $this->em->persist($diet);
        $this->em->flush();
    }
}