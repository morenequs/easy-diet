<?php

declare(strict_types=1);

namespace App\Infrastructure\CommandHandler\Diet;

use App\Form\Model\Transformer\DietModelTransformer;
use App\Infrastructure\Command\Diet\EditDietCommand;
use App\Repository\DietRepository;
use App\Shared\System\Command\CommandHandlerInterface;
use Doctrine\ORM\EntityManagerInterface;

class EditDietHandler implements CommandHandlerInterface
{
    private EntityManagerInterface $em;
    private DietModelTransformer $dietModelTransformer;
    private DietRepository $dietRepository;

    public function __construct(
        EntityManagerInterface $em,
        DietModelTransformer $dietModelTransformer,
        DietRepository $dietRepository
    )
    {
        $this->em = $em;
        $this->dietModelTransformer = $dietModelTransformer;
        $this->dietRepository = $dietRepository;
    }

    public function __invoke(EditDietCommand $editDietCommand): void
    {
        if(!$diet = $this->dietRepository->findOneBy(['id' => $editDietCommand->getDietId()])) {
            throw new \RuntimeException('Diet not found');
        }

        $this->dietModelTransformer->transformModelToDiet($editDietCommand->getDietModel(), $diet);

        $this->em->persist($diet);
        $this->em->flush();
    }
}