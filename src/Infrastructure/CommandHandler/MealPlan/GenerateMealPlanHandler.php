<?php

declare(strict_types=1);

namespace App\Infrastructure\CommandHandler\MealPlan;

use App\Entity\MealPlan;
use App\Entity\MealPlanType;
use App\Infrastructure\Command\MealPlan\GenerateMealPlanCommand;
use App\Infrastructure\Command\MealPlan\SolveMealPlanTaskCommand;
use App\Infrastructure\Service\MealPlan\MealPlanPreprocessor\MealPlanPreprocessorInterface;
use App\Infrastructure\Service\MealPlan\MealPlanTask\MealPlanTaskFactory;
use App\Shared\System\Command\CommandHandlerInterface;
use App\Shared\System\SystemInterface;
use Doctrine\ORM\EntityManagerInterface;

class GenerateMealPlanHandler implements CommandHandlerInterface
{
    private MealPlanPreprocessorInterface $mealPlanPreprocessor;
    private MealPlanTaskFactory $mealPlanTaskFactory;
    private SystemInterface $system;
    private EntityManagerInterface $em;

    public function __construct(
        MealPlanPreprocessorInterface $mealPlanPreprocessor,
        MealPlanTaskFactory $mealPlanTaskFactory,
        EntityManagerInterface $em,
        SystemInterface $system
    )
    {
        $this->mealPlanPreprocessor = $mealPlanPreprocessor;
        $this->mealPlanTaskFactory = $mealPlanTaskFactory;
        $this->system = $system;
        $this->em = $em;
    }

    public function __invoke(GenerateMealPlanCommand $command): void
    {
        $mealPlanRequirements = $command->getMealPlanRequirements();

        $setsOfMeals = $this->mealPlanPreprocessor->prepareSetsOfMealsByRequirements($mealPlanRequirements);
        $mealPlanTask = $this->mealPlanTaskFactory->create($mealPlanRequirements, $setsOfMeals);

        $mealPlan = new MealPlan($command->getUuid(), new MealPlanType(MealPlanType::PENDING));
        $this->em->persist($mealPlan);
        $this->em->flush();

        $solveMealPlanTaskCommand = new SolveMealPlanTaskCommand($command->getUuid(), $mealPlanTask);
        $this->system->dispatch($solveMealPlanTaskCommand);
    }
}