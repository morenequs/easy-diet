<?php

declare(strict_types=1);

namespace App\Infrastructure\CommandHandler\MealPlan;

use App\Entity\MealPlan;
use App\Entity\MealPlanType;
use App\Infrastructure\Command\MealPlan\CreateMealPlanFromSolutionCommand;
use App\Infrastructure\Service\MealPlan\MealPlanFactory;
use App\Infrastructure\Service\Solver\SolutionInterface;
use App\Repository\MealPlanRepository;
use App\Repository\MealRepository;
use App\Shared\System\Command\CommandHandlerInterface;
use Doctrine\ORM\EntityManagerInterface;

class CreateMealPlanFromSolutionHandler implements CommandHandlerInterface
{
    private MealPlanFactory $mealPlanFactory;
    private EntityManagerInterface $em;
    private MealPlanRepository $mealPlanRepository;
    private MealRepository $mealRepository;

    public function __construct(
        MealPlanFactory $mealPlanFactory,
        EntityManagerInterface $em,
        MealPlanRepository $mealPlanRepository,
        MealRepository $mealRepository
    )
    {
        $this->mealPlanFactory = $mealPlanFactory;
        $this->em = $em;
        $this->mealPlanRepository = $mealPlanRepository;
        $this->mealRepository = $mealRepository;
    }

    public function __invoke(CreateMealPlanFromSolutionCommand $command): void
    {
        $mealPlan = $this->mealPlanRepository->findOneBy(['id' => $command->getUuid()]);
        if (!$mealPlan) {
            throw new \RuntimeException('Meal plan not found.');
        }

        $this->completeMealPlan($mealPlan, $command->getSolution());
        $this->em->persist($mealPlan);
        $this->em->flush();
    }

    private function completeMealPlan(MealPlan $mealPlan, SolutionInterface $solution): void
    {
        $meals = $this->mealRepository->findMealsByIds($solution->getMealsIds());
        foreach ($meals as $meal) {
            $mealPlan->addMeal($meal);
        }
        $mealPlan->setType(new MealPlanType($solution->getType()));
        $mealPlan->setUpdatedAt(new \DateTime('now'));
    }
}