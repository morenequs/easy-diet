<?php

declare(strict_types=1);

namespace App\Infrastructure\Command\MealPlan;

use App\Infrastructure\Service\MealPlan\MealPlanTask\MealPlanTask;
use App\Shared\System\Command\CommandInterface;
use Ramsey\Uuid\UuidInterface;

class SolveMealPlanTaskCommand implements CommandInterface
{
    private UuidInterface $uuid;
    private MealPlanTask $mealPlanTask;

    public function __construct(UuidInterface $uuid, MealPlanTask $mealPlanTask)
    {
        $this->uuid = $uuid;
        $this->mealPlanTask = $mealPlanTask;
    }

    public function getUuid(): UuidInterface
    {
        return $this->uuid;
    }

    public function getMealPlanTask(): MealPlanTask
    {
        return $this->mealPlanTask;
    }
}