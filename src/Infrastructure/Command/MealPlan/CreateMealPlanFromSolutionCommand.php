<?php

declare(strict_types=1);

namespace App\Infrastructure\Command\MealPlan;

use App\Infrastructure\Service\Solver\SolutionInterface;
use App\Shared\System\Command\CommandInterface;
use Ramsey\Uuid\UuidInterface;

class CreateMealPlanFromSolutionCommand implements CommandInterface
{
    private UuidInterface $uuid;
    private SolutionInterface $solution;

    public function __construct(UuidInterface $uuid, SolutionInterface $solution)
    {
        $this->uuid = $uuid;
        $this->solution = $solution;
    }

    public function getUuid(): UuidInterface
    {
        return $this->uuid;
    }

    public function getSolution(): SolutionInterface
    {
        return $this->solution;
    }
}