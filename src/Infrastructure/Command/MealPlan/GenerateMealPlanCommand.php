<?php

declare(strict_types=1);

namespace App\Infrastructure\Command\MealPlan;

use App\Infrastructure\Service\MealPlan\MealPlanRequirements\MealPlanRequirements;
use App\Shared\System\Command\CommandInterface;
use Ramsey\Uuid\UuidInterface;

class GenerateMealPlanCommand implements CommandInterface
{
    private UuidInterface $uuid;
    private MealPlanRequirements $mealPlanRequirements;

    public function __construct(UuidInterface $uuid, MealPlanRequirements $mealPlanRequirements)
    {
        $this->uuid = $uuid;
        $this->mealPlanRequirements = $mealPlanRequirements;
    }

    public function getMealPlanRequirements(): MealPlanRequirements
    {
        return $this->mealPlanRequirements;
    }

    public function getUuid(): UuidInterface
    {
        return $this->uuid;
    }
}