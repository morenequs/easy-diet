<?php

declare(strict_types=1);

namespace App\Infrastructure\Command\Product;

use App\Form\Model\ProductModel;
use App\Shared\System\Command\CommandInterface;

class EditProductCommand implements CommandInterface
{
    private $productId;

    private ProductModel $productModel;

    public function __construct($productId, ProductModel $productModel)
    {
        $this->productId = $productId;
        $this->productModel = $productModel;
    }

    public function getProductId()
    {
        return $this->productId;
    }

    public function getProductModel(): ProductModel
    {
        return $this->productModel;
    }
}