<?php

declare(strict_types=1);

namespace App\Infrastructure\Command\Product;

use App\Form\Model\ProductModel;
use App\Shared\System\Command\CommandInterface;

class CreateProductCommand implements CommandInterface
{
    private ProductModel $productModel;

    public function __construct(ProductModel $productModel)
    {
        $this->productModel = $productModel;
    }

    public function getProductModel(): ProductModel
    {
        return $this->productModel;
    }
}