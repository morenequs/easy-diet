<?php

declare(strict_types=1);

namespace App\Infrastructure\Command\Meal;

use App\Form\Model\MealModel;
use App\Shared\System\Command\CommandInterface;

class EditMealCommand implements CommandInterface
{
    private $mealId;
    private MealModel $mealModel;

    public function __construct($mealId, MealModel $mealModel)
    {
        $this->mealId = $mealId;
        $this->mealModel = $mealModel;
    }

    public function getMealId()
    {
        return $this->mealId;
    }

    public function getMealModel(): MealModel
    {
        return $this->mealModel;
    }
}