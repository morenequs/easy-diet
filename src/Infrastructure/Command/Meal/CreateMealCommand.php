<?php

declare(strict_types=1);

namespace App\Infrastructure\Command\Meal;

use App\Form\Model\MealModel;
use App\Shared\System\Command\CommandInterface;

class CreateMealCommand implements CommandInterface
{
    private MealModel $mealModel;

    public function __construct(MealModel $mealModel)
    {
        $this->mealModel = $mealModel;
    }

    public function getMealModel(): MealModel
    {
        return $this->mealModel;
    }
}