<?php

declare(strict_types=1);

namespace App\Infrastructure\Command\Diet;

use App\Form\Model\DietModel;
use App\Shared\System\Command\CommandInterface;

class CreateDietCommand implements CommandInterface
{
    private DietModel $dietModel;

    public function __construct(DietModel $dietModel)
    {
        $this->dietModel = $dietModel;
    }

    public function getDietModel(): DietModel
    {
        return $this->dietModel;
    }
}