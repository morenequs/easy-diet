<?php

declare(strict_types=1);

namespace App\Infrastructure\Command\Diet;

use App\Form\Model\DietModel;
use App\Shared\System\Command\CommandInterface;

class EditDietCommand implements CommandInterface
{
    private $dietId;
    private DietModel $dietModel;

    public function __construct($dietId, DietModel $dietModel)
    {
        $this->dietId = $dietId;
        $this->dietModel = $dietModel;
    }

    public function getDietId()
    {
        return $this->dietId;
    }

    public function setDietId($dietId): void
    {
        $this->dietId = $dietId;
    }

    public function getDietModel(): DietModel
    {
        return $this->dietModel;
    }

    public function setDietModel(DietModel $dietModel): void
    {
        $this->dietModel = $dietModel;
    }
}