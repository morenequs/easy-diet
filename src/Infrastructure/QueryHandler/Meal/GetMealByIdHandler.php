<?php

declare(strict_types=1);

namespace App\Infrastructure\QueryHandler\Meal;

use App\Entity\Meal;
use App\Infrastructure\Query\Meal\GetMealByIdQuery;
use App\Repository\MealRepository;
use App\Shared\System\Query\QueryHandlerInterface;

class GetMealByIdHandler implements QueryHandlerInterface
{
    private MealRepository $mealRepository;

    public function __construct(MealRepository $mealRepository)
    {
        $this->mealRepository = $mealRepository;
    }

    public function __invoke(GetMealByIdQuery $getMealByIdQuery): ?Meal
    {
        return $this->mealRepository->findOneBy(['id' => $getMealByIdQuery->getMealId()]);
    }
}