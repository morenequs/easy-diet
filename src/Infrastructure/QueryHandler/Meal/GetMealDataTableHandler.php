<?php

declare(strict_types=1);

namespace App\Infrastructure\QueryHandler\Meal;

use App\Entity\Meal;
use App\Infrastructure\DataTable\DataSourceInterface;
use App\Infrastructure\Query\Meal\GetMealDataTableQuery;
use App\Shared\System\Query\QueryHandlerInterface;

class GetMealDataTableHandler implements QueryHandlerInterface
{
    private DataSourceInterface $dataSource;

    public function __construct(DataSourceInterface $dataSource)
    {
        $this->dataSource = $dataSource;
    }

    public function __invoke(GetMealDataTableQuery $getMealDataTableQuery): array
    {
        return $this->dataSource->getDataSource($getMealDataTableQuery->getParams(), Meal::class);
    }
}