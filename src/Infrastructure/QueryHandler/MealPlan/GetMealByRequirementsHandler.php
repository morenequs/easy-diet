<?php

declare(strict_types=1);

namespace App\Infrastructure\QueryHandler\MealPlan;

use App\Infrastructure\Finder\MealFinderInterface;
use App\Infrastructure\Query\MealPlan\GetMealByRequirementsQuery;
use App\Infrastructure\ViewModel\Meal\MealViewModel;
use App\Infrastructure\ViewModel\Meal\Transformer\MealTransformer;
use App\Repository\MealRepository;
use App\Shared\System\Query\QueryHandlerInterface;

class GetMealByRequirementsHandler implements QueryHandlerInterface
{
    private MealFinderInterface $mealFinder;
    private MealRepository $mealRepository;
    private MealTransformer $mealTransformer;

    public function __construct(
        MealFinderInterface $mealFinder,
        MealRepository $mealRepository,
        MealTransformer $mealTransformer
    )
    {
        $this->mealFinder = $mealFinder;
        $this->mealRepository = $mealRepository;
        $this->mealTransformer = $mealTransformer;
    }

    public function __invoke(GetMealByRequirementsQuery $getMealByRequirementsQuery): ?MealViewModel
    {
        if(!$mealId = $this->mealFinder->findMealByRequirements($getMealByRequirementsQuery->getRequirements())) {
            return null;
        }
        $meal = $this->mealRepository->findOneBy(['id' => $mealId]);
        return $this->mealTransformer->transform($meal);
    }
}