<?php

declare(strict_types=1);

namespace App\Infrastructure\QueryHandler\MealPlan;

use App\Infrastructure\Query\MealPlan\GetInitDataQuery;
use App\Infrastructure\ViewModel\MealPlan\InitDataViewModel;
use App\Infrastructure\ViewModel\MealPlan\Transformer\InitDataTransformer;
use App\Repository\DietRepository;
use App\Shared\System\Query\QueryHandlerInterface;

class GetInitDataHandler implements QueryHandlerInterface
{
    private DietRepository $dietRepository;
    private InitDataTransformer $initDataTransformer;

    public function __construct(DietRepository $dietRepository, InitDataTransformer $initDataTransformer)
    {
        $this->dietRepository = $dietRepository;
        $this->initDataTransformer = $initDataTransformer;
    }

    public function __invoke(GetInitDataQuery $getInitDataQuery): InitDataViewModel
    {
        $diets = $this->dietRepository->findAll();

        return $this->initDataTransformer->transform($diets);
    }
}