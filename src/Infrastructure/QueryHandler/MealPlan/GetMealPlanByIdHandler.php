<?php

declare(strict_types=1);

namespace App\Infrastructure\QueryHandler\MealPlan;

use App\Infrastructure\Query\MealPlan\GetMealPlanByIdQuery;
use App\Infrastructure\ViewModel\MealPlan\MealPlanResponseViewModel;
use App\Infrastructure\ViewModel\MealPlan\Transformer\MealPlanResponseTransformer;
use App\Repository\MealPlanRepository;
use App\Shared\System\Query\QueryHandlerInterface;

class GetMealPlanByIdHandler implements QueryHandlerInterface
{
    private MealPlanRepository $mealPlanRepository;

    private MealPlanResponseTransformer $mealPlanResponseTransformer;

    public function __construct(
        MealPlanRepository $mealPlanRepository,
        MealPlanResponseTransformer $mealPlanResponseTransformer
    )
    {
        $this->mealPlanRepository = $mealPlanRepository;
        $this->mealPlanResponseTransformer = $mealPlanResponseTransformer;
    }

    public function __invoke(GetMealPlanByIdQuery $getMealPlanByIdQuery): ?MealPlanResponseViewModel
    {
        if (!$mealPlan = $this->mealPlanRepository->findOneBy(['id' => $getMealPlanByIdQuery->getId()])) {
            return null;
        }

        return $this->mealPlanResponseTransformer->transform($mealPlan);
    }
}