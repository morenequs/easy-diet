<?php

declare(strict_types=1);

namespace App\Infrastructure\QueryHandler\Product;

use App\Entity\Product;
use App\Infrastructure\Query\Product\GetProductByIdQuery;
use App\Repository\ProductRepository;
use App\Shared\System\Query\QueryHandlerInterface;

class GetProductByIdHandler implements QueryHandlerInterface
{
    private ProductRepository $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function __invoke(GetProductByIdQuery $getProductByIdQuery): ?Product
    {
        return $this->productRepository->findOneBy(['id' => $getProductByIdQuery->getProductId()]);
    }
}