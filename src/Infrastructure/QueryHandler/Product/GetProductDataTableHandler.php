<?php

declare(strict_types=1);

namespace App\Infrastructure\QueryHandler\Product;

use App\Entity\Product;
use App\Infrastructure\DataTable\DataSourceInterface;
use App\Infrastructure\Query\Product\GetProductDataTableQuery;
use App\Shared\System\Query\QueryHandlerInterface;

class GetProductDataTableHandler implements QueryHandlerInterface
{
    private DataSourceInterface $dataSource;

    public function __construct(DataSourceInterface $dataSource)
    {
        $this->dataSource = $dataSource;
    }

    public function __invoke(GetProductDataTableQuery $getProductDataTableQuery)
    {
        return $this->dataSource->getDataSource($getProductDataTableQuery->getParams(), Product::class);
    }
}