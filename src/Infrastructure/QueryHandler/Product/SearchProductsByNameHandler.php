<?php

declare(strict_types=1);

namespace App\Infrastructure\QueryHandler\Product;

use App\Infrastructure\Query\Product\SearchProductsByNameQuery;
use App\Repository\ProductRepository;
use App\Shared\System\Query\QueryHandlerInterface;

class SearchProductsByNameHandler implements QueryHandlerInterface
{
    private ProductRepository $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function __invoke(SearchProductsByNameQuery $searchProductsByNameQuery): array
    {
        return $this->productRepository->searchByName(
            $searchProductsByNameQuery->getQuery(),
            $searchProductsByNameQuery->getLimit()
        );
    }
}