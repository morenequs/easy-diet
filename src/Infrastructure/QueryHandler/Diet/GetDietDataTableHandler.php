<?php

declare(strict_types=1);

namespace App\Infrastructure\QueryHandler\Diet;

use App\Entity\Diet;
use App\Infrastructure\DataTable\DataSourceInterface;
use App\Infrastructure\Query\Diet\GetDietDataTableQuery;
use App\Shared\System\Query\QueryHandlerInterface;

class GetDietDataTableHandler implements QueryHandlerInterface
{
    private DataSourceInterface $dataSource;

    public function __construct(DataSourceInterface $dataSource)
    {
        $this->dataSource = $dataSource;
    }

    public function __invoke(GetDietDataTableQuery $getDietDataTableQuery): array
    {
        return $this->dataSource->getDataSource($getDietDataTableQuery->getParams(), Diet::class);
    }
}