<?php

declare(strict_types=1);

namespace App\Infrastructure\QueryHandler\Diet;

use App\Entity\Diet;
use App\Infrastructure\Query\Diet\GetDietByIdQuery;
use App\Repository\DietRepository;
use App\Shared\System\Query\QueryHandlerInterface;

class GetDietByIdHandler implements QueryHandlerInterface
{
    private DietRepository $dietRepository;

    public function __construct(DietRepository $dietRepository)
    {
        $this->dietRepository = $dietRepository;
    }

    public function __invoke(GetDietByIdQuery $getDietByIdQuery): ?Diet
    {
        return $this->dietRepository->findOneBy(['id' => $getDietByIdQuery->getDietId()]);
    }
}