<?php

namespace App\Repository\FoodDataCentral;

use App\Entity\FoodDataCentral\FoodNutrient;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FoodNutrient|null find($id, $lockMode = null, $lockVersion = null)
 * @method FoodNutrient|null findOneBy(array $criteria, array $orderBy = null)
 * @method FoodNutrient[]    findAll()
 * @method FoodNutrient[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @extends ServiceEntityRepository<FoodNutrient>
 */
class FoodNutrientRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FoodNutrient::class);
    }
}
