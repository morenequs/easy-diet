<?php

namespace App\Repository\FoodDataCentral;

use App\Entity\FoodDataCentral\Food;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Contracts\Cache\CacheInterface;

/**
 * @method Food|null find($id, $lockMode = null, $lockVersion = null)
 * @method Food|null findOneBy(array $criteria, array $orderBy = null)
 * @method Food[]    findAll()
 * @method Food[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @extends ServiceEntityRepository<Food>
 */
class FoodRepository extends ServiceEntityRepository
{
    private CacheInterface $cache;

    public function __construct(ManagerRegistry $registry, CacheInterface $cache)
    {
        parent::__construct($registry, Food::class);
        $this->cache = $cache;
    }

    /**
     * @return array[]
     */
    public function getAllFoodIds(): array
    {
        $ids = $this->cache->get('food_with_all_nutrients_info', function () {
            return $this->createQueryBuilder('food')
                ->select('partial food.{id}')
                ->andWhere('food.dataType IN (:allowedTypes)')
                ->setParameter('allowedTypes', Food::MAIN_FOOD_TYPES)
                ->leftJoin('food.foodNutrients', 'foodNutrients')
                ->leftJoin('foodNutrients.nutrient', 'nutrient', 'WITH', 'nutrient.id IN (:nutrients)')
                ->setParameter('nutrients', [1003, 1004, 1005, 1008])
                ->andHaving('count(nutrient) = 4')
                ->groupBy('food.id')
                ->getQuery()
                ->getArrayResult();
        });

        return $ids;
    }

    /**
     * @param int[] $ids
     *
     * @return Food[]
     */
    public function getFoodByIds(array $ids): array
    {
        return $this->createQueryBuilder('food')
            ->addSelect('foodNutrients')
            ->addSelect('nutrient')
            ->leftJoin('food.foodNutrients', 'foodNutrients')
            ->leftJoin('foodNutrients.nutrient', 'nutrient')
            ->andWhere('food.id IN (:ids)')
            ->setParameter('ids', $ids)
            ->getQuery()
            ->getResult();
    }
}
