<?php

declare(strict_types=1);

namespace App\Repository;

interface DataTableRepositoryInterface
{
    public function count(array $params);
    public function findByParams(string $search, int $offset, int $limit): array;
    public function countFiltered(string $search): int;
}