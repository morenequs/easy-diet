<?php

namespace App\Repository;

use App\Entity\MealProduct;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MealProduct|null find($id, $lockMode = null, $lockVersion = null)
 * @method MealProduct|null findOneBy(array $criteria, array $orderBy = null)
 * @method MealProduct[]    findAll()
 * @method MealProduct[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @extends ServiceEntityRepository<MealProduct>
 */
class MealProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MealProduct::class);
    }
}
