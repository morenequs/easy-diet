<?php

namespace App\Repository;

use App\Entity\Diet;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Diet|null find($id, $lockMode = null, $lockVersion = null)
 * @method Diet|null findOneBy(array $criteria, array $orderBy = null)
 * @method Diet[]    findAll()
 * @method Diet[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DietRepository extends ServiceEntityRepository implements DataTableRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Diet::class);
    }

    public function findByParams(string $search, int $offset, int $limit): array
    {
        $qb = $this->createQueryBuilder('diet')
            ->select('partial diet.{id, name}')
            ->setFirstResult($offset)
            ->setMaxResults($limit)
        ;

        $this->filteredByName($qb, $search);

        return $qb->getQuery()
            ->getArrayResult();
    }

    public function countFiltered(string $search): int
    {
        $qb = $this->createQueryBuilder('diet')
            ->select('count(diet.id)');

        $this->filteredByName($qb, $search);

        return $qb->getQuery()
            ->getSingleScalarResult();
    }

    private function filteredByName(QueryBuilder $qb, string $search): void
    {
        if (strlen($search) > 0) {
            $qb->andWhere('diet.name LIKE :search')
                ->setParameter('search', '%' . $search . '%');
        }
    }
}
