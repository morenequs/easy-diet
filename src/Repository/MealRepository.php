<?php

namespace App\Repository;

use App\Entity\Meal;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Meal|null find($id, $lockMode = null, $lockVersion = null)
 * @method Meal|null findOneBy(array $criteria, array $orderBy = null)
 * @method Meal[]    findAll()
 * @method Meal[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @extends ServiceEntityRepository<Meal>
 */
class MealRepository extends ServiceEntityRepository implements DataTableRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Meal::class);
    }

    /** @return Meal[] */
    public function findMealsByIds(array $ids): array
    {
        return $this->createQueryBuilder('meal')
            ->andWhere('meal.id IN (:ids)')
            ->setParameter('ids', $ids)
            ->getQuery()
            ->getResult();
    }

    public function findByParams(string $search, int $offset, int $limit): array
    {
        $qb = $this->createQueryBuilder('meal')
            ->select('partial meal.{id, name}')
            ->setFirstResult($offset)
            ->setMaxResults($limit)
        ;

        $this->filteredByName($qb, $search);

        return $qb->getQuery()
            ->getArrayResult();
    }

    public function countFiltered(string $search): int
    {
        $qb = $this->createQueryBuilder('meal')
            ->select('count(meal.id)');

        $this->filteredByName($qb, $search);

        return $qb->getQuery()
            ->getSingleScalarResult();
    }

    private function filteredByName(QueryBuilder $qb, string $search): void
    {
        if (strlen($search) > 0) {
            $qb->andWhere('meal.name LIKE :search')
                ->setParameter('search', '%' . $search . '%');
        }
    }
}
