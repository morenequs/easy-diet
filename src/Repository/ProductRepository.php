<?php

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @extends ServiceEntityRepository<Product>
 */
class ProductRepository extends ServiceEntityRepository implements DataTableRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    public function searchByName(string $name, int $limit): array
    {
        $qb = $this->createQueryBuilder('product')
            ->setMaxResults($limit);

        $this->filteredByName($qb, $name);

        return $qb->getQuery()->getResult();
    }

    public function findByParams(string $search, int $offset, int $limit): array
    {
        $qb = $this->createQueryBuilder('product')
            ->select('partial product.{id, name}')
            ->setFirstResult($offset)
            ->setMaxResults($limit)
        ;

        $this->filteredByName($qb, $search);

        return $qb->getQuery()
            ->getArrayResult();
    }

    public function countFiltered(string $search): int
    {
        $qb = $this->createQueryBuilder('product')
            ->select('count(product.id)');

        $this->filteredByName($qb, $search);

        return $qb->getQuery()
            ->getSingleScalarResult();
    }

    private function filteredByName(QueryBuilder $qb, string $search): void
    {
        if (strlen($search) > 0) {
            $qb->andWhere('product.name LIKE :search')
                ->setParameter('search', '%' . $search . '%');
        }
    }
}
