<?php

declare(strict_types=1);

namespace App\Shared\System;

use App\Shared\System\Command\CommandBusInterface;
use App\Shared\System\Command\CommandInterface;
use App\Shared\System\Query\QueryBusInterface;
use App\Shared\System\Query\QueryInterface;

class System implements SystemInterface
{
    private CommandBusInterface $commandBus;
    private QueryBusInterface $queryBus;

    public function __construct(CommandBusInterface $commandBus, QueryBusInterface $queryBus)
    {
        $this->commandBus = $commandBus;
        $this->queryBus = $queryBus;
    }

    public function dispatch(CommandInterface $command): void
    {
        $this->commandBus->dispatch($command);
    }

    /**
     * @return mixed
     */
    public function handle(QueryInterface $query)
    {
        return $this->queryBus->handle($query);
    }
}