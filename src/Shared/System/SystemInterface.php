<?php

declare(strict_types=1);

namespace App\Shared\System;

use App\Shared\System\Command\CommandInterface;
use App\Shared\System\Query\QueryInterface;

interface SystemInterface
{
    public function dispatch(CommandInterface $command): void;

    /** @return mixed */
    public function handle(QueryInterface $query);
}