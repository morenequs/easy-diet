<?php

namespace App\Shared\System\Command;

interface CommandBusInterface
{
    public function dispatch(CommandInterface $command): void;
}