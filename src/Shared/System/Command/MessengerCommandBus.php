<?php

declare(strict_types=1);

namespace App\Shared\System\Command;

use Symfony\Component\Messenger\MessageBusInterface;

class MessengerCommandBus implements CommandBusInterface
{
    private MessageBusInterface $messengerBus;

    public function __construct(MessageBusInterface $messengerBus)
    {
        $this->messengerBus = $messengerBus;
    }

    public function dispatch(CommandInterface $command): void
    {
        $this->messengerBus->dispatch($command);
    }
}