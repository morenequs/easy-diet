<?php

namespace App\Shared\System\Query;

interface QueryBusInterface
{
    /** @return mixed */
    public function handle(QueryInterface $query);
}