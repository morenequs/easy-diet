<?php

declare(strict_types=1);

namespace App\Shared\System\Query;

use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\HandledStamp;

class MessengerQueryBus implements QueryBusInterface
{
    private MessageBusInterface $messengerBus;

    public function __construct(MessageBusInterface $messengerBus)
    {
        $this->messengerBus = $messengerBus;
    }

    public function handle(QueryInterface $query)
    {
        $envelope = $this->messengerBus->dispatch($query);
        /** @var HandledStamp $handled */
        $handled = $envelope->last(HandledStamp::class);
        return $handled->getResult();
    }
}