<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Diet;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\String\Slugger\SluggerInterface;

class DietFixture extends Fixture implements FixtureGroupInterface
{
    private SluggerInterface $sluggerInterface;

    public function __construct(SluggerInterface $sluggerInterface)
    {
        $this->sluggerInterface = $sluggerInterface;
    }

    public function load(ObjectManager $manager): void
    {
        foreach ($this->getDiets() as $dietName) {
            $diet = (new Diet())
                ->setName($dietName)
                ->setSlug($this->sluggerInterface->slug($dietName)->lower()->toString());
            $manager->persist($diet);
        }

        $manager->flush();
    }

    private function getDiets(): array
    {
        return ['Paleo', 'Wegetariańska', 'Wegańska', 'Ketogeniczna', 'Niskotłuszczowa'];
    }

    public static function getGroups(): array
    {
        return ['app', 'diet'];
    }
}