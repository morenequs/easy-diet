<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\FoodDataCentral\Food;
use App\Repository\FoodDataCentral\FoodRepository;
use App\Service\FoodDataCentral\Transformer\FoodToProductTransformer;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;

class ProductFixture extends Fixture implements FixtureGroupInterface
{
    const COUNT_PRODUCTS = 1000;

    private FoodRepository $foodRepository;
    private FoodToProductTransformer $foodToProductTransformer;

    public function __construct(FoodRepository $foodRepository, FoodToProductTransformer $foodToProductTransformer)
    {
        $this->foodRepository = $foodRepository;
        $this->foodToProductTransformer = $foodToProductTransformer;
    }

    public function load(ObjectManager $manager): void
    {
        $randomFoods = $this->getRandomFood(self::COUNT_PRODUCTS);

        foreach ($randomFoods as $food) {
            $product = $this->foodToProductTransformer->transform($food);
            $manager->persist($product);
        }
        $manager->flush();
    }

    /**
     * @return Food[]
     */
    private function getRandomFood(int $count): array
    {
        /** @var int[] $randomFoodIds */
        $randomFoodIds = array_rand(array_flip($this->getAllFoodIds()), $count);

        return $this->foodRepository->getFoodByIds($randomFoodIds);
    }

    /**
     * @return int[]
     */
    private function getAllFoodIds(): array
    {
        $result = array_map([$this, 'rowToId'], $this->foodRepository->getAllFoodIds());
        if (empty($result)) {
            throw new \RuntimeException('Food repository is empty.');
        }

        return $result;
    }

    /**
     * @param int[] $row
     */
    private function rowToId(array $row): int
    {
        if (!isset($row['id'])) {
            throw new \RuntimeException('Param $row must contain id key.');
        }

        return $row['id'];
    }

    public static function getGroups(): array
    {
        return ['app'];
    }
}
