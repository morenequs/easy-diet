<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Meal;
use App\Entity\MealProduct;
use App\Entity\Product;
use App\Infrastructure\Projector\MealPlan\MealProjectorInterface;
use App\Repository\DietRepository;
use App\Repository\ProductRepository;
use App\Util\Type\Decimal;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;
use Symfony\Component\String\Slugger\SluggerInterface;

class MealFixture extends Fixture implements FixtureGroupInterface, DependentFixtureInterface
{
    const COUNT_MEALS = 10000;

    private Generator $faker;
    private ProductRepository $productRepository;
    private SluggerInterface $slugger;
    private MealProjectorInterface $mealProjector;
    private DietRepository $dietRepository;

    /** @var Product[] */
    private array $products = [];

    private array $diets = [];

    public function __construct(
        ProductRepository $productRepository,
        SluggerInterface $slugger,
        MealProjectorInterface $mealProjector,
        DietRepository $dietRepository
    )
    {
        $this->faker = Factory::create();
        $this->productRepository = $productRepository;
        $this->slugger = $slugger;
        $this->mealProjector = $mealProjector;
        $this->dietRepository = $dietRepository;
    }

    public function load(ObjectManager $manager): void
    {
        $this->products = $this->productRepository->findAll();
        $this->diets = $this->dietRepository->findAll();

        for ($i = 0; $i < self::COUNT_MEALS; ++$i) {
            $meal = $this->generateMeal();
            $manager->persist($meal);
        }

        $manager->flush();
        $this->mealProjector->refreshAllFromDatabase();
    }

    public function getDependencies(): array
    {
        return [
            ProductFixture::class,
            DietFixture::class
        ];
    }

    public static function getGroups(): array
    {
        return ['app', 'meal'];
    }

    private function generateMeal(): Meal
    {
        $mealProducts = $this->getRandomMealProduct();
        $meal = new Meal();

        $mealEnergy = 0;
        foreach ($mealProducts as $mealProduct) {
            if (!$product = $mealProduct->getProduct()) {
                throw new \RuntimeException('MealProduct must have a Product.');
            }

            $productEnergy = Decimal::fromString($product->getEnergyAmount());
            $mealItemGram = Decimal::fromString($mealProduct->getGram());
            $mealItemEnergy = Decimal::div(Decimal::mul($productEnergy, $mealItemGram), Decimal::fromString('100'));

            $mealEnergy += $mealItemEnergy->toInt();
            if ($mealEnergy > 1200 && $mealEnergy - $mealItemEnergy->toInt() !== 0) {
                break;
            }
            $meal->addMealProduct($mealProduct);
        }
        $this->generateMealNameAndSlug($meal);
        $this->assignDiet($meal);
        $this->generateSteps($meal);
        $this->assignImage($meal);
        $meal->setPrepTime($this->faker->numberBetween(5, 60));

        if (count($meal->getMealProducts()) == 0) {
            throw new \RuntimeException('No meal products in meal');
        }

        return $meal;
    }

    /**
     * @return MealProduct[]
     */
    private function getRandomMealProduct(): array
    {
        if (count($this->products) < 8) {
            throw new \RuntimeException('Product repository must contain at least 8 products.');
        }

        $randomKeysOfProducts = (array) array_rand($this->products, random_int(2, 8));

        return array_map(function (int $index) {
            return $this->createMealProduct($index);
        }, $randomKeysOfProducts);
    }

    private function createMealProduct(int $productIndex): MealProduct
    {
        /** @var Product $product */
        $product = $this->products[$productIndex];

        return (new MealProduct())
            ->setProduct($product)
            ->setGram((string) $this->generateBasicWeight($product));
    }

    private function generateBasicWeight(Product $product): int
    {
        $kcal = intval($product->getEnergyAmount());
        $max = 300;
        if ($kcal > 3000) {
            $max = 10;
        } elseif ($kcal > 1000) {
            $max = 40;
        } elseif ($kcal > 500) {
            $max = 100;
        } elseif ($kcal > 300) {
            $max = 200;
        }

        return random_int(10, $max);
    }

    private function generateMealNameAndSlug(Meal $meal): void
    {
        $name = $this->faker->words(random_int(2, 8), true);
        if (!is_string($name)) {
            throw new \RuntimeException('Name of meal must be a string');
        }
        $name = ucfirst($name);
        $meal->setName($name);
        $meal->setSlug($this->slugger->slug($name)->lower()->toString());
    }

    private function generateSteps(Meal $meal): void
    {
        $steps = [];
        $countSteps = $this->faker->numberBetween(3, 10);
        for ($i = 0; $i < $countSteps; $i++) {
            $steps[] = $this->faker->sentences($this->faker->numberBetween(1, 5), true);
        }

        $meal->setSteps($steps);
    }

    private function assignImage(Meal $meal): void
    {
        $meal->setImage('build/foods/food' . $this->faker->numberBetween(1, 20) . '.jpg');
    }

    private function assignDiet(Meal $meal): void
    {
        $num = random_int(0, 2);
        if ($num) {
            $dietKeys = array_rand($this->diets, $num);
            if (1 === $num) {
                $dietKeys = [$dietKeys];
            }

            foreach ($dietKeys as $key) {
                $meal->addFitForDiet($this->diets[$key]);
            }
        }
    }
}
