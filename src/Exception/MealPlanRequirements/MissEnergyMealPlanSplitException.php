<?php

declare(strict_types=1);

namespace App\Exception\MealPlanRequirements;

class MissEnergyMealPlanSplitException extends \InvalidArgumentException
{
    /** @var string */
    protected $message = 'MealPlan must have energy splits in nutrientsMealPlanSplit';
}
