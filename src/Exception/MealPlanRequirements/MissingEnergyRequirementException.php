<?php

declare(strict_types=1);

namespace App\Exception\MealPlanRequirements;

class MissingEnergyRequirementException extends \InvalidArgumentException
{
    /** @var string */
    protected $message = 'MealPlan must have energy requirement in nutrientRequirements';
}
