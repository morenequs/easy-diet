<?php

declare(strict_types=1);

namespace App\Exception\Sovler;

class SolverInvalidResponseException extends \Exception
{
    protected $message = 'Solver response is invalid';
}