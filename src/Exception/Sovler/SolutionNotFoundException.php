<?php

declare(strict_types=1);

namespace App\Exception\Sovler;

class SolutionNotFoundException extends \Exception
{
    protected $message = 'Solution for this request was not found.';
}