<?php

declare(strict_types=1);

namespace App\Exception\Sovler;

class SolverCommunicationException extends \RuntimeException
{
}