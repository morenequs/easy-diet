<?php

declare(strict_types=1);

namespace App\Form\Model;

use App\Entity\Diet;

class DietModel
{
    private $id;

    private ?string $name = null;

    private ?string $slug = null;

    public static function createFromDiet(Diet $diet): self
    {
        $self = new self();
        $self->setId($diet->getId());
        $self->setName($diet->getName());
        $self->setSlug($diet->getSlug());

        return $self;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): void
    {
        $this->slug = $slug;
    }
}