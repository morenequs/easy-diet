<?php

declare(strict_types=1);

namespace App\Form\Model;

use App\Entity\Meal;
use App\Entity\MealProduct;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class MealModel
{
    private $id;

    private ?string $name;

    /** @var MealProductModel[] */
    private array $mealProducts;

    private int $prepTime;

    private ?UploadedFile $image = null;

    private ?string $imagePath = null;

    /** @var string[] */
    private array $steps;

    private Collection $fitForDiets;

    public function __construct()
    {
        $this->fitForDiets = new ArrayCollection();
    }

    public static function createFromMeal(Meal $meal): self
    {
        $self = new self();
        $self->setId($meal->getId());
        $self->setName($meal->getName());
        $self->setPrepTime($meal->getPrepTime());
        $self->setImagePath($meal->getImage());
        $self->setSteps($meal->getSteps());
        $self->setFitForDiets(clone $meal->getFitForDiets());

        $mealProducts = array_map(
            fn (MealProduct $mealProduct) => MealProductModel::createFromMealProduct($mealProduct),
            $meal->getMealProducts()->toArray()
        );

        $self->setMealProducts($mealProducts);
        return $self;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    public function getMealProducts(): array
    {
        return $this->mealProducts;
    }

    public function setMealProducts(array $mealProducts): void
    {
        $this->mealProducts = $mealProducts;
    }

    public function getPrepTime(): int
    {
        return $this->prepTime;
    }

    public function setPrepTime(int $prepTime): void
    {
        $this->prepTime = $prepTime;
    }

    public function getImage(): ?UploadedFile
    {
        return $this->image;
    }

    public function setImage(?UploadedFile $image): void
    {
        $this->image = $image;
    }

    public function getImagePath(): ?string
    {
        return $this->imagePath;
    }

    public function setImagePath(?string $imagePath): void
    {
        $this->imagePath = $imagePath;
    }

    public function getSteps(): array
    {
        return $this->steps;
    }

    public function setSteps(array $steps): void
    {
        $this->steps = $steps;
    }

    public function getFitForDiets(): Collection
    {
        return $this->fitForDiets;
    }

    public function setFitForDiets(Collection $fitForDiets): void
    {
        $this->fitForDiets = $fitForDiets;
    }
}