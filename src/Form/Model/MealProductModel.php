<?php

declare(strict_types=1);

namespace App\Form\Model;

use App\Entity\MealProduct;
use App\Entity\Product;

class MealProductModel
{
    private $id;

    private ?Product $product = null;

    private ?string $gram = null;

    public static function createFromMealProduct(MealProduct $mealProducts): self
    {
        $self = new self();

        $self->setId($mealProducts->getId());
        $self->setProduct($mealProducts->getProduct());
        $self->setGram($mealProducts->getGram());

        return $self;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): void
    {
        $this->product = $product;
    }

    public function getGram(): ?string
    {
        return $this->gram;
    }

    public function setGram(?string $gram): void
    {
        $this->gram = $gram;
    }
}