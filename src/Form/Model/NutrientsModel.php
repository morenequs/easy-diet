<?php

declare(strict_types=1);

namespace App\Form\Model;

use App\Entity\Nutrient;
use App\Entity\NutrientType;

class NutrientsModel
{
    private ?string $energy = null;

    private ?string $proteins = null;

    private ?string $fats = null;

    private ?string $carbs = null;

    public static function createFromNutrientsArray(array $nutrients): self
    {
        $self = new self();

        /** @var Nutrient $nutrient */
        foreach ($nutrients as $nutrient) {
            $type = (string) $nutrient->getType();

            $setter = 'set' .  ucfirst ($type);
            $self->{$setter}($nutrient->getAmount());
        }

        return $self;
    }

    public function getEnergy(): ?string
    {
        return $this->energy;
    }

    public function setEnergy(?string $energy): void
    {
        $this->energy = $energy;
    }

    public function getProteins(): ?string
    {
        return $this->proteins;
    }

    public function setProteins(?string $proteins): void
    {
        $this->proteins = $proteins;
    }

    public function getFats(): ?string
    {
        return $this->fats;
    }

    public function setFats(?string $fats): void
    {
        $this->fats = $fats;
    }

    public function getCarbs(): ?string
    {
        return $this->carbs;
    }

    public function setCarbs(?string $carbs): void
    {
        $this->carbs = $carbs;
    }

    public function toNutrientTypeAmountMap(): array
    {
        foreach (get_object_vars($this) as $property => $value) {
            if (!in_array($property, NutrientType::AVAILABLE_NUTRIENT_TYPES)) {
                throw new \LogicException(
                    sprintf('Not recognised property as nutrient type in NutrientsModel: %s', $property)
                );
            }
        }
        return get_object_vars($this);
    }
}