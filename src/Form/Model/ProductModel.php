<?php

declare(strict_types=1);

namespace App\Form\Model;

use App\Entity\Product;

class ProductModel
{
    private ?int $id = null;

    private ?string $name = null;

    private ?NutrientsModel $nutrients = null;

    public static function createFromProduct(Product $product): ProductModel
    {
        $self = new self();
        $self->setId($product->getId());
        $self->setName($product->getName());

        $nutrientsModel = NutrientsModel::createFromNutrientsArray($product->getNutrients()->toArray());
        $self->setNutrients($nutrientsModel);

        return $self;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    public function getNutrients(): ?NutrientsModel
    {
        return $this->nutrients;
    }

    public function setNutrients(?NutrientsModel $nutrients): void
    {
        $this->nutrients = $nutrients;
    }
}