<?php

declare(strict_types=1);

namespace App\Form\Model\Transformer;

use App\Entity\Nutrient;
use App\Entity\NutrientType;
use App\Entity\Product;
use App\Form\Model\ProductModel;
use Symfony\Component\String\Slugger\SluggerInterface;

class ProductModelTransformer
{
    private SluggerInterface $slugger;

    public function __construct(SluggerInterface $slugger)
    {
        $this->slugger = $slugger;
    }

    public function transformModelToProduct(ProductModel $productModel, Product $product): void
    {
        $product->setName($productModel->getName());
        $product->setSlug($this->slugger->slug($productModel->getName())->lower()->toString());

        $nutrientsFromModel = $productModel->getNutrients()->toNutrientTypeAmountMap();
        /** @var Nutrient $nutrient */
        foreach ($product->getNutrients() as $nutrient) {
            $type = (string) $nutrient->getType();
            if (array_key_exists($type, $nutrientsFromModel)) {
                $nutrient->setAmount($nutrientsFromModel[$type]);
                unset($nutrientsFromModel[$type]);
            }
        }

        foreach ($nutrientsFromModel as $nutrientType => $amount) {
            $nutrient = (new Nutrient())
                ->setType(new NutrientType($nutrientType))
                ->setAmount($amount)
                ->setProduct($product);
            $product->addNutrient($nutrient);
        }
    }
}