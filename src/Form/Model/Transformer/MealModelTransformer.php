<?php

declare(strict_types=1);

namespace App\Form\Model\Transformer;

use App\Entity\Meal;
use App\Entity\MealProduct;
use App\Form\Model\MealModel;
use App\Form\Model\MealProductModel;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\String\Slugger\SluggerInterface;

class MealModelTransformer
{
    private SluggerInterface $slugger;
    private string $mediaPath;

    public function __construct(SluggerInterface $slugger, string $mediaPath)
    {
        $this->slugger = $slugger;
        $this->mediaPath = $mediaPath;
    }

    public function transformModelToMeal(MealModel $mealModel, Meal $meal): void
    {
        $meal->setName($mealModel->getName());
        $meal->setSlug($this->slugger->slug($mealModel->getName())->lower()->toString());
        $meal->setPrepTime($mealModel->getPrepTime());
        $meal->setSteps($mealModel->getSteps());

        $this->updateFitForDiets($mealModel, $meal);

        $this->updateOrRemoveCurrentMealProducts($mealModel->getMealProducts(), $meal);
        $this->addNewMealProducts($mealModel->getMealProducts(), $meal);

        $this->saveImage($mealModel->getImage(), $meal);
    }

    private function updateOrRemoveCurrentMealProducts(array $mealProductModels, Meal $meal): void
    {
        $mealProducts = $meal->getMealProducts();
        $idToMealProductModelMap = $this->createIdToMealProductModelMap($mealProductModels);

        /** @var MealProduct $mealProduct */
        foreach ($mealProducts as $mealProduct) {
            if (!isset($idToMealProductModelMap[$mealProduct->getId()])) {
                $mealProducts->removeElement($mealProduct);
                continue;
            }

            /** @var MealProductModel $mealProductModel */
            $mealProductModel = $idToMealProductModelMap[$mealProduct->getId()];
            $mealProduct->setGram($mealProductModel->getGram());
            $mealProduct->setProduct($mealProductModel->getProduct());
        }
    }

    private function createIdToMealProductModelMap(array $mealProductModels): array
    {
        $map = [];

        foreach ($mealProductModels as $mealProduct) {
            if (!$id = $mealProduct->getId()) {
                continue;
            }

            $map[$id] = $mealProduct;
        }
        return $map;
    }

    private function addNewMealProducts(array $mealProductModels, Meal $meal): void
    {
        $mealProducts = $meal->getMealProducts();
        $newMealProductModel = $this->getNewMealProductModels($mealProductModels);

        /** @var MealProductModel $mealProductModel */
        foreach ($newMealProductModel as $mealProductModel) {
            $mealProduct = (new MealProduct())
                ->setProduct($mealProductModel->getProduct())
                ->setGram($mealProductModel->getGram())
                ->setMeal($meal);

            $mealProducts->add($mealProduct);
        }
    }

    private function getNewMealProductModels(array $mealProductModels): array
    {
        return array_filter(
            $mealProductModels,
            fn(MealProductModel $mealProductModel) => is_null($mealProductModel->getId())
        );
    }

    private function saveImage(?UploadedFile $uploadedFile, Meal $meal): void
    {
        if ($uploadedFile) {
            $originalFilename = pathinfo($uploadedFile->getClientOriginalName(), PATHINFO_FILENAME);

            $safeFilename = $this->slugger->slug($originalFilename);
            $newFilename = $safeFilename . '-' . uniqid() . '.' . $uploadedFile->guessExtension();

            try {
                $uploadedFile->move(
                    $this->mediaPath,
                    $newFilename
                );
            } catch (FileException $e) {
                throw new \RuntimeException('Fail during save image');
            }


            $meal->setImage($this->mediaPath . $newFilename);
        }
    }

    private function updateFitForDiets(MealModel $mealModel, Meal $meal): void
    {
        $fitForDietsCollection = $meal->getFitForDiets();
        $fitForDietsCollection->clear();

        foreach ($mealModel->getFitForDiets() as $diet) {
            $fitForDietsCollection->add($diet);
        }
    }
}