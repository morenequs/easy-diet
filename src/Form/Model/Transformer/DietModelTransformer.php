<?php

declare(strict_types=1);

namespace App\Form\Model\Transformer;

use App\Entity\Diet;
use App\Form\Model\DietModel;
use Symfony\Component\String\Slugger\SluggerInterface;

class DietModelTransformer
{
    private SluggerInterface $slugger;

    public function __construct(SluggerInterface $slugger)
    {
        $this->slugger = $slugger;
    }

    public function transformModelToDiet(DietModel $dietModel, Diet $diet): void
    {
        $diet->setName($dietModel->getName());
        $diet->setSlug($this->slugger->slug($dietModel->getName())->lower()->toString());
    }
}