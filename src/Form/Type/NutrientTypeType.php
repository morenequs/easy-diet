<?php

declare(strict_types=1);

namespace App\Form\Type;

use App\Entity\NutrientType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NutrientTypeType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver): void
    {
        $choices = [];

        foreach (NutrientType::AVAILABLE_NUTRIENT_TYPES as $type) {
            $choices[] = new NutrientType($type);
        }

        $resolver->setDefaults([
            'choices' => $choices,
            'choice_value' => 'type',
            'choice_label' => function (?NutrientType $nutrientType) {
                return $nutrientType ? strtoupper($nutrientType->getType()) : '';
            },
        ]);
    }

    public function getParent(): string
    {
        return ChoiceType::class;
    }
}