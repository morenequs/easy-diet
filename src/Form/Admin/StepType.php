<?php

declare(strict_types=1);

namespace App\Form\Admin;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class StepType extends AbstractType
{
    public function getBlockPrefix(): string
    {
        return 'StepType';
    }

    public function getParent(): string
    {
        return TextareaType::class;
    }
}