<?php

declare(strict_types=1);

namespace App\Form\Admin;

use App\Form\Model\NutrientsModel;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Range;

class NutrientsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('energy', NumberType::class, [
                'label' => 'nutrients.energy',
                'html5' => true,
                'scale' => 3,
                'attr' => [
                    'step' => '0.001',
                ],
                'constraints' => [
                    new NotBlank(),
                    new Range(['min' => 0, 'max' => 10000]),
                ]
            ])
            ->add('proteins', NumberType::class, [
                'label' => 'nutrients.proteins',
                'html5' => true,
                'scale' => 3,
                'attr' => [
                    'step' => '0.001',
                ],
                'constraints' => [
                    new NotBlank(),
                    new Range(['min' => 0, 'max' => 10000]),
                ]
            ])
            ->add('fats', NumberType::class, [
                'label' => 'nutrients.fats',
                'html5' => true,
                'scale' => 3,
                'attr' => [
                    'step' => '0.001',
                ],
                'constraints' => [
                    new NotBlank(),
                    new Range(['min' => 0, 'max' => 10000]),
                ]
            ])
            ->add('carbs', NumberType::class, [
                'label' => 'nutrients.carbs',
                'html5' => true,
                'input' => 'string',
                'scale' => 3,
                'attr' => [
                    'step' => '0.001',
                ],
                'constraints' => [
                    new NotBlank(),
                    new Range(['min' => 0, 'max' => 10000]),
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => NutrientsModel::class
        ]);
    }
}