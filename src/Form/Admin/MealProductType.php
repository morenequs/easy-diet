<?php

declare(strict_types=1);

namespace App\Form\Admin;

use App\Entity\Product;
use App\Form\Model\MealProductModel;
use App\Form\Type\AutocompleteTextType;
use App\Repository\ProductRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Range;

class MealProductType extends AbstractType
{
    private ProductRepository $productRepository;
    private RouterInterface $router;

    public function __construct(ProductRepository $productRepository, RouterInterface $router)
    {
        $this->productRepository = $productRepository;
        $this->router = $router;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('gram', NumberType::class, [
                'label' => 'meals.gram',
                'html5' => true,
                'scale' => 3,
                'attr' => [
                    'step' => '0.001',
                ],
                'constraints' => [
                    new NotBlank(),
                    new Range(['min' => 0.001, 'max' => 1000])
                ]
            ])
            ->add('product', AutocompleteTextType::class, [
                'label' => 'product.name',
                'autocomplete_url' => $this->router->generate('admin_products_search'),
                'autocomplete_field' => 'name',
                'constraints' => [
                    new NotBlank(),
                ]
            ]);

        $builder->get('product')
            ->addModelTransformer(new CallbackTransformer(
                function (?Product $product) {
                    if (!$product) {
                        return '';
                    }

                    return $product->getName();
                },
                function (string $productName) {
                    if(!$product = $this->productRepository->findOneBy(['name' => $productName])) {
                        throw new TransformationFailedException(
                            sprintf('Product not found with name "%s"', $productName)
                        );
                    }
                    
                    return $product;
                }
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => MealProductModel::class,
        ]);
    }

    public function getBlockPrefix()
    {
        return 'MealProductType';
    }
}