<?php

declare(strict_types=1);

namespace App\Form\Admin;

use App\Entity\Company;
use App\Entity\Diet;
use App\Form\Model\MealModel;
use App\Infrastructure\Validator\Meal\UniqueMealName;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Range;

class MealType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'meals.name',
                'constraints' => [
                    new NotBlank(),
                ]
            ])
            ->add('prepTime', NumberType::class, [
                'label' => 'meals.prep_time',
                'html5' => true,
                'constraints' => [
                    new NotBlank(),
                    new Range(['min' => 0, 'max' => 360])
                ]
            ])
            ->add('fitForDiets', EntityType::class, [
                'label' => 'meals.fit_for_diets',
                'class' => Diet::class,
                'choice_label' => 'name',
                'multiple' => true,
                'required' => false,
            ])
            ->add('image', FileType::class, [
                'label' => 'meals.image',
                'required' => false,
                'constraints' => [
                    new NotBlank(['groups' => 'image_required']),
                    new File([
                        'maxSize' => '2M',
                        'mimeTypes' => [
                            'image/png',
                            'image/jpeg',
                        ],
                        'mimeTypesMessage' => 'images_only',
                    ])
                ],
            ])
            ->add('mealProducts', CollectionType::class, [
                'entry_type' => MealProductType::class,
                'entry_options' => ['label' => false],
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'prototype_name' => '__parent_name__',
                'attr' => [
                    'class' => 'parent-collection',
                ],
                'constraints' => [
                    new NotBlank(),
                ]
            ])
            ->add('steps', CollectionType::class, [
                'entry_type' => StepType::class,
                'entry_options' => ['label' => false],
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'prototype_name' => '__parent_name2__',
                'attr' => [
                    'class' => 'parent-collection2',
                ],
                'constraints' => [
                    new NotBlank(),
                ]
            ]);
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => MealModel::class,
            'validation_groups' => function (FormInterface $form) {
                $data = $form->getData();

                if (!$data instanceof MealModel) {
                    throw new \InvalidArgumentException('MealModel required');
                }

                $groups = ['Default'];

                if (!$data->getImagePath()) {
                    $groups[] = 'image_required';
                }

                return $groups;
            },
            'constraints' => [
                new UniqueMealName(),
            ]
        ]);
    }
}