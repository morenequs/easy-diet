<?php

namespace App\Command;

use App\Service\FoodDataCentral\Import\FoodDataCentralImporterInterface;
use App\Util\Progress\ProgressTerminal;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\HttpKernel\KernelInterface;

class ImportFoodDataFromCsvCommand extends Command
{
    const DEFAULT_FOOD_DIRECTORY = 'food_data';
    const FOOD = 'food.csv';

    protected static $defaultName = 'app:import-food-data-from-csv';

    private KernelInterface $kernel;
    private FoodDataCentralImporterInterface $foodDataCentralImporter;

    public function __construct(KernelInterface $kernel, FoodDataCentralImporterInterface $foodDataCentralImporter)
    {
        parent::__construct();

        $this->kernel = $kernel;
        $this->foodDataCentralImporter = $foodDataCentralImporter;
    }

    protected function configure(): void
    {
        $this
            ->setDescription('Add a short description for your command')
            ->addArgument('directory', InputArgument::OPTIONAL, 'Dir with food data (relative to root project directory)')
            ->addOption('force', 'f', InputOption::VALUE_OPTIONAL, 'Force reimport food data', false);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        /** @var string $directory */
        $directory = $input->getArgument('directory') ?: self::DEFAULT_FOOD_DIRECTORY;

        $fullPath = $this->kernel->getProjectDir() . '/' . $directory;
        $io->note(sprintf('Food directory: %s', $fullPath));

        $this->foodDataCentralImporter->importAll($fullPath, new ProgressTerminal($io));

        return Command::SUCCESS;
    }
}
