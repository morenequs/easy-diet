<?php

namespace App\Entity;

use App\Repository\ProductRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=ProductRepository::class)
 * @UniqueEntity(
 *     fields={"name", "slug"},
 *     errorPath="name",
 *     message="Name field must be unique."
 * )
 */
class Product
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $name = null;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $slug = null;

    /**
     * @ORM\OneToMany(targetEntity=MealProduct::class, mappedBy="product", orphanRemoval=true)
     *
     * @var Collection<int, MealProduct>
     */
    private Collection $mealProducts;

    /**
     * @ORM\OneToMany(targetEntity=Nutrient::class, mappedBy="product", orphanRemoval=true, cascade={"all"})
     *
     * @var Collection<int, Nutrient>
     */
    private Collection $nutrients;

    public function __construct()
    {
        $this->mealProducts = new ArrayCollection();
        $this->nutrients = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return Collection<int, MealProduct>
     */
    public function getMealProducts(): Collection
    {
        return $this->mealProducts;
    }

    public function addMealProduct(MealProduct $mealProduct): self
    {
        if (!$this->mealProducts->contains($mealProduct)) {
            $this->mealProducts[] = $mealProduct;
            $mealProduct->setProduct($this);
        }

        return $this;
    }

    public function removeMealProduct(MealProduct $mealProduct): self
    {
        if ($this->mealProducts->removeElement($mealProduct)) {
            // set the owning side to null (unless already changed)
            if ($mealProduct->getProduct() === $this) {
                $mealProduct->setProduct(null);
            }
        }

        return $this;
    }

    public function getEnergyAmount(): string
    {
        $energy = array_filter($this->nutrients->toArray(), function (Nutrient $nutrient) {
            return $nutrient->isType(NutrientType::ENERGY);
        });

        if (!count($energy)) {
            throw new \Exception('Product has not contain a energy nutrient');
        }

        return array_shift($energy)->getAmount();
    }

    /**
     * @return Collection|Nutrient[]
     */
    public function getNutrients(): Collection
    {
        return $this->nutrients;
    }

    public function addNutrient(Nutrient $nutrient): self
    {
        if (!$this->nutrients->contains($nutrient)) {
            $this->nutrients[] = $nutrient;
            $nutrient->setProduct($this);
        }

        return $this;
    }

    public function removeNutrient(Nutrient $nutrient): self
    {
        if ($this->nutrients->removeElement($nutrient)) {
            // set the owning side to null (unless already changed)
            if ($nutrient->getProduct() === $this) {
                $nutrient->setProduct(null);
            }
        }

        return $this;
    }
}
