<?php

namespace App\Entity;

use App\Repository\MealPlanRepository;
use App\Util\Type\Decimal;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Embedded;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity(repositoryClass=MealPlanRepository::class)
 */
class MealPlan
{
    /**
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     */
    private UuidInterface $id;

    /**
     * @ORM\ManyToMany(targetEntity=Meal::class)
     */
    private Collection $meals;

    /**
     * @Embedded(class="MealPlanType", columnPrefix=false)
     */
    private ?MealPlanType $type;

    /** @ORM\Column(type="datetime") */
    private ?\DateTime $createdAt;

    /** @ORM\Column(type="datetime", nullable=true) */
    private ?\DateTime $updatedAt = null;

    public function __construct(UuidInterface $id, MealPlanType $type)
    {
        $this->id = $id;
        $this->type = $type;
        $this->meals = new ArrayCollection();
        $this->createdAt = new \DateTime('now');
    }

    public function getId(): ?UuidInterface
    {
        return $this->id;
    }

    /**
     * @return Collection|Meal[]
     */
    public function getMeals(): Collection
    {
        return $this->meals;
    }

    public function addMeal(Meal $meal): self
    {
        if (!$this->meals->contains($meal)) {
            $this->meals[] = $meal;
        }

        return $this;
    }

    public function removeMeal(Meal $meal): self
    {
        $this->meals->removeElement($meal);

        return $this;
    }

    public function getType(): MealPlanType
    {
        return $this->type;
    }

    public function isType(string $type): bool
    {
        return $this->type->isType($type);
    }

    public function setType(MealPlanType $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    public function setUpdatedAt(?\DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    public function getTotalNutrientAmountData(): array
    {
        $mealNutrients = array_map(
            fn (Meal $meal) => $meal->getTotalNutrientAmountData(),
            $this->getMeals()->toArray()
        );

        $mealNutrientsTypes = array_map(
            fn (array $nutrientsData) => array_keys($nutrientsData),
            $mealNutrients
        );

        $intersectNutrientTypes = array_intersect(...$mealNutrientsTypes);

        return array_reduce($mealNutrients, function (array $nutrientsCarry, array $nutrients) {
            foreach ($nutrientsCarry as $nutrientType => $nutrientCarry) {
                $nutrientsCarry[$nutrientType] = Decimal::add($nutrientCarry, $nutrients[$nutrientType]);
            }
            return $nutrientsCarry;
        }, array_fill_keys($intersectNutrientTypes, Decimal::fromString('0')));
    }
}
