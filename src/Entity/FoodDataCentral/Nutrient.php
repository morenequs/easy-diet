<?php

namespace App\Entity\FoodDataCentral;

use App\Repository\FoodDataCentral\NutrientRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=NutrientRepository::class)
 * @ORM\Table(name="fdc_nutrient")
 */
class Nutrient
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $unitName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $nutrientNbr;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $nutrientRank;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getUnitName(): ?string
    {
        return $this->unitName;
    }

    public function setUnitName(string $unitName): self
    {
        $this->unitName = $unitName;

        return $this;
    }

    public function getNutrientNbr(): ?string
    {
        return $this->nutrientNbr;
    }

    public function setNutrientNbr(string $nutrientNbr): self
    {
        $this->nutrientNbr = $nutrientNbr;

        return $this;
    }

    public function getNutrientRank(): ?string
    {
        return $this->nutrientRank;
    }

    public function setNutrientRank(?string $nutrientRank): self
    {
        $this->nutrientRank = $nutrientRank;

        return $this;
    }
}
