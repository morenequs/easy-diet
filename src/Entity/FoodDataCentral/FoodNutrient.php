<?php

namespace App\Entity\FoodDataCentral;

use App\Repository\FoodDataCentral\FoodNutrientRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FoodNutrientRepository::class)
 * @ORM\Table(name="fdc_food_nutrient")
 */
class FoodNutrient
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\ManyToOne(targetEntity=Food::class, inversedBy="foodNutrients")
     */
    private ?Food $food;

    /**
     * @ORM\ManyToOne(targetEntity=Nutrient::class)
     */
    private ?Nutrient $nutrient;

    /**
     * @ORM\Column(type="decimal", precision=14, scale=3)
     */
    private ?string $amount;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFood(): ?Food
    {
        return $this->food;
    }

    public function setFood(?Food $food): self
    {
        $this->food = $food;

        return $this;
    }

    public function getNutrient(): ?Nutrient
    {
        return $this->nutrient;
    }

    public function setNutrient(?Nutrient $nutrient): self
    {
        $this->nutrient = $nutrient;

        return $this;
    }

    public function getAmount(): ?string
    {
        return $this->amount;
    }

    public function setAmount(string $amount): self
    {
        $this->amount = $amount;

        return $this;
    }
}
