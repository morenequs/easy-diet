<?php

namespace App\Entity\FoodDataCentral;

use App\Repository\FoodDataCentral\FoodRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FoodRepository::class)
 * @ORM\Table(name="fdc_food")
 */
class Food
{
    const MAIN_FOOD_TYPES = ['branded_food', 'survey_fndds_food', 'sr_legacy_food', 'foundation_food'];

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $dataType;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $description;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private ?\DateTimeInterface $publicationDate;

    /**
     * @ORM\OneToMany(targetEntity=FoodNutrient::class, mappedBy="food")
     *
     * @var Collection<int, FoodNutrient>
     */
    private Collection $foodNutrients;

    public function __construct()
    {
        $this->foodNutrients = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDataType(): ?string
    {
        return $this->dataType;
    }

    public function setDataType(string $dataType): self
    {
        $this->dataType = $dataType;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPublicationDate(): ?\DateTimeInterface
    {
        return $this->publicationDate;
    }

    public function setPublicationDate(?\DateTimeInterface $publicationDate): self
    {
        $this->publicationDate = $publicationDate;

        return $this;
    }

    /**
     * @return Collection<int, FoodNutrient>
     */
    public function getFoodNutrients(): Collection
    {
        return $this->foodNutrients;
    }

    public function addFoodNutrient(FoodNutrient $foodNutrient): self
    {
        if (!$this->foodNutrients->contains($foodNutrient)) {
            $this->foodNutrients[] = $foodNutrient;
            $foodNutrient->setFood($this);
        }

        return $this;
    }

    public function removeFoodNutrient(FoodNutrient $foodNutrient): self
    {
        if ($this->foodNutrients->removeElement($foodNutrient)) {
            // set the owning side to null (unless already changed)
            if ($foodNutrient->getFood() === $this) {
                $foodNutrient->setFood(null);
            }
        }

        return $this;
    }
}
