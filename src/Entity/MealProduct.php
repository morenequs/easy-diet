<?php

namespace App\Entity;

use App\Repository\MealProductRepository;
use App\Util\Type\Decimal;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MealProductRepository::class)
 */
class MealProduct
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\ManyToOne(targetEntity=Meal::class, inversedBy="mealProducts")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Meal $meal = null;

    /**
     * @ORM\ManyToOne(targetEntity=Product::class, inversedBy="mealProducts")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Product $product = null;

    /**
     * @ORM\Column(type="decimal", precision=5, scale=1)
     */
    private ?string $gram = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMeal(): ?Meal
    {
        return $this->meal;
    }

    public function setMeal(?Meal $meal): self
    {
        $this->meal = $meal;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getGram(): ?string
    {
        return $this->gram;
    }

    public function setGram(string $gram): self
    {
        $this->gram = $gram;

        return $this;
    }

    public function getNutrientAmountData(): array
    {
        if (!$this->product || !$this->gram) {
            throw new \LogicException('Invalid MealProduct entity (not contain product or gram info)');
        }

        $gram = Decimal::fromString($this->getGram());

        $nutrients = [];
        foreach ($this->product->getNutrients() as $nutrient) {
            $type = (string) $nutrient->getType();
            $nutrients[$type] = Decimal::fromString($nutrient->getAmount())->getPercentage($gram);
        }

        return $nutrients;
    }
}
