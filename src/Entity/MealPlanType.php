<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Embeddable;

/**
 * @Embeddable
 */
class MealPlanType
{
    const PENDING = 'pending';
    const SUCCESS = 'success';
    const TOO_LONG = 'too_long';
    const NO_PLAN = 'no_plan';

    const AVAILABLE_TYPES = [self::PENDING, self::SUCCESS, self::TOO_LONG, self::NO_PLAN];

    /** @ORM\Column(type="string") */
    private string $type;

    public function __construct(string $type)
    {
        if (!in_array($type, self::AVAILABLE_TYPES)) {
            throw new \InvalidArgumentException(sprintf('Unrecognized meal plan type %s', $type));
        }

        $this->type = $type;
    }

    public function __toString(): string
    {
        return $this->type;
    }

    public function isType(string $type): bool
    {
        return $this->type === $type;
    }

    public static function isValidType(string $type): bool
    {
        return in_array($type, self::AVAILABLE_TYPES);
    }
}