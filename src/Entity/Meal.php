<?php

namespace App\Entity;

use App\Repository\MealRepository;
use App\Util\Type\Decimal;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MealRepository::class)
 */
class Meal
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $name = null;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $slug = null;

    /**
     * @ORM\OneToMany(targetEntity=MealProduct::class, mappedBy="meal", orphanRemoval=true, cascade={"persist", "remove"})
     *
     * @var Collection<int, MealProduct>
     */
    private Collection $mealProducts;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $image = null;

    /**
     * @ORM\Column(type="json")
     */
    private array $steps = [];

    /**
     * @ORM\Column(type="integer")
     */
    private ?int $prepTime = null;

    /**
     * @ORM\ManyToMany(targetEntity=Diet::class)
     */
    private Collection $fitForDiets;

    public function __construct()
    {
        $this->mealProducts = new ArrayCollection();
        $this->fitForDiets = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return Collection<int, MealProduct>
     */
    public function getMealProducts(): Collection
    {
        return $this->mealProducts;
    }

    public function addMealProduct(MealProduct $mealProduct): self
    {
        if (!$this->mealProducts->contains($mealProduct)) {
            $this->mealProducts[] = $mealProduct;
            $mealProduct->setMeal($this);
        }

        return $this;
    }

    public function removeMealProduct(MealProduct $mealProduct): self
    {
        if ($this->mealProducts->removeElement($mealProduct)) {
            // set the owning side to null (unless already changed)
            if ($mealProduct->getMeal() === $this) {
                $mealProduct->setMeal(null);
            }
        }

        return $this;
    }

    public function getTotalNutrientAmountData(): array
    {
        $mealProductNutrients = array_map(
            fn (MealProduct $mealProduct) => $mealProduct->getNutrientAmountData(),
            $this->getMealProducts()->toArray()
        );

        $mealProductNutrientsTypes = array_map(
            fn (array $nutrientsData) => array_keys($nutrientsData),
            $mealProductNutrients
        );

        $types = $mealProductNutrientsTypes[0];
        if (count($mealProductNutrientsTypes) > 1) {
            $types = array_intersect(...$mealProductNutrientsTypes);
        }

        return array_reduce($mealProductNutrients, function (array $nutrientsCarry, array $nutrients) {
            foreach ($nutrientsCarry as $nutrientType => $nutrientCarry) {
                $nutrientsCarry[$nutrientType] = Decimal::add($nutrientCarry, $nutrients[$nutrientType]);
            }
            return $nutrientsCarry;
        }, array_fill_keys($types, Decimal::fromString('0')));
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getSteps(): ?array
    {
        return $this->steps;
    }

    public function setSteps(array $steps): self
    {
        $this->steps = $steps;

        return $this;
    }

    public function getPrepTime(): ?int
    {
        return $this->prepTime;
    }

    public function setPrepTime(int $prepTime): self
    {
        $this->prepTime = $prepTime;

        return $this;
    }

    /**
     * @return Collection|Diet[]
     */
    public function getFitForDiets(): Collection
    {
        return $this->fitForDiets;
    }

    public function addFitForDiet(Diet $fitForDiet): self
    {
        if (!$this->fitForDiets->contains($fitForDiet)) {
            $this->fitForDiets[] = $fitForDiet;
        }

        return $this;
    }

    public function removeFitForDiet(Diet $fitForDiet): self
    {
        $this->fitForDiets->removeElement($fitForDiet);

        return $this;
    }
}
