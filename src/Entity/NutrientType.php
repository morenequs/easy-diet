<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Embeddable;

/**
 * @Embeddable
 */
class NutrientType
{
    const ENERGY = 'energy';
    const PROTEINS = 'proteins';
    const CARBS = 'carbs';
    const FATS = 'fats';

    const AVAILABLE_NUTRIENT_TYPES = [self::ENERGY, self::PROTEINS, self::CARBS, self::FATS];

    /** @ORM\Column(type="string") */
    private string $type;

    public function __construct(string $type)
    {
        if (!in_array($type, self::AVAILABLE_NUTRIENT_TYPES)) {
            throw new \InvalidArgumentException(sprintf('Unrecognized nutrient requirement %s', $type));
        }

        $this->type = $type;
    }

    public function __toString(): string
    {
        return $this->type;
    }

    public function isType(string $type): bool
    {
        return $this->type === $type;
    }

    public static function isMealGeneratorSupportedNutrientType(string $type): bool
    {
        return in_array($type, [self::ENERGY, self::PROTEINS, self::CARBS, self::FATS]);
    }

    public function isMealGeneratorSupportedNutrient(): bool
    {
        return self::isMealGeneratorSupportedNutrientType($this->type);
    }

    public function getType(): string
    {
        return $this->type;
    }
}
