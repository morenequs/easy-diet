<?php

namespace App\Entity;

use App\Repository\NutrientRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Embedded;

/**
 * @ORM\Entity(repositoryClass=NutrientRepository::class)
 */
class Nutrient
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @Embedded(class="NutrientType", columnPrefix=false)
     */
    private ?NutrientType $type = null;

    /**
     * @ORM\Column(type="decimal", precision=14, scale=3)
     */
    private ?string $amount = null;

    /**
     * @ORM\ManyToOne(targetEntity=Product::class, inversedBy="nutrients")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Product $product = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAmount(): ?string
    {
        return $this->amount;
    }

    public function setAmount(string $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getType(): ?NutrientType
    {
        return $this->type;
    }

    public function isType(string $type): bool
    {
        return $this->type->isType($type);
    }

    public function setType(NutrientType $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }
}
