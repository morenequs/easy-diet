<?php

declare(strict_types=1);

namespace App\Service\FoodDataCentral\Transformer;

use App\Entity\FoodDataCentral\Food;
use App\Entity\FoodDataCentral\FoodNutrient;
use App\Entity\Nutrient;
use App\Entity\NutrientType;
use App\Entity\Product;
use Symfony\Component\String\Slugger\SluggerInterface;

class FoodToProductTransformer
{
    const PROTEIN = 1003;
    const FATS = 1004;
    const CARBS = 1005;
    const KCAL = 1008;

    const NUTRIENT_ID_TO_TYPE = [
        1003 => NutrientType::PROTEINS,
        1004 => NutrientType::FATS,
        1005 => NutrientType::CARBS,
        1008 => NutrientType::ENERGY,
    ];

    private SluggerInterface $slugger;

    public function __construct(SluggerInterface $slugger)
    {
        $this->slugger = $slugger;
    }

    /**
     * @throws \RuntimeException
     */
    public function transform(Food $food): Product
    {
        if (!$name = $food->getDescription()) {
            throw new \RuntimeException('Food must have a description.');
        }

        $product = new Product();
        $product->setName($name);
        $product->setSlug($this->slugger->slug($name)->lower()->toString());
        $this->addNutrients($food->getFoodNutrients()->toArray(), $product);

        return $product;
    }

    /**
     * @param FoodNutrient[] $foodNutrients
     */
    private function addNutrients(array $foodNutrients, Product $product): void
    {
        foreach ($foodNutrients as $foodNutrient) {
            if (!$nutrient = $foodNutrient->getNutrient()) {
                throw new \RuntimeException('Nutrient in FoodNutrient must be provided.');
            }

            $id = $nutrient->getId();
            $amount = $foodNutrient->getAmount();
            if (!$id or !$amount) {
                throw new \RuntimeException('Id of nutrient and amount of food nutrient must not be null');
            }

            if (!in_array($id, array_keys(self::NUTRIENT_ID_TO_TYPE))) {
                continue;
            }

            $nutrient = new Nutrient();
            $nutrient->setType(new NutrientType(self::NUTRIENT_ID_TO_TYPE[$id]));
            $nutrient->setAmount($amount);
            $product->addNutrient($nutrient);
        }
    }
}
