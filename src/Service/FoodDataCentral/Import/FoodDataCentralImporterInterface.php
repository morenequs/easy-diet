<?php

declare(strict_types=1);

namespace App\Service\FoodDataCentral\Import;

use App\Util\Progress\ProgressInterface;

interface FoodDataCentralImporterInterface
{
    public function importAll(string $pathToDirectoryWitData, ProgressInterface $progress): void;

    public function importType(string $entityClassName, string $pathToDirectoryWitData, ProgressInterface $progress): void;
}
