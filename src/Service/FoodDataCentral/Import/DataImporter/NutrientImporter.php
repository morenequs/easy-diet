<?php

declare(strict_types=1);

namespace App\Service\FoodDataCentral\Import\DataImporter;

use Doctrine\DBAL\ParameterType;

class NutrientImporter extends AbstractFoodImporter
{
    const COLUMN_ID = 'id';
    const COLUMN_NAME = 'name';
    const COLUMN_UNIT_NAME = 'unit_name';
    const COLUMN_NUTRIENT_NBR = 'nutrient_nbr';
    const COLUMN_NUTRIENT_RANK = 'nutrient_rank';

    protected function prepareRow(array $row): array
    {
        return [
            self::COLUMN_ID => $this->quote($row[0], ParameterType::INTEGER),
            self::COLUMN_NAME => $this->quote($row[1]),
            self::COLUMN_UNIT_NAME => $this->quote($row[2]),
            self::COLUMN_NUTRIENT_NBR => $this->quote($row[3]),
            self::COLUMN_NUTRIENT_RANK => '' !== $row[4] ? $this->quote($row[4]) : 'NULL',
        ];
    }

    protected function getColumns(): array
    {
        return [self::COLUMN_ID, self::COLUMN_NAME, self::COLUMN_UNIT_NAME, self::COLUMN_NUTRIENT_NBR,
            self::COLUMN_NUTRIENT_RANK, ];
    }
}
