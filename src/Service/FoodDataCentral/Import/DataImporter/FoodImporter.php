<?php

declare(strict_types=1);

namespace App\Service\FoodDataCentral\Import\DataImporter;

use Doctrine\DBAL\ParameterType;

class FoodImporter extends AbstractFoodImporter
{
    const COLUMN_FDC_ID = 'id';
    const COLUMN_DATA_TYPE = 'data_type';
    const COLUMN_DESCRIPTION = 'description';
    const COLUMN_PUBLICATION_DATE = 'publication_date';

    protected function prepareRow(array $row): array
    {
        return [
            self::COLUMN_FDC_ID => $this->quote($row[0], ParameterType::INTEGER),
            self::COLUMN_DATA_TYPE => $this->quote($row[1]),
            self::COLUMN_DESCRIPTION => $this->quote($row[2]),
            self::COLUMN_PUBLICATION_DATE => !empty($row[4]) ? $this->quote($row[4]) : 'NULL',
        ];
    }

    protected function getColumns(): array
    {
        return [self::COLUMN_FDC_ID, self::COLUMN_DATA_TYPE, self::COLUMN_DESCRIPTION, self::COLUMN_PUBLICATION_DATE];
    }
}
