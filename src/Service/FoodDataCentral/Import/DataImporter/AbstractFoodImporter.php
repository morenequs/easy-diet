<?php

declare(strict_types=1);

namespace App\Service\FoodDataCentral\Import\DataImporter;

use App\Util\InsertBulk;
use App\Util\Progress\ProgressInterface;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\ParameterType;
use Doctrine\ORM\EntityManagerInterface;

abstract class AbstractFoodImporter
{
    private EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param string[] $row
     *
     * @return array<string, string>
     */
    abstract protected function prepareRow(array $row): array;

    /** @return string[] */
    abstract protected function getColumns(): array;

    public function import(string $path, string $entityClassName, ProgressInterface $progress): void
    {
        $tableName = $this->getTableName($entityClassName);
        $this->clearTable($tableName);
        $this->populateDataToDatabase($path, $tableName, $progress);
    }

    private function populateDataToDatabase(string $path, string $tableName, ProgressInterface $progress): void
    {
        $progress->start($this->countLineInFile($path) - 1); // -1 omit header
        $insertBulk = $this->createInsertBulk($tableName, $this->getColumns());
        $i = 1;

        $file = $this->openFile($path, 'r');
        fgets($file); // omit header
        while ($line = fgets($file)) {
            $lineCsv = str_getcsv($line);
            $insertBulk->addRow($this->prepareRow($lineCsv));

            if (!(++$i % 1000)) {
                $insertBulk->executeInsert();
                $progress->advance(1000);
            }
        }
        $insertBulk->executeInsert();
        fclose($file);
        $progress->finish();
    }

    protected function quote(string $value, int $type = ParameterType::STRING): string
    {
        return $this->getConnection()->quote($value, $type);
    }

    private function clearTable(string $tableName): void
    {
        $stmt = $this->getConnection()->prepare('TRUNCATE ' . $tableName);
        $stmt->execute();
    }

    private function getTableName(string $entityClassName): string
    {
        return $this->em->getClassMetadata($entityClassName)->getTableName();
    }

    /** @param string[] $columns */
    private function createInsertBulk(string $tableName, array $columns): InsertBulk
    {
        return new InsertBulk($this->getConnection(), $tableName, $columns);
    }

    private function getConnection(): Connection
    {
        return $this->em->getConnection();
    }

    private function countLineInFile(string $path): int
    {
        $f = $this->openFile($path, 'rb');
        $cnt = 0;

        while (!feof($f)) {
            $chunk = fread($f, 65536);
            if (false === $chunk) {
                throw new \RuntimeException(sprintf('Failure during read file %s', $path));
            }
            $cnt += substr_count($chunk, "\n");
        }

        fclose($f);

        return $cnt;
    }

    /**
     * @return resource
     */
    private function openFile(string $path, string $mode)
    {
        $file = fopen($path, $mode);
        if (false === $file) {
            throw new \RuntimeException(sprintf('Failure during attempting to open file %s', $path));
        }

        return $file;
    }
}
