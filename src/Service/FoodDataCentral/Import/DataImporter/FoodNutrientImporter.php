<?php

declare(strict_types=1);

namespace App\Service\FoodDataCentral\Import\DataImporter;

use Doctrine\DBAL\ParameterType;

class FoodNutrientImporter extends AbstractFoodImporter
{
    const COLUMN_ID = 'id';
    const COLUMN_FOOD_ID = 'food_id';
    const COLUMN_NUTRIENT_ID = 'nutrient_id';
    const COLUMN_AMOUNT = 'amount';

    protected function prepareRow(array $row): array
    {
        return [
            self::COLUMN_ID => $this->quote($row[0], ParameterType::INTEGER),
            self::COLUMN_FOOD_ID => $this->quote($row[1], ParameterType::INTEGER),
            self::COLUMN_NUTRIENT_ID => $this->quote($row[2], ParameterType::INTEGER),
            self::COLUMN_AMOUNT => $this->quote($row[3], ParameterType::STRING),
        ];
    }

    protected function getColumns(): array
    {
        return [self::COLUMN_ID, self::COLUMN_FOOD_ID, self::COLUMN_NUTRIENT_ID, self::COLUMN_AMOUNT];
    }
}
