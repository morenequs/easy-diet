<?php

declare(strict_types=1);

namespace App\Service\FoodDataCentral\Import;

use App\Service\FoodDataCentral\Import\DataImporter\AbstractFoodImporter;

class ImporterConfig
{
    private AbstractFoodImporter $importer;
    private string $fileNameToImport;

    public function __construct(AbstractFoodImporter $importer, string $fileNameToImport)
    {
        $this->importer = $importer;
        $this->fileNameToImport = $fileNameToImport;
    }

    public function getImporter(): AbstractFoodImporter
    {
        return $this->importer;
    }

    public function getFileNameToImport(): string
    {
        return $this->fileNameToImport;
    }
}
