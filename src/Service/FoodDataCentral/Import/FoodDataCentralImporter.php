<?php

declare(strict_types=1);

namespace App\Service\FoodDataCentral\Import;

use App\Entity\FoodDataCentral\Food;
use App\Entity\FoodDataCentral\FoodNutrient;
use App\Entity\FoodDataCentral\Nutrient;
use App\Service\FoodDataCentral\Import\DataImporter\FoodImporter;
use App\Service\FoodDataCentral\Import\DataImporter\FoodNutrientImporter;
use App\Service\FoodDataCentral\Import\DataImporter\NutrientImporter;
use App\Util\Progress\ProgressInterface;
use Doctrine\DBAL\Connection;

class FoodDataCentralImporter implements FoodDataCentralImporterInterface
{
    const FOOD_FILENAME = 'food.csv';
    const NUTRIENT_FILENAME = 'nutrient.csv';
    const FOOD_NUTRIENT_FILENAME = 'food_nutrient.csv';

    /** @var Connection */
    private Connection $connection;

    /** @var array<string, ImporterConfig> */
    private array $mapTypeToImporter = [];

    public function __construct(
        Connection $connection,
        FoodImporter $foodImporter,
        NutrientImporter $nutrientImporter,
        FoodNutrientImporter $foodNutrientImporter
    ) {
        $this->mapTypeToImporter = [
            FoodNutrient::class => new ImporterConfig($foodNutrientImporter, self::FOOD_NUTRIENT_FILENAME),
            Food::class => new ImporterConfig($foodImporter, self::FOOD_FILENAME),
            Nutrient::class => new ImporterConfig($nutrientImporter, self::NUTRIENT_FILENAME),
        ];
        $this->connection = $connection;
    }

    public function importAll(string $pathToDirectoryWitData, ProgressInterface $progress): void
    {
        $this->connection->exec('SET FOREIGN_KEY_CHECKS=0;');
        try {
            /**
             * @var string $entityClassName
             * @var ImporterConfig $importerData
             */
            foreach ($this->mapTypeToImporter as $entityClassName => $importerData) {
                $path = $pathToDirectoryWitData . '/' . $importerData->getFileNameToImport();
                $importer = $importerData->getImporter();
                $importer->import($path, $entityClassName, $progress);
            }
        } finally {
            $this->connection->exec('SET FOREIGN_KEY_CHECKS=1;');
        }
    }

    public function importType(
        string $entityClassName,
        string $pathToDirectoryWitData,
        ProgressInterface $progress
    ): void {
        if (!isset($this->mapTypeToImporter[$entityClassName])) {
            throw new \Exception(sprintf('Type %s is not supported.', $entityClassName));
        }
        $this->connection->exec('SET FOREIGN_KEY_CHECKS=0;');
        $importerData = $this->mapTypeToImporter[$entityClassName];
        $path = $pathToDirectoryWitData . '/' . $importerData->getFileNameToImport();

        $importer = $importerData->getImporter();
        $importer->import($path, $entityClassName, $progress);
        $this->connection->exec('SET FOREIGN_KEY_CHECKS=1;');
    }
}
