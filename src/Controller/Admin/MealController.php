<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use App\Form\Admin\MealType;
use App\Form\Model\MealModel;
use App\Infrastructure\Command\Meal\CreateMealCommand;
use App\Infrastructure\Command\Meal\EditMealCommand;
use App\Infrastructure\Query\Meal\GetMealByIdQuery;
use App\Infrastructure\Query\Meal\GetMealDataTableQuery;
use App\Infrastructure\Query\Product\GetProductDataTableQuery;
use App\Shared\System\SystemInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/admin/meals")
 */
class MealController extends AbstractController
{
    private SystemInterface $system;
    private TranslatorInterface $translator;
    private LoggerInterface $logger;

    public function __construct(SystemInterface $system, TranslatorInterface $translator, LoggerInterface $logger)
    {
        $this->system = $system;
        $this->translator = $translator;
        $this->logger = $logger;
    }

    /**
     * @Route("/", methods={"GET"}, name="admin_meals_index")
     */
    public function index(): Response
    {
        return $this->render('admin/meals/index.html.twig');
    }

    /**
     * @Route("/dataTableSource", methods={"GET"}, name="admin_meals_data_table_source")
     */
    public function dataTableSource(Request $request): Response
    {
        $data = $this->system->handle(new GetMealDataTableQuery($request->query->all()));
        $response = array_merge(['draw' => (int) $request->query->get('draw')], $data);

        return new JsonResponse($response);
    }

    /**
     * @Route("/create", methods={"POST", "GET"}, name="admin_meals_create")
     */
    public function create(Request $request): Response
    {
        return $this->save($request);
    }

    /**
     * @Route("/{mealId}/edit", methods={"POST", "GET"}, name="admin_meals_edit")
     */
    public function edit(Request $request, $mealId): Response
    {
        return $this->save($request, $mealId);
    }

    private function save(Request $request, $mealId = null): Response
    {
        $mealModel = new MealModel();
        if ($mealId) {
            if (!$meal = $this->system->handle(new GetMealByIdQuery($mealId))) {
                throw new NotFoundHttpException('Meal not found');
            }
            $mealModel = MealModel::createFromMeal($meal);
        }

        $form = $this->createForm(MealType::class, $mealModel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                if ($mealId) {
                    $this->system->dispatch(new EditMealCommand($mealId, $mealModel));
                } else {
                    $this->system->dispatch(new CreateMealCommand($mealModel));
                }
                $this->addFlash('success', $this->translator->trans('success_save'));
            } catch (\Throwable $e) {
                $this->addFlash('warning', $this->translator->trans('fail_save'));
                $this->logger->critical('MEAL ERROR SAVE', ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]);
            }

            return $this->redirectToRoute('admin_meals_index');
        }

        return $this->render('admin/meals/save.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}