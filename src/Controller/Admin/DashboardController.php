<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractController
{
    /**
     * @Route("/admin/", methods={"GET"}, name="admin_dashboard")
     */
    public function index(): Response
    {
        return $this->render('admin/base.html.twig');
    }
}