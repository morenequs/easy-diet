<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use App\Form\Admin\ProductType;
use App\Form\Model\ProductModel;
use App\Infrastructure\Command\Product\CreateProductCommand;
use App\Infrastructure\Command\Product\EditProductCommand;
use App\Infrastructure\Query\Product\GetProductByIdQuery;
use App\Infrastructure\Query\Product\GetProductDataTableQuery;
use App\Infrastructure\Query\Product\SearchProductsByNameQuery;
use App\Infrastructure\ViewModel\Product\Transformer\ProductNameTransformer;
use App\Shared\System\SystemInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/admin/products")
 */
class ProductController extends AbstractController
{
    private SystemInterface $system;
    private TranslatorInterface $translator;
    private LoggerInterface $logger;

    public function __construct(SystemInterface $system, TranslatorInterface $translator, LoggerInterface $logger)
    {
        $this->system = $system;
        $this->translator = $translator;
        $this->logger = $logger;
    }

    /**
     * @Route("/", methods={"GET"}, name="admin_products_index")
     */
    public function index(): Response
    {
        return $this->render('admin/products/index.html.twig');
    }

    /**
     * @Route("/create", methods={"GET", "POST"}, name="admin_products_create")
     */
    public function create(Request $request): Response
    {
        return $this->save($request, null);
    }

    /**
     * @Route("/{productId}/edit", methods={"GET", "POST"}, name="admin_products_edit")
     */
    public function edit($productId, Request $request): Response
    {
        return $this->save($request, $productId);
    }

    /**
     * @Route("/dataTableSource", methods={"GET"}, name="admin_products_data_table_source")
     */
    public function dataTableSource(Request $request): Response
    {
        $data = $this->system->handle(new GetProductDataTableQuery($request->query->all()));
        $response = array_merge(['draw' => (int) $request->query->get('draw')], $data);

        return new JsonResponse($response);
    }

    /**
     * @Route("/search", methods={"GET"}, name="admin_products_search")
     */
    public function search(
        Request $request,
        ProductNameTransformer $productNameTransformer,
        SerializerInterface $serializer
    ): Response
    {
        $products = $this->system->handle(new SearchProductsByNameQuery($request->query->get('query') ?: '', 5));
        $productNameModels = $productNameTransformer->transformMultiple($products);

        return new JsonResponse($serializer->serialize($productNameModels, 'json'), 200, [], true);
    }

    private function save(Request $request, $productId): Response
    {
        $productModel = new ProductModel();
        if ($productId) {
            if (!$product = $this->system->handle(new GetProductByIdQuery($productId))) {
                throw new NotFoundHttpException('Product not found');
            }
            $productModel = ProductModel::createFromProduct($product);
        }

        $form = $this->createForm(ProductType::class, $productModel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                if (!$productId) {
                    $this->system->dispatch(new CreateProductCommand($productModel));
                } else {
                    $this->system->dispatch(new EditProductCommand($productId, $productModel));
                }
                $this->addFlash('success', $this->translator->trans('success_save'));
            } catch (\Throwable $e) {
                $this->addFlash('warning', $this->translator->trans('fail_save'));
                $this->logger->critical('PRODUCT ERROR SAVE', ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]);
            }

            return $this->redirectToRoute('admin_products_index');
        }

        return $this->render('admin/products/save.html.twig', [
            'form' => $form->createView()
        ]);
    }
}