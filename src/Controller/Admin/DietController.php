<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use App\Form\Admin\DietType;
use App\Form\Model\DietModel;
use App\Infrastructure\Command\Diet\CreateDietCommand;
use App\Infrastructure\Command\Diet\EditDietCommand;
use App\Infrastructure\Query\Diet\GetDietByIdQuery;
use App\Infrastructure\Query\Diet\GetDietDataTableQuery;
use App\Shared\System\SystemInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/admin/diets")
 */
class DietController extends AbstractController
{
    private SystemInterface $system;
    private TranslatorInterface $translator;
    private LoggerInterface $logger;

    public function __construct(SystemInterface $system, TranslatorInterface $translator, LoggerInterface $logger)
    {
        $this->system = $system;
        $this->translator = $translator;
        $this->logger = $logger;
    }

    /**
     * @Route("/", methods={"GET"}, name="admin_diets_index")
     */
    public function index(): Response
    {
        return $this->render('admin/diets/index.html.twig');
    }

    /**
     * @Route("/dataTableSource", methods={"GET"}, name="admin_diets_data_table_source")
     */
    public function dataTableSource(Request $request): Response
    {
        $data = $this->system->handle(new GetDietDataTableQuery($request->query->all()));
        $response = array_merge(['draw' => (int) $request->query->get('draw')], $data);

        return new JsonResponse($response);
    }

    /**
     * @Route("/create", methods={"GET", "POST"}, name="admin_diets_create")
     */
    public function create(Request $request): Response
    {
        return $this->save($request);
    }

    /**
     * @Route("/{dietId}/edit", methods={"POST", "GET"}, name="admin_diets_edit")
     */
    public function edit(Request $request, $dietId): Response
    {
        return $this->save($request, $dietId);
    }

    private function save(Request $request, $dietId = null): Response
    {
        $dietModel = new DietModel();
        if ($dietId) {
            if (!$diet = $this->system->handle(new GetDietByIdQuery($dietId))) {
                throw new NotFoundHttpException('Diet not found');
            }
            $dietModel = DietModel::createFromDiet($diet);
        }

        $form = $this->createForm(DietType::class, $dietModel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                if ($dietId) {
                    $this->system->dispatch(new EditDietCommand($dietId, $dietModel));
                } else {
                    $this->system->dispatch(new CreateDietCommand($dietModel));
                }
                $this->addFlash('success', $this->translator->trans('success_save'));
            } catch (\Throwable $e) {
                $this->addFlash('warning', $this->translator->trans('fail_save'));
                $this->logger->critical('DIET ERROR SAVE', ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]);
            }

            return $this->redirectToRoute('admin_diets_index');
        }

        return $this->render('admin/diets/save.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}