<?php

declare(strict_types=1);

namespace App\Controller\Api;

use App\Infrastructure\Command\MealPlan\GenerateMealPlanCommand;
use App\Infrastructure\Query\MealPlan\GetInitDataQuery;
use App\Infrastructure\Query\MealPlan\GetMealByRequirementsQuery;
use App\Infrastructure\Query\MealPlan\GetMealPlanByIdQuery;
use App\Infrastructure\Service\MealPlan\MealPlanRequirements\MealPlanRequirementsFactory;
use App\Shared\System\SystemInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class MealPlanCreatorController
{
    private SystemInterface $system;
    private SerializerInterface $serializer;

    public function __construct(SystemInterface $system, SerializerInterface $serializer)
    {
        $this->system = $system;
        $this->serializer = $serializer;
    }

    /**
     * @Route("/mealPlan/initData", methods={"GET"}, name="get_init_data")
     */
    public function getInitData(): Response
    {
        $initData = $this->system->handle(new GetInitDataQuery());
        return new JsonResponse($this->serializer->serialize($initData, 'json'), 200, [], true);
    }

    /**
     * @Route("/mealPlan/create", methods={"POST"}, name="generate_meal_plan")
     */
    public function createMealPlan(Request $request, MealPlanRequirementsFactory $mealPlanRequirementsFactory): Response
    {
        $data = json_decode($request->getContent(), true);

        $requirements = $mealPlanRequirementsFactory->create($data);

        $id = Uuid::uuid4();

        $this->system->dispatch(new GenerateMealPlanCommand($id, $requirements));

        return new JsonResponse(json_encode(['id' => $id]), 200, [], true);
    }

    /**
     * @Route("/mealPlan/{id}", methods={"GET"}, name="get_meal_plan_by_id")
     */
    public function getMealPlan(string $id): Response
    {
        $mealPlan = $this->system->handle(new GetMealPlanByIdQuery($id));

        return new JsonResponse($this->serializer->serialize($mealPlan, 'json'), 200, [], true);
    }

    /**
     * @Route("/meal/generateByRequirement", methods={"POST", "GET"}, name="generate_meal")
     */
    public function oneMeal(Request $request, MealPlanRequirementsFactory $mealPlanRequirementsFactory): Response
    {
        $data = json_decode($request->getContent(), true);
        $requirements = $mealPlanRequirementsFactory->create($data);
        if (!$meal = $this->system->handle(new GetMealByRequirementsQuery($requirements))) {
            return new JsonResponse(['message' => 'Brak dostępnych propozycji spełniających wymagania.'], 404);
        }

        return new JsonResponse($this->serializer->serialize($meal, 'json'), 200, [], true);
    }
}