import initAutocomplete from "../../components/autocomplete";

$(document).ready(() => {
  $('.parent-collection').collection({
    // allow_duplicate: true,
    add: '<button class="btn btn-success add-word-button">Add</button>',
    add_at_the_end: true,
    allow_down: false,
    allow_up: false,
    prefix: 'mealProduct',
    after_add: function (collection, element) {
      const input = element.find($('.js-autocomplete'))

      initAutocomplete(
        input,
        input.data('autocomplete-url'),
        input.data('autocomplete-field'),
        input.data('default')
      );
      return true;
    },
  });

  $('.parent-collection2').collection({
    // allow_duplicate: true,
    add: '<button class="btn btn-success add-word-button">Add</button>',
    add_at_the_end: true,
    allow_down: false,
    allow_up: false,
    prefix: 'steps',
  });
});