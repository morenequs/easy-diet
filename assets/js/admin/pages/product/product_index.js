import initDataTable from "../../components/data_table";

$(document).ready(() => {
  const table = $('#table');
  const dataInfo = $('#data-info');

  initDataTable(table, dataInfo, {
    columns: [{data: "name"}]
  });
});