import 'autocomplete.js/dist/autocomplete.jquery.min';
import '../../../css/admin/autocomplete.css';

export default function initAutocomplete(input, url, key, defaultValue) {
  input.autocomplete({hint: false}, [
    {
      source: function (query, cb) {
        $.ajax({
          url: url + '?query=' + query
        }).then(function (data) {
          cb(data);
        });
      },
      displayKey: key,
      debounce: 500
    }
  ]);

  if (typeof defaultValue !== 'undefined') {
    input.autocomplete('val', defaultValue);
  }
}