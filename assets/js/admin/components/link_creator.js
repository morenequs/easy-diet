export default function createLink(url, fieldToReplace, args) {
    for (let i = 0; i < fieldToReplace.length; i++) {
        url = url.replace(fieldToReplace[i], args[i]);
    }
    return url;
}