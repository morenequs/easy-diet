import 'datatables.net/js/jquery.dataTables.min';
import 'datatables.net-bs4/js/dataTables.bootstrap4.min';
import 'datatables.net-responsive-bs4/js/responsive.bootstrap4.min';
import 'datatables.net-bs4/css/dataTables.bootstrap4.min.css';
import 'datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css';
import createLink from "./link_creator";

export default function initDataTable(table, dataInfo, config)
{
    const defaultConfig = {
        ownColumnsConfig: false,
        columns: [],
        columnDefs: [],
        order: [[0 , "desc"]],
        initComplete: function () {}
    }

    config = Object.assign({}, defaultConfig, config);

    let columns;
    let columnDefs;
    if (config.ownColumnsConfig) {
        columns = config.columns;
        columnDefs = config.columnDefs;
    } else {
        columns = [{data: "id"}, ...config.columns];
        columnDefs = [
            ...config.columnDefs,
            // {
            //     targets: [0],
            //     visible: false
            // },
            {
                render: function ( data, type, row ) {
                    return `<a href="${createLink(dataInfo.data('edit-url'), ['_ID_'], [row['id']])}" class="edit">${data}</a>`;
                },
                targets: 1
            },
            // {orderable: false, targets: [1]},
        ]
    }

    const dataTable = table.DataTable({
        processing: true,
        serverSide: true,
        ajax: dataInfo.data('source-url'),
        columns: columns,
        columnDefs: columnDefs,
        initComplete: config.initComplete,
        order: config.order
    });
}