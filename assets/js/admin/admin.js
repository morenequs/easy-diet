import 'jquery';
import 'bootstrap/dist/js/bootstrap.min';
import 'jquery-ui';
import 'popper.js/dist/popper.min';
import 'chart.js/dist/Chart.min';
import './pcoded.min';
import './script';
import './vartical-layout';
import '../../css/admin/admin.css';
import 'chosen-js/chosen.jquery.min';
import initAutocomplete from "./components/autocomplete";

$(document).ready(function() {
  $('.js-autocomplete').each(function() {
    const input = $(this);
    initAutocomplete(
      input,
      input.data('autocomplete-url'),
      input.data('autocomplete-field'),
      input.data('default')
    );
  })

  $('.chosen').each(function() {
    const input = $(this);
    input.chosen({ width: "100%" });
  })
});