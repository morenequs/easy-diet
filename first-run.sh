#!/bin/bash

composer install
yarn install
yarn encore dev
bin/console doctrine:migrations:migrate -q
bin/console messenger:setup-transports
bin/console app:create-user