<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201122193547 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE fdc_food_nutrient (id INT AUTO_INCREMENT NOT NULL, food_id INT DEFAULT NULL, nutrient_id INT DEFAULT NULL, amount NUMERIC(14, 3) NOT NULL, INDEX IDX_5C3DCDD0BA8E87C4 (food_id), INDEX IDX_5C3DCDD027373320 (nutrient_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE fdc_food_nutrient ADD CONSTRAINT FK_5C3DCDD0BA8E87C4 FOREIGN KEY (food_id) REFERENCES fdc_food (id)');
        $this->addSql('ALTER TABLE fdc_food_nutrient ADD CONSTRAINT FK_5C3DCDD027373320 FOREIGN KEY (nutrient_id) REFERENCES fdc_nutrient (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE fdc_food_nutrient');
    }
}
