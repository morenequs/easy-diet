<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201213191151 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE meal CHANGE nutrient_kcal nutrient_energy NUMERIC(5, 1) NOT NULL');
        $this->addSql('ALTER TABLE product CHANGE nutrient_kcal nutrient_energy NUMERIC(5, 1) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE meal CHANGE nutrient_energy nutrient_kcal NUMERIC(5, 1) NOT NULL');
        $this->addSql('ALTER TABLE product CHANGE nutrient_energy nutrient_kcal NUMERIC(5, 1) NOT NULL');
    }
}
