<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201226002000 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE nutrient (id INT AUTO_INCREMENT NOT NULL, product_id INT NOT NULL, amount NUMERIC(14, 3) NOT NULL, type VARCHAR(255) NOT NULL, INDEX IDX_A9962C5A4584665A (product_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE nutrient ADD CONSTRAINT FK_A9962C5A4584665A FOREIGN KEY (product_id) REFERENCES product (id)');
        $this->addSql('ALTER TABLE meal DROP nutrient_energy, DROP nutrient_proteins, DROP nutrient_carbs, DROP nutrient_fats');
        $this->addSql('ALTER TABLE product DROP nutrient_energy, DROP nutrient_proteins, DROP nutrient_carbs, DROP nutrient_fats');
        $this->addSql('CREATE TABLE projection_meal (id INT AUTO_INCREMENT NOT NULL, energy NUMERIC(14, 3) NOT NULL, fats NUMERIC(14, 3) NOT NULL, proteins NUMERIC(14, 3) NOT NULL, carbs NUMERIC(14, 3) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE projection_meal');
        $this->addSql('DROP TABLE nutrient');
        $this->addSql('ALTER TABLE meal ADD nutrient_energy NUMERIC(5, 1) NOT NULL, ADD nutrient_proteins NUMERIC(5, 1) NOT NULL, ADD nutrient_carbs NUMERIC(5, 1) NOT NULL, ADD nutrient_fats NUMERIC(5, 1) NOT NULL');
        $this->addSql('ALTER TABLE product ADD nutrient_energy NUMERIC(5, 1) NOT NULL, ADD nutrient_proteins NUMERIC(5, 1) NOT NULL, ADD nutrient_carbs NUMERIC(5, 1) NOT NULL, ADD nutrient_fats NUMERIC(5, 1) NOT NULL');
    }
}
