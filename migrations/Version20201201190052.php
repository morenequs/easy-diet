<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201201190052 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE meal (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, slug VARCHAR(255) NOT NULL, nutrient_kcal NUMERIC(5, 1) NOT NULL, nutrient_proteins NUMERIC(5, 1) NOT NULL, nutrient_carbs NUMERIC(5, 1) NOT NULL, nutrient_fats NUMERIC(5, 1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE meal_product (id INT AUTO_INCREMENT NOT NULL, meal_id INT NOT NULL, product_id INT NOT NULL, gram NUMERIC(5, 1) NOT NULL, INDEX IDX_AAF9B6B4639666D6 (meal_id), INDEX IDX_AAF9B6B44584665A (product_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, slug VARCHAR(255) NOT NULL, nutrient_kcal NUMERIC(5, 1) NOT NULL, nutrient_proteins NUMERIC(5, 1) NOT NULL, nutrient_carbs NUMERIC(5, 1) NOT NULL, nutrient_fats NUMERIC(5, 1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE meal_product ADD CONSTRAINT FK_AAF9B6B4639666D6 FOREIGN KEY (meal_id) REFERENCES meal (id)');
        $this->addSql('ALTER TABLE meal_product ADD CONSTRAINT FK_AAF9B6B44584665A FOREIGN KEY (product_id) REFERENCES product (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE meal_product DROP FOREIGN KEY FK_AAF9B6B4639666D6');
        $this->addSql('ALTER TABLE meal_product DROP FOREIGN KEY FK_AAF9B6B44584665A');
        $this->addSql('DROP TABLE meal');
        $this->addSql('DROP TABLE meal_product');
        $this->addSql('DROP TABLE product');
    }
}
