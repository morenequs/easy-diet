# Backend (Meal plan generator)

Current project supports docker only for development environment.

## Requirements:

- docker
- docker-compose
- linux distribution

## Instalation

### Build docker images

Run command in project directory:
```
sh docker-build.sh
```

### First run
Run commands in project directory:
```
docker-compose up -d
docker-compose exec php sh first-run.sh
docker-compose restart
```

### Fake data

By default application starts with empty database (without meals and products).
In order to complete fake data you should make some steps:

```
git clone https://gitlab.com/morenequs/food-database-data.git food_data #clone reppository with official food data (food database) to proper directory (food_data)
docker-compose exec php bash #jump to php container
bin/console app:import-food-data-from-csv #import data from csv to database
composer fake-data-load #load fake data
exit
```