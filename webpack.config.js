var Encore = require('@symfony/webpack-encore');
var CopyWebpackPlugin = require('copy-webpack-plugin');

// Manually configure the runtime environment if not already configured yet by the "encore" command.
// It's useful when you use tools that rely on webpack.config.js file.
if (!Encore.isRuntimeEnvironmentConfigured()) {
    Encore.configureRuntimeEnvironment(process.env.NODE_ENV || 'dev');
}

Encore
    // directory where compiled assets will be stored
    .setOutputPath('public/build/')
    // public path used by the web server to access the output path
    .setPublicPath('/build')
    // only needed for CDN's or sub-directory deploy
    //.setManifestKeyPrefix('build/')

    /*
     * ENTRY CONFIG
     *
     * Each entry will result in one JavaScript file (e.g. app.js)
     * and one CSS file (e.g. app.css) if your JavaScript imports CSS.
     */
    .addEntry('admin', './assets/js/admin/admin.js')
    .addEntry('admin_product_index', './assets/js/admin/pages/product/product_index.js')
    .addEntry('admin_diet_index', './assets/js/admin/pages/diet/diet_index.js')
    .addEntry('admin_meal_index', './assets/js/admin/pages/meal/meal_index.js')
    .addEntry('admin_meal_save', './assets/js/admin/pages/meal/meal_save.js')
    .addEntry('jquery_collection', './assets/js/admin/jquery.collection.js')

    // When enabled, Webpack "splits" your files into smaller pieces for greater optimization.
    .splitEntryChunks()

    // will require an extra script tag for runtime.js
    // but, you probably want this, unless you're building a single-page app
    .enableSingleRuntimeChunk()

    /*
     * FEATURE CONFIG
     *
     * Enable & configure other features below. For a full
     * list of features, see:
     * https://symfony.com/doc/current/frontend.html#adding-more-features
     */
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    // enables hashed filenames (e.g. app.abc123.css)
    .enableVersioning(true)

    .configureBabel((config) => {
        config.plugins.push('@babel/plugin-proposal-class-properties');
    })

    // enables @babel/preset-env polyfills
    .configureBabelPresetEnv((config) => {
        config.useBuiltIns = 'usage';
        config.corejs = 3;
    })

    // enables Sass/SCSS support
    //.enableSassLoader()

    // uncomment if you use TypeScript
    //.enableTypeScriptLoader()

    // uncomment if you use React
    //.enableReactPreset()

    // uncomment to get integrity="..." attributes on your script & link tags
    // requires WebpackEncoreBundle 1.4 or higher
    //.enableIntegrityHashes(Encore.isProduction())

    .addPlugin(
        new CopyWebpackPlugin({
            patterns: [
                {
                    from: 'assets/images',
                    to: 'images',
                    globOptions: {
                        ignore: ['*.js,', '*.css', '*.map']
                    }
                },
                {
                    from: 'assets/foods',
                    to: 'foods',
                    globOptions: {
                        ignore: ['*.js,', '*.css', '*.map']
                    }
                },
                {
                    from: 'assets/fonts',
                    to: 'fonts',
                    globOptions: {
                        ignore: ['*.js,', '*.css', '*.map']
                    }
                }
            ]
        })
    )

    .autoProvidejQuery()
;

module.exports = Encore.getWebpackConfig();
