<?php

declare(strict_types=1);

namespace App\Tests\Unit\Util\Type;

use App\Util\Type\Decimal;
use PHPUnit\Framework\TestCase;

final class DecimalTest extends TestCase
{
    /**
     * @dataProvider createDecimalFromStringProvider
     */
    public function testCreateDecimalFromString(string $expected, string $value, int $scale): void
    {
        $this->assertEquals($expected, (string)Decimal::fromString($value, $scale));
    }

    public function createDecimalFromStringProvider(): array
    {
        return [
            ['1.00', '1.00', 2],
            ['1.00', '1.003', 2],
            ['1.10', '1.1', 2],
            ['1.333333', '1.333333', 6],
            ['10.00', '010', 2]
        ];
    }

    /**
     * @dataProvider attemptToCreateInvalidDecimalProvider
     */
    public function testAttemptToCreateInvalidDecimal(string $value): void
    {
        $this->expectException(\InvalidArgumentException::class);
        Decimal::fromString($value);
    }

    public function attemptToCreateInvalidDecimalProvider(): array
    {
        return [
            ['1,3'],
            ['1X'],
            ['NaN'],
            ['0x539']
        ];
    }

    /**
     * @dataProvider addProvider
     */
    public function testAdd(string $expected, Decimal $left, Decimal $right, int $scale = 2): void
    {
        $this->assertEquals($expected, (string)Decimal::add($left, $right, $scale));
        $this->assertEquals($expected, (string)Decimal::add($right, $left, $scale));
    }

    public function addProvider(): array
    {
        return [
            ['1.31', Decimal::fromString('0.111', 4), Decimal::fromString('1.2', 2)],
            ['2.99', Decimal::fromString('1.11', 2), Decimal::fromString('1.88', 2)],
            ['200000.38', Decimal::fromString('100000.19', 2), Decimal::fromString('100000.19', 2)],
            ['-11.11', Decimal::fromString('-111.11', 2), Decimal::fromString('100', 3)],
        ];
    }

    /**
     * @dataProvider subProvider
     */
    public function testSub(string $expected, Decimal $left, Decimal $right, int $scale = 2): void
    {
        $this->assertEquals($expected, (string)Decimal::sub($left, $right, $scale));
    }

    public function subProvider(): array
    {
        return [
            ['-1.00', Decimal::fromString('0.111', 3), Decimal::fromString('1.111', 3)],
            ['0.10', Decimal::fromString('1.3', 2), Decimal::fromString('1.2', 2)],
        ];
    }

    /**
     * @dataProvider mulProvider
     */
    public function testMul(string $expected, Decimal $left, Decimal $right, int $scale = 2): void
    {
        $this->assertEquals($expected, (string)Decimal::mul($left, $right, $scale));
        $this->assertEquals($expected, (string)Decimal::mul($right, $left, $scale));
    }

    public function mulProvider(): array
    {
        return [
            ['1.21', Decimal::fromString('1.1', 2), Decimal::fromString('1.1', 2)],
            ['1001.1235234', Decimal::fromString('1.001', 3), Decimal::fromString('1000.1234', 4), 7],
            ['2.00', Decimal::fromString('-1', 2), Decimal::fromString('-2', 2), 2],
        ];
    }

    /**
     * @dataProvider divProvider
     */
    public function testDiv(string $expected, Decimal $left, Decimal $right, int $scale = 2): void
    {
        $this->assertEquals($expected, (string)Decimal::div($left, $right, $scale));
    }

    public function divProvider(): array
    {
        return [
            ['0.990', Decimal::fromString('1', 2), Decimal::fromString('1.01', 2), 3],
            ['3.33', Decimal::fromString('10', 2), Decimal::fromString('3', 2), 2],
            ['0.50', Decimal::fromString('-1', 2), Decimal::fromString('-2', 2), 2],
        ];
    }

    /**
     * @dataProvider equalsProvider
     */
    public function testEquals(bool $expected, Decimal $left, Decimal $right): void
    {
        $this->assertEquals($expected, $left->equal($right));
    }

    public function equalsProvider(): array
    {
        return [
            [true, Decimal::fromString('1', 2), Decimal::fromString('1', 2)],
            [true, Decimal::fromString('1001.11', 2), Decimal::fromString('1001.1111', 2)],
            [false, Decimal::fromString('1', 2), Decimal::fromString('1.00001', 5)],
            [false, Decimal::fromString('1.01', 2), Decimal::fromString('1.00001', 5)],
        ];
    }

    /**
     * @dataProvider greaterThanOrEqualProvider
     */
    public function testGreaterThanOrEqual(bool $expected, Decimal $left, Decimal $right): void
    {
        $this->assertEquals($expected, $left->greaterThenOrEqual($right));
    }

    public function greaterThanOrEqualProvider(): array
    {
        return [
            [true, Decimal::fromString('1.1', 2), Decimal::fromString('1', 2)],
            [true, Decimal::fromString('1', 2), Decimal::fromString('1', 2)],
            [false, Decimal::fromString('1', 2), Decimal::fromString('1.00001', 5)],
            [false, Decimal::fromString('1', 2), Decimal::fromString('20000000')],
        ];
    }

    /**
     * @dataProvider lessThanOrEqualProvider
     */
    public function testLessThanOrEqual(bool $expected, Decimal $left, Decimal $right): void
    {
        $this->assertEquals($expected, $left->lessThenOrEqual($right));
    }

    public function lessThanOrEqualProvider(): array
    {
        return [
            [false, Decimal::fromString('1.1', 2), Decimal::fromString('1', 2)],
            [true, Decimal::fromString('1', 2), Decimal::fromString('1', 2)],
            [true, Decimal::fromString('1', 2), Decimal::fromString('1.00001', 5)],
            [true, Decimal::fromString('1', 2), Decimal::fromString('20000000')],
        ];
    }

    /**
     * @dataProvider mulByIntProvider
     */
    public function testMulByInt(string $expected, Decimal $value, int $multiplier): void
    {
        $this->assertEquals($expected, (string)$value->mulByInt($multiplier));
    }

    public function mulByIntProvider(): array
    {
        return [
            ['10.00', Decimal::fromString('1', 2), 10],
            ['10.10', Decimal::fromString('1.01', 2), 10],
            ['100.00100', Decimal::fromString('1.00001', 5), 100],
        ];
    }

    /**
     * @dataProvider getPercentageProvider
     */
    public function testGetPercentage(string $expected, Decimal $value, Decimal $percentage, int $scale = Decimal::DEFAULT_SCALE): void
    {
        $this->assertEquals($expected, (string)$value->getPercentage($percentage, $scale));
    }

    public function getPercentageProvider(): array
    {
        return [
            ['10.00', Decimal::fromString('100', 2), Decimal::fromString('10', 2)],
            ['1.27', Decimal::fromString('127', 2), Decimal::fromString('1', 2)],
            ['0.12', Decimal::fromString('127', 2), Decimal::fromString('0.1', 2)],
            ['0.12', Decimal::fromString('127', 2), Decimal::fromString('0.1', 2)],
            ['0.127', Decimal::fromString('127', 2), Decimal::fromString('0.1', 3), 3],
        ];
    }

    public function testToString(): void
    {
        $this->assertEquals('100.00', (string) Decimal::fromString("100"));
    }

    public function testToInt(): void
    {
        $this->assertEquals(100, (string) Decimal::fromString("100.11")->toInt());
    }
}