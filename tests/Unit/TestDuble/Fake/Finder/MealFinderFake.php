<?php

declare(strict_types=1);

namespace App\Tests\Unit\TestDuble\Fake\Finder;

use App\Entity\NutrientType;
use App\Infrastructure\Finder\MealFinderInterface;
use App\Infrastructure\Service\MealPlan\MealPlanRequirements\Range;
use App\Util\Type\Decimal;

class MealFinderFake implements MealFinderInterface
{
    /** @var array[] */
    private array $meals = [
        ['id' => 1, NutrientType::ENERGY => "130", NutrientType::CARBS => "5", NutrientType::PROTEINS => "5", NutrientType::FATS => "10"],
        ['id' => 2, NutrientType::ENERGY => "170", NutrientType::CARBS => "10", NutrientType::PROTEINS => "10", NutrientType::FATS => '10'],
        ['id' => 3, NutrientType::ENERGY => "199", NutrientType::CARBS => "16.75", NutrientType::PROTEINS => "15", NutrientType::FATS => "8"],
        ['id' => 4, NutrientType::ENERGY => "200", NutrientType::CARBS => "17", NutrientType::PROTEINS => "15", NutrientType::FATS => "8"],
        ['id' => 5, NutrientType::ENERGY => "215", NutrientType::CARBS => "10", NutrientType::PROTEINS => "10", NutrientType::FATS => "15"],
        ['id' => 6, NutrientType::ENERGY => "287", NutrientType::CARBS => "18", NutrientType::PROTEINS => "20", NutrientType::FATS => "15"],
        ['id' => 7, NutrientType::ENERGY => "300", NutrientType::CARBS => "30", NutrientType::PROTEINS => "20.25", NutrientType::FATS => "11"],
        ['id' => 8, NutrientType::ENERGY => "301", NutrientType::CARBS => "30", NutrientType::PROTEINS => "20.5", NutrientType::FATS => "11"],
        ['id' => 9, NutrientType::ENERGY => "315", NutrientType::CARBS => "25", NutrientType::PROTEINS => "20", NutrientType::FATS => "15"],
        ['id' => 10, NutrientType::ENERGY => "382", NutrientType::CARBS => "25", NutrientType::PROTEINS => "30", NutrientType::FATS => "18"],
        ['id' => 11, NutrientType::ENERGY => "418", NutrientType::CARBS => "34", NutrientType::PROTEINS => "30", NutrientType::FATS => "18"],
        ['id' => 12, NutrientType::ENERGY => "472", NutrientType::CARBS => "34", NutrientType::PROTEINS => "30", NutrientType::FATS => "24"],
        ['id' => 13, NutrientType::ENERGY => "592", NutrientType::CARBS => "39", NutrientType::PROTEINS => "55", NutrientType::FATS => "24"],
        ['id' => 14, NutrientType::ENERGY => "596", NutrientType::CARBS => "40", NutrientType::PROTEINS => "55", NutrientType::FATS => "24"],
        ['id' => 15, NutrientType::ENERGY => "656", NutrientType::CARBS => "40", NutrientType::PROTEINS => "70", NutrientType::FATS => "24"],
        ['id' => 16, NutrientType::ENERGY => "672", NutrientType::CARBS => "10", NutrientType::PROTEINS => "23", NutrientType::FATS => "60"],
        ['id' => 17, NutrientType::ENERGY => "744", NutrientType::CARBS => "10", NutrientType::PROTEINS => "23", NutrientType::FATS => "68"],
        ['id' => 18, NutrientType::ENERGY => "784", NutrientType::CARBS => "20", NutrientType::PROTEINS => "23", NutrientType::FATS => "68"],
        ['id' => 19, NutrientType::ENERGY => "804", NutrientType::CARBS => "25", NutrientType::PROTEINS => "23", NutrientType::FATS => "68"],
        ['id' => 20, NutrientType::ENERGY => "842", NutrientType::CARBS => "40", NutrientType::PROTEINS => "40", NutrientType::FATS => "58"],
        ['id' => 21, NutrientType::ENERGY => "950", NutrientType::CARBS => "40", NutrientType::PROTEINS => "40", NutrientType::FATS => "70"],
        ['id' => 22, NutrientType::ENERGY => "987", NutrientType::CARBS => "45", NutrientType::PROTEINS => "42", NutrientType::FATS => "71"],
    ];

    public function findMealsByRangeOfEnergy(Range $range, array $returnedNutrientTypes, array $excludedIds = []): array
    {
        $filteredByRange = $this->filterByRange($range);
        return $this->clearUnusedNutrientTypesFromRows($filteredByRange, $returnedNutrientTypes);
    }

    private function filterByRange(Range $range): array
    {
        return array_values(array_filter($this->meals, function (array $row) use ($range) {
            return $range->inRange(Decimal::fromString($row[NutrientType::ENERGY]));
        }));
    }

    private function clearUnusedNutrientTypesFromRows(array $rows, array $allowedNutrientTypes): array
    {
        return array_map(function (array $row) use ($allowedNutrientTypes) {
            return $this->clearUnusedNutrientTypesFromRow($row, $allowedNutrientTypes);
        }, $rows);
    }

    private function clearUnusedNutrientTypesFromRow(array $row, array $allowedNutrientTypes): array
    {
        $clearRow = ['id' => $row['id']];
        foreach ($row as $columnName => $value) {
            if (in_array($columnName, $allowedNutrientTypes)) {
                $clearRow[$columnName] = $value;
            }
        }
        return $clearRow;
    }
}