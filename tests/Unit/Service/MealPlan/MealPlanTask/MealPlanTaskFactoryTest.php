<?php

declare(strict_types=1);

namespace App\Tests\Unit\Service\MealPlan\MealPlanTask;

use App\Entity\NutrientType;
use App\Infrastructure\Service\MealPlan\MealPlanTask\MealPlanTaskFactory;
use App\Tests\ObjectMother\MealPlanRequirementsMother;
use PHPUnit\Framework\TestCase;

class MealPlanTaskFactoryTest extends TestCase
{
    /**
     * @dataProvider createMealPlanTaskProvider
     */
    public function testCreateMealPlanTask(
        string $expected,
        array $mealEnergySplit,
        array $nutrientRequirements,
        array $sets
    ): void
    {
        $mealPlanTaskFactory = new MealPlanTaskFactory();
        $mealPlanRequirements = MealPlanRequirementsMother::createBySimpleArray($mealEnergySplit, $nutrientRequirements);

        $result = $mealPlanTaskFactory->create($mealPlanRequirements, $sets);

        $this->assertEquals($expected, $result->getJson());
    }

    public function createMealPlanTaskProvider(): array
    {
        return [
            // expected, energySplit, nutrientRequirements, sets
            [
                '{"requirements":{"energy":{"min":800,"max":1000}},"sets":[[{"id":"1","energy":234},{"id":"2","energy":400}],[{"id":"1","energy":234},{"id":"2","energy":400}]]}',
                [50, 50],
                [[NutrientType::ENERGY, '800', '1000']],
                $this->getSetOfMeals(2, ['id', 'energy'])
            ],
            [
                '{"requirements":{"energy":{"min":1000,"max":1200},"proteins":{"min":100,"max":120},"carbs":{"min":120,"max":150},"fats":{"min":150,"max":170}},"sets":[[{"id":"1","energy":234,"proteins":12,"carbs":23,"fats":21},{"id":"2","energy":400,"proteins":32,"carbs":11,"fats":23}],[{"id":"1","energy":234,"proteins":12,"carbs":23,"fats":21},{"id":"2","energy":400,"proteins":32,"carbs":11,"fats":23}]]}',
                [50, 50],
                [[NutrientType::ENERGY, '1000', '1200'], [NutrientType::PROTEINS, '100', '120'], [NutrientType::CARBS, '120', '150'], [NutrientType::FATS, '150', '170']],
                $this->getSetOfMeals(2, ['id', 'energy', 'carbs', 'proteins', 'fats'])
            ]
        ];
    }

    private function getSetOfMeals(int $countSets, array $fields): array
    {
        $template = [
            ['id' => 1, 'energy' => '234', 'proteins' => '12', 'carbs' => '23', 'fats' => '21'],
            ['id' => 2, 'energy' => '400', 'proteins' => '32', 'carbs' => '11', 'fats' => '23'],
        ];

        $clearedFields = array_map(function (array $meal) use ($fields) {
            return array_filter($meal, function (string $key) use ($fields)  {
                return in_array($key, $fields);
            }, ARRAY_FILTER_USE_KEY);
        }, $template);

        return array_fill(0, $countSets, $clearedFields);
    }
}