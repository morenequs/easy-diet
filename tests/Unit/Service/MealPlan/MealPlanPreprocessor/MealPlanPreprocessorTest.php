<?php

declare(strict_types=1);

namespace App\Tests\Unit\Service\MealPlan\MealPlanPreprocessor;

use App\Entity\NutrientType;
use App\Infrastructure\Service\MealPlan\MealPlanPreprocessor\MealPlanPreprocessor;
use App\Infrastructure\Service\MealPlan\MealPlanRequirements\MealPlanRequirements;
use App\Infrastructure\Service\MealPlan\MealPlanRequirements\NutrientMealPlanSplit;
use App\Infrastructure\Service\MealPlan\MealPlanRequirements\NutrientRequirement;
use App\Tests\Unit\TestDuble\Fake\Finder\MealFinderFake;
use App\Util\Type\Decimal;
use PHPUnit\Framework\TestCase;

class MealPlanPreprocessorTest extends TestCase
{
    /**
     * @dataProvider prepareSetsOfMealsByRequirementsProvider
     */
    public function testPrepareSetsOfMealsByOnlyEnergyRequirements(
        array $acceptableEnergyRangesPerSet,
        array $energyMealPlanSplit,
        string $mealPlanEnergyMin,
        string $mealPlanEnergyMax
    ): void
    {
        $mealFinder = new MealFinderFake();
        $mealPlanPreprocessor = new MealPlanPreprocessor($mealFinder);
        $mealPlanRequirements = new MealPlanRequirements(
            [new NutrientMealPlanSplit(new NutrientType(NutrientType::ENERGY), $energyMealPlanSplit)],
            [new NutrientRequirement(
                new NutrientType(NutrientType::ENERGY),
                Decimal::fromString($mealPlanEnergyMin),
                Decimal::fromString($mealPlanEnergyMax)
            )]
        );

        $sets = $mealPlanPreprocessor->prepareSetsOfMealsByRequirements($mealPlanRequirements);
        $this->assertCount(count($energyMealPlanSplit), $sets);
        for ($i = 0; $i < count($energyMealPlanSplit); $i++) {
            $this->assertGreaterThan(0, count($sets[$i]));
            $min = $acceptableEnergyRangesPerSet[$i]['min'];
            $max = $acceptableEnergyRangesPerSet[$i]['max'];
            $this->assertEquals(
                count($sets[$i]),
                count(array_filter($sets[$i], function (array $meal) use ($min, $max) {
                    return $meal['energy'] >= $min && $meal['energy'] <= $max;
                }))
            );
        }
    }

    public function prepareSetsOfMealsByRequirementsProvider(): array
    {
        return [
            [
                [$this->rangeArray('200', '300')], [100], '200', '300',
            ],
            [
                [$this->rangeArray('495', '605'),$this->rangeArray('495', '605')], [50, 50], '1000', '1200',
            ],
            [
                [
                    $this->rangeArray('342', '418'),
                    $this->rangeArray('256.5', '313.5'),
                    $this->rangeArray('684', '836'),
                    $this->rangeArray('427.5', '522.5'),
                ], [20, 15, 40, 25], '1800', '2000',
            ],
        ];
    }

    private function rangeArray(string $min, string $max): array
    {
        return [
            'min' => $min,
            'max' => $max
        ];
    }
}