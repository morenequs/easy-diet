<?php

declare(strict_types=1);

namespace App\Tests\Unit\Service\MealPlan\MealPlanRequirements;

use App\Entity\NutrientType;
use App\Infrastructure\Service\MealPlan\MealPlanRequirements\NutrientMealPlanSplit;
use App\Infrastructure\Service\MealPlan\MealPlanRequirements\MealPlanRequirements;
use App\Infrastructure\Service\MealPlan\MealPlanRequirements\MealPlanRequirementsFactory;
use App\Infrastructure\Service\MealPlan\MealPlanRequirements\NutrientRequirement;
use App\Util\Type\Decimal;
use PHPUnit\Framework\TestCase;

class MealPlanRequirementsFactoryTest extends TestCase
{
    /**
     * @dataProvider createMealRequirementsProvider
     */
    public function testCreateMealRequirements(MealPlanRequirements $expected, array $data): void
    {
        $factory = new MealPlanRequirementsFactory();
        $mealPlanRequirements = $factory->create($data);
        $this->assertEquals($expected, $mealPlanRequirements);
    }

    public function createMealRequirementsProvider(): array
    {
        return [
            [
                new MealPlanRequirements(
                    [new NutrientMealPlanSplit(new NutrientType(NutrientType::ENERGY), [20, 80])],
                    [
                        new NutrientRequirement(new NutrientType(NutrientType::ENERGY), Decimal::fromString('1400'), Decimal::fromString('1800')),
                        new NutrientRequirement(new NutrientType(NutrientType::PROTEINS), Decimal::fromString('80.5'), Decimal::fromString('100')),
                        new NutrientRequirement(new NutrientType(NutrientType::CARBS), Decimal::fromString('100'), Decimal::fromString('120')),
                        new NutrientRequirement(new NutrientType(NutrientType::FATS), Decimal::fromString('50.5'), Decimal::fromString('70.5')),
                    ]),
                [
                    'energyMealSplit' => [
                        20, 80
                    ],
                    'nutrients' => [
                        ['type' => 'energy', 'min' => '1400', 'max' => '1800'],
                        ['type' => 'proteins', 'min' => '80.5', 'max' => '100'],
                        ['type' => 'carbs', 'min' => '100', 'max' => '120'],
                        ['type' => 'fats', 'min' => '50.5', 'max' => '70.5'],
                    ]
                ]
            ]
        ];
    }

    /**
     * @dataProvider attemptCreateIncorrectMealPlanRequirements
     */
    public function testAttemptCreateInvalidMealPlanRequirements(array $data): void
    {
        $this->expectException(\InvalidArgumentException::class);
        $factory = new MealPlanRequirementsFactory();
        $factory->create($data);
    }

    public function attemptCreateIncorrectMealPlanRequirements(): array
    {
        return [
            [[]], // empty
            [['energyMealSplit' => [100]]], // without nutrients
            [['nutrients' => [['type' => 'energy', 'min' => '10', 'max' => '20']]]], // without energyMealSplit
            [['energyMealSplit' => 100, 'nutrients' => [['type' => 'energy', 'min' => '10', 'max' => '20']]]], // invalid energyMealSplit
            [['energyMealSplit' => [10, 20], 'nutrients' => [['type' => 'energy', 'min' => '10', 'max' => '20']]]], // not equal 100 energyMealSplit
            [['energyMealSplit' => [10, 91], 'nutrients' => [['type' => 'energy', 'min' => '10', 'max' => '20']]]], // not equal 100 energyMealSplit
            [['energyMealSplit' => [100], 'nutrients' => [['type' => 'proteins', 'min' => '10', 'max' => '20']]]], // missing energy nutrients
            [[
                'energyMealSplit' => [100],
                'nutrients' => [
                    ['type' => 'energy', 'min' => '10', 'max' => '20'],
                    ['type' => 'energy', 'min' => '20', 'max' => '30'],
                ]
            ]], // not unique nutrients
        ];
    }
}