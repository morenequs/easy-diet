<?php

declare(strict_types=1);

namespace App\Tests\ObjectMother;

use App\Entity\NutrientType;
use App\Infrastructure\Service\MealPlan\MealPlanRequirements\MealPlanRequirements;
use App\Infrastructure\Service\MealPlan\MealPlanRequirements\NutrientMealPlanSplit;
use App\Infrastructure\Service\MealPlan\MealPlanRequirements\NutrientRequirement;
use App\Util\Type\Decimal;

class MealPlanRequirementsMother
{
    public static function createBySimpleArray(array $mealSplit, array $nutrientRequirementsSimpleArray): MealPlanRequirements
    {
        $nutrientRequirements = array_map(function (array $nutrientRequirement) {
            return new NutrientRequirement(
                new NutrientType($nutrientRequirement[0]),
                Decimal::fromString($nutrientRequirement[1]),
                Decimal::fromString($nutrientRequirement[2])
            );
        }, $nutrientRequirementsSimpleArray);

        return new MealPlanRequirements(
            [new NutrientMealPlanSplit(new NutrientType(NutrientType::ENERGY), $mealSplit)],
            $nutrientRequirements
        );
    }
}